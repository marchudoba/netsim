﻿using Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading;

namespace netsim_unittest
{


    /// <summary>
    ///This is a test class for SwitchProtocolTest and is intended
    ///to contain all SwitchProtocolTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SwitchProtocolTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for SwitchProtocol Constructor
        ///</summary>
        [TestMethod()]
        public void FrameSwitchingTest()
        {
            Interface lookedInterface;

            ManualResetEvent waitForEvent1 = new ManualResetEvent(false);
            ManualResetEvent waitForEvent2 = new ManualResetEvent(false);

            Interface int1 = new Interface("source1");
            Interface int2 = new Interface("source2");
            Interface int3 = new Interface("source3");

            Wire wire1 = new Wire();
            Wire wire2 = new Wire();
            Wire wire3 = new Wire();

            Switch Switch = new Switch();
            Switch.Interfaces[0].Port = wire1;
            Switch.Interfaces[1].Port = wire2;
            Switch.Interfaces[2].Port = wire3;

            int1.Port = wire1;
            int2.Port = wire2;
            int3.Port = wire3;

            wire1.SourceInterface = Switch.Interfaces[0];
            wire1.DestinationInterface = int1;

            wire2.SourceInterface = Switch.Interfaces[1];
            wire2.DestinationInterface = int2;

            wire3.SourceInterface = Switch.Interfaces[2];
            wire3.DestinationInterface = int3;



            PrivateObject switchProtocolStackHelper = new PrivateObject((Switch.Stack as SwitchProtocolStack));
            PrivateObject switchProtocolHelper = new PrivateObject((switchProtocolStackHelper.GetField("Protocols") as Dictionary<Protocol.Type, Protocol>)[Protocol.Type.SwitchProtocol]);
            SwitchTable table = switchProtocolHelper.GetFieldOrProperty("Table") as SwitchTable;


            MacAddress sourceMac = int1.Mac;
            MacAddress destMac = MacAddress.Broadcast;
            Protocol.Type etherType = Protocol.Type.Ipv4;

            EthernetPacket frame = new EthernetPacket(destMac, sourceMac, etherType, null);
            int2.ReceivedPacket += delegate(object sender, PacketReceivedEventArgs e)
            {
                waitForEvent1.Set();
            };
            int3.ReceivedPacket += delegate(object sender, PacketReceivedEventArgs e)
            {
                waitForEvent2.Set();
            };
            int1.Send(frame);

            bool ret1 = waitForEvent1.WaitOne();
            bool ret2 = waitForEvent2.WaitOne();

            if ((ret1 && ret2) == false)
            {
                Assert.Fail("Broadcast isn't flooded to ports.");
            }
            if (table.LookUp(sourceMac, out lookedInterface) ==false)
            {
                Assert.Fail("Table doesn't contain entry for int1");
            }

            waitForEvent1.Reset();
            waitForEvent2.Reset();

            destMac = int2.Mac;
            frame = new EthernetPacket(destMac, sourceMac, etherType, null);

            int1.Send(frame);
            ret1 = waitForEvent1.WaitOne();
            if (ret1 == false)
            {
                Assert.Fail("Failed to send to int2");
            }

        }
    }
}
