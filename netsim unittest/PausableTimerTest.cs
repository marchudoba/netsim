﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using System.Timers;


namespace netsim_unittest
{
    [TestClass()]
    public class PausableTimerTest
    {
        [TestMethod()]
        public void PauseTest()
        {
            bool ret = false;
            ManualResetEvent waitForEvent1 = new ManualResetEvent(false);

            PausableTimer timer = new PausableTimer();
            timer.Interval = 2000;
            timer.Elapsed += delegate(object sender, ElapsedEventArgs e)
            {
                waitForEvent1.Set();
            };
            
            ret = waitForEvent1.WaitOne(1000);
            if(ret == true)
            {
                Assert.Fail("Elapsed fired before 1s.");
            }
            timer.Paused = true;
            ret = waitForEvent1.WaitOne(1000);
            if (ret == true)
            {
                Assert.Fail("Elapsed fired when paused.");
            }

            timer.Paused = false;
            ret = waitForEvent1.WaitOne(1100);
            if (ret == true)
            {
                Assert.Fail("Elapsed wont fire after unpaused.");
            }

            Assert.IsTrue(true);
        }
    }
}
