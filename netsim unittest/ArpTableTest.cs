﻿using Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading;

namespace netsim_unittest
{


    /// <summary>
    ///This is a test class for ArpTableTest and is intended
    ///to contain all ArpTableTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ArpTableTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion



        /// <summary>
        ///A test for Table
        ///</summary>
        [TestMethod()]
        public void EntryAgingTest()
        {
            MacAddress dum;

            MacAddress testMac1 = new MacAddress();
            MacAddress testMac2 = new MacAddress();
            MacAddress testMac3 = new MacAddress();

            Ipv4Address testIp1 = new Ipv4Address("10.0.0.3/8");
            Ipv4Address testIp2 = new Ipv4Address("10.0.0.5/8");
            Ipv4Address testIp3 = new Ipv4Address("10.0.0.8/8");

            ArpTable table = new ArpTable();
            table.Add(testIp1, testMac1, 3);
            table.Add(testIp2, testMac2, 5);
            table.Add(testIp3, testMac3, 8);

            Thread.Sleep(1000);
            if ((table.LookUp(testIp1, out dum) && table.LookUp(testIp2, out dum) && table.LookUp(testIp3, out dum))==false)
            {
                Assert.Fail("Entries missing after 1 sec");
            }
            Thread.Sleep(2500);
            if (table.LookUp(testIp1, out dum) != false)
            {
                Assert.Fail("Entry not missing after 3.5 sec");
            }
            if ((table.LookUp(testIp2, out dum) && table.LookUp(testIp3, out dum))==false)
            {
                Assert.Fail("Entries missing after 3.5 sec");
            }
            Thread.Sleep(2000);
            if (table.LookUp(testIp2, out dum) != false)
            {
                Assert.Fail("Entry not missing after 5.5 sec");
            }
            if (table.LookUp(testIp3, out dum) == false)
            {
                Assert.Fail("Entries missing after 5.5 sec");
            }
            Thread.Sleep(3000);
            if (table.LookUp(testIp3, out dum) != false)
            {
                Assert.Fail("Entry not missing after 8.5 sec");
            }

            Assert.IsTrue(true);
        }
    }
}
