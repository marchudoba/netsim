﻿using Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace netsim_unittests
{


    /// <summary>
    ///This is a test class for Ipv4AddressTest and is intended
    ///to contain all Ipv4AddressTest Unit Tests
    ///</summary>
    [TestClass()]
    public class Ipv4AddressTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        /// Ipv4s the address constructor test.
        /// </summary>
        [TestMethod()]
        public void ConstructorTest()
        {
            PrivateObject ipAddressHelper = new PrivateObject(new Ipv4Address("192.168.1.1/20"));
            byte[] address = (byte[])ipAddressHelper.GetFieldOrProperty("address");
            byte[] mask = (byte[])ipAddressHelper.GetFieldOrProperty("mask");

            if (address[0] == 192 && address[1] == 168 && address[2] == 1 && address[3] == 1 && mask[0] == 255 && mask[1] == 255 && mask[2] == 240 && mask[3] == 0)
            {
                Assert.IsTrue(true);
            }
            else
            {
                Assert.Fail();
            }
        }

        [TestMethod()]
        public void IsOnSameNetworkTest()
        {
            Ipv4Address addr1 = new Ipv4Address("192.168.1.1/24");
            Ipv4Address addr2 = new Ipv4Address("192.168.1.128/24");
            if (addr1.IsOnSameNetwork(addr2) != true)
            {
                Assert.Fail();
                return;
            }

            addr1 = new Ipv4Address("192.168.1.1/24");
            addr2 = new Ipv4Address("192.168.2.128/24");
            if (addr1.IsOnSameNetwork(addr2) != false)
            {
                Assert.Fail();
                return;
            }

            addr1 = new Ipv4Address("192.168.1.1/20");
            addr2 = new Ipv4Address("192.168.1.128/24");
            if (addr1.IsOnSameNetwork(addr2) != false)
            {
                Assert.Fail();
                return;
            }

        }
        /// <summary>
        /// Masks the with test.
        /// </summary>
        [TestMethod()]
        public void MaskWithTest()
        {
            Ipv4Address testAddress = new Ipv4Address("192.168.1.1/24");
            string testMask = "255.0.0.0";
            Ipv4Address retAddress = testAddress.MaskWith(testMask);
            if (retAddress.ToString() != "192.0.0.0/8")
            {
                Assert.Fail();
                return;
            }

            testAddress = new Ipv4Address("10.0.0.1/8");
            testMask = "255.255.255.0";
            retAddress = testAddress.MaskWith(testMask);
            if (retAddress.ToString() != "10.0.0.0/24")
            {
                Assert.Fail();
                return;
            }
            Assert.IsTrue(true);
        }


        /// <summary>
        /// Equals the test.
        /// </summary>
        [TestMethod()]
        public void EqualsTest()
        {
            bool ret;
            Ipv4Address comp1 = new Ipv4Address("192.168.1.1/24");
            Ipv4Address comp2 = new Ipv4Address("192.168.1.1/24");
            ret = comp1.Equals(comp2);
            if (ret == false)
            {
                Assert.Fail();
                return;
            }


            comp1 = new Ipv4Address("192.168.1.1/20");
            comp2 = new Ipv4Address("192.168.1.1/24");
            ret = comp1.Equals(comp2);
            if (ret == true)
            {
                Assert.Fail();
                return;
            }

            comp1 = new Ipv4Address("10.168.1.1/4");
            comp2 = new Ipv4Address("10.168.1.1/4");
            ret = comp1.Equals(comp2);
            if (ret == false)
            {
                Assert.Fail();
                return;
            }
        }

        /// <summary>
        /// GetHashCode the test.
        /// </summary>
        [TestMethod()]
        public void GetHashCodeTest()
        {
            Ipv4Address address1 = new Ipv4Address("127.0.0.1/24");
            Ipv4Address address2 = new Ipv4Address("127.0.0.1/24");

            Assert.IsTrue(address1.GetHashCode() == address2.GetHashCode());

        }

        /// <summary>
        /// MaskLength test.
        /// </summary>
        [TestMethod()]
        public void MaskLengthTest()
        {
            Ipv4Address address = new Ipv4Address("1.1.1.1/8");
            if (address.MaskLength != 8)
            {
                Assert.Fail("Failed at " + address.ToString());
            }

            address = new Ipv4Address("1.1.1.1/0");
            if (address.MaskLength != 0)
            {
                Assert.Fail("Failed at " + address.ToString());
            }

            address = new Ipv4Address("1.1.1.1/9");
            if (address.MaskLength != 9)
            {
                Assert.Fail("Failed at " + address.ToString());
            }
            address = new Ipv4Address("1.1.1.1/16");
            if (address.MaskLength != 16)
            {
                Assert.Fail("Failed at " + address.ToString());
            }

            address = new Ipv4Address("1.1.1.1/31");
            if (address.MaskLength != 31)
            {
                Assert.Fail("Failed at " + address.ToString());
            }

            address = new Ipv4Address("1.1.1.1/15");
            if (address.MaskLength != 15)
            {
                Assert.Fail("Failed at " + address.ToString());
            }
        }
        /// <summary>
        /// Broadcasts the address test.
        /// </summary>
        [TestMethod()]
        public void BroadcastAddressTest()
        {
            Ipv4Address test = new Ipv4Address("10.0.0.1/8");
            string ret = test.BroadcastAddress;
            if(ret != "10.255.255.255")
            {
                Assert.Fail("Failed at 10.0.0.1/8.");
            }

            test = new Ipv4Address("10.32.0.0/11");
            ret = test.BroadcastAddress;
            if (ret != "10.63.255.255")
            {
                Assert.Fail("Failed at 10.32.0.1/12.");
            }
        }
    }
}
