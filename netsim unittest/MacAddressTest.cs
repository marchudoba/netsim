﻿using Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace netsim_unittests
{
    
    
    /// <summary>
    ///This is a test class for MacAddressTest and is intended
    ///to contain all MacAddressTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MacAddressTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        [TestMethod()]
        public void randomMacTest()
        {
            bool res;
            PrivateObject macAddressHelper = new PrivateObject(new MacAddress());
            res = (bool)macAddressHelper.Invoke("isMac", macAddressHelper.GetFieldOrProperty("address"));
            if (res == false)
            {
                Assert.Fail();
                return;
            }

            macAddressHelper = new PrivateObject(new MacAddress());
            res = (bool)macAddressHelper.Invoke("isMac", macAddressHelper.GetFieldOrProperty("address"));
            if (res == false)
            {
                Assert.Fail();
                return;
            }

            macAddressHelper = new PrivateObject(new MacAddress());
            res = (bool)macAddressHelper.Invoke("isMac", macAddressHelper.GetFieldOrProperty("address"));
            if (res == false)
            {
                Assert.Fail();
                return;
            }

            macAddressHelper = new PrivateObject(new MacAddress());
            res = (bool)macAddressHelper.Invoke("isMac", macAddressHelper.GetFieldOrProperty("address"));
            if (res == false)
            {
                Assert.Fail();
                return;
            }
        }


        /// <summary>
        ///A test for isMac
        ///</summary>
        [TestMethod()]
        public void isMacTest()
        {
            bool res;
            string address = "FFFFFFFFFFFF";
            PrivateObject macAddressHelper = new PrivateObject(new MacAddress());
            res = (bool)macAddressHelper.Invoke("isMac", address);
            if (res == false)
            {
                Assert.Fail();
                return;
            }

            address = "ABCDEF123456";
            res = (bool)macAddressHelper.Invoke("isMac", address);
            if (res == false)
            {
                Assert.Fail();
                return;
            }

            address = "GBCDEF123456";
            res = (bool)macAddressHelper.Invoke("isMac", address);
            if (res == true)
            {
                Assert.Fail();
                return;
            }

            address = "ABCDEF12345AA";
            res = (bool)macAddressHelper.Invoke("isMac", address);
            if (res == true)
            {
                Assert.Fail();
                return;
            }
            Assert.IsTrue(true);
        }
    }
}
