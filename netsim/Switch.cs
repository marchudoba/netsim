﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core
{
    /// <summary>
    /// Represents switch
    /// </summary>
    [Serializable]
    public class Switch : NetworkElement
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Switch"/> class.
        /// </summary>
        public Switch() :base(10)
        {
            Stack = new SwitchProtocolStack(Interfaces);
            //needed so IsUp is true
            foreach(Interface interfac in Interfaces)
            {
                interfac.IpAddress = new Ipv4Address("127.0.0.1/16");
            }
        }
        #endregion

        #region ISerializable
        protected Switch(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
        #endregion
    }
}
