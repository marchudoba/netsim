﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core;

namespace Gui
{
    /// <summary>
    /// View class for echo client
    /// </summary>
    class EchoClientView
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="EchoClientView"/> class.
        /// </summary>
        /// <param name="stack">The stack.</param>
        public EchoClientView(PcProtocolStack stack)
        {
            client = stack.Protocols[Core.Protocol.Type.EchoClient] as EchoClient;
            Protocols = new List<Core.Protocol.Type>();
            Protocols.Add(Core.Protocol.Type.Udp);
            Protocols.Add(Core.Protocol.Type.Tcp);
        }
        #endregion

        #region Fields
        EchoClient client;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the destination ip.
        /// </summary>
        /// <value>
        /// The destination ip.
        /// </value>
        public string DestinationIp { get; set; }
        /// <summary>
        /// Gets or sets the port.
        /// </summary>
        /// <value>
        /// The port.
        /// </value>
        public int Port { get; set; }
        /// <summary>
        /// Gets or sets the protocol.
        /// </summary>
        /// <value>
        /// The protocol.
        /// </value>
        public Protocol.Type Protocol { get; set; }
        /// <summary>
        /// Gets or sets the protocols.
        /// </summary>
        /// <value>
        /// The protocols.
        /// </value>
        public List<Protocol.Type> Protocols { get; set; }
        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        public string Text { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Sends this instance.
        /// </summary>
        public void Send()
        {
            if(DestinationIp == "" || Port == 0 || Text == "" || (Protocol != Core.Protocol.Type.Tcp && Protocol != Core.Protocol.Type.Udp))
            {
                return;
            }
            client.Send(new Ipv4Address(DestinationIp + "/0"), Port, Text, Protocol);
        }
        #endregion
    }
}
