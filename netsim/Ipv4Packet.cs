﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core
{
    /// <summary>
    /// Represents Ipv4Packet
    /// </summary>
    public class Ipv4Packet : Layer2Packet
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Ipv4Packet"/> class.
        /// </summary>
        /// <param name="sourceAddress">The source address.</param>
        /// <param name="destinationAddress">The destination address.</param>
        /// <param name="protocol">The protocol.</param>
        /// <param name="ttl">The TTL.</param>
        /// <param name="data">The data.</param>
        public Ipv4Packet(Ipv4Address sourceAddress, Ipv4Address destinationAddress, Protocol.Type protocol, int ttl, Layer3Packet data) : base(data)
        {
            SourceAddress = sourceAddress;
            DestinationAddress = destinationAddress;
            Protocol = protocol;
            TimeToLive = ttl;
            Checksum = calculateChecksum();
        }
        #endregion
        
        #region Properties
        /// <summary>
        /// Gets or sets the time to live.
        /// </summary>
        /// <value>
        /// The time to live.
        /// </value>
        public int TimeToLive { get; set; }
        /// <summary>
        /// Gets or sets the protocol.
        /// </summary>
        /// <value>
        /// The protocol.
        /// </value>
        public Protocol.Type Protocol { get; set; }
        /// <summary>
        /// Gets or sets the checksum.
        /// </summary>
        /// <value>
        /// The checksum.
        /// </value>
        public int Checksum { get; set; }
        /// <summary>
        /// Gets or sets the source address.
        /// </summary>
        /// <value>
        /// The source address.
        /// </value>
        public Ipv4Address SourceAddress { get; set; }
        /// <summary>
        /// Gets or sets the destination address.
        /// </summary>
        /// <value>
        /// The destination address.
        /// </value>
        public Ipv4Address DestinationAddress { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Calculates the checksum.
        /// </summary>
        /// <returns></returns>
        public int calculateChecksum()
        {
            int ret = 0;
            
            foreach(string octet in SourceAddress.Address.Split('.'))
            {
                ret += Int32.Parse(octet);
            }
            foreach (string octet in DestinationAddress.Address.Split('.'))
            {
                ret += Int32.Parse(octet);
            }
            return ret;
        }
        #endregion
    }
}
