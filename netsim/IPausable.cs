﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core
{
    /// <summary>
    /// Interface for all pausable items.
    /// </summary>
    /// <remarks>
    /// This is meant to be used when pause tool is actived.
    /// </remarks>
    interface IPausable
    {
        /// <summary>
        /// Pauses this instance.
        /// </summary>
        void Pause();
        /// <summary>
        /// Unpauses this instance.
        /// </summary>
        void Unpause();
    }
}
