﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core;
using System.Collections.ObjectModel;

namespace Gui
{
    /// <summary>
    /// Interaction logic for PacketView.xaml
    /// </summary>
    public partial class PacketView : UserControl
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="PacketView"/> class.
        /// </summary>
        /// <param name="packet">The packet.</param>
        public PacketView(Packet packet)
        {
            InitializeComponent();

            detailsWindow = new PacketViewDetailsWindow();
            detailsWindow.DataContext = Data;


            if ((packet as Layer1Packet).Data != null) //icmp; tcp, udp
            {
                if((packet as Layer1Packet).Data.Data!=null)
                {
                    Data.Add((packet as Layer1Packet).Data.Data);
                }
            }
          

            if ((packet as Layer1Packet).Data != null) //arp; ipv4
            {
                Data.Add((packet as Layer1Packet).Data);
            }
            
            Data.Add(packet);
        }
        #endregion

        #region Fields
        private PacketViewDetailsWindow detailsWindow;
        #endregion

        #region Properties
        /// <summary>
        /// The packet view data
        /// </summary>
        public ObservableCollection<Packet> Data = new ObservableCollection<Packet>();
        #endregion

        #region Methods
        public void Remove()
        {
            detailsWindow.Close();
        }
        #endregion

        #region Event Handlers
        private void OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            detailsWindow.Show();
            detailsWindow.Activate();
        }
        #endregion
    }
}
