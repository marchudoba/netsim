﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core;
using System.ComponentModel;
using System.Windows.Controls;
using System.Globalization;
using System.Windows;

namespace Gui
{
    /// <summary>
    /// View class for interface
    /// </summary>
    public class InterfaceView : INotifyPropertyChanged
    {
        #region Contructors
        /// <summary>
        /// Initializes a new instance of the <see cref="InterfaceView"/> class.
        /// </summary>
        /// <param name="interfac">The interfac.</param>
        public InterfaceView(Interface interfac)
        {
            Interface = interfac;
            if (Interface.IpAddress == null)
            {
                IpAddress = "";
                Mask = "";
            }
            else
            {
                IpAddress = Interface.IpAddress.Address;
                Mask = Interface.IpAddress.Mask;
            }
            if (Interface.DefaultGateway == null)
            {
                DefaultGateway = "";
            }
            else
            {
                DefaultGateway = Interface.DefaultGateway.Address;
            }
        }
        #endregion

        #region Fields
        string ipAddress;
        string mask;
        string defaultGateway;
        #endregion

        #region Properties
        /// <summary>
        /// Gets the interface.
        /// </summary>
        /// <value>
        /// The interface.
        /// </value>
        public Interface Interface { get; private set; }
        /// <summary>
        /// Gets or sets the ip address.
        /// </summary>
        /// <value>
        /// The ip address.
        /// </value>
        public string IpAddress
        {
            get
            {
                return ipAddress;
            }
            set
            {
                ipAddress = value;
                NotifyPropertyChanged("IpAddress");
            }
        }
        /// <summary>
        /// Gets or sets the mask.
        /// </summary>
        /// <value>
        /// The mask.
        /// </value>
        public string Mask
        {
            get
            {
                return mask;
            }
            set
            {
                mask = value;
                NotifyPropertyChanged("Mask");
            }
        }
        /// <summary>
        /// Gets or sets the default gateway.
        /// </summary>
        /// <value>
        /// The default gateway.
        /// </value>
        public string DefaultGateway
        {
            get
            {
                return defaultGateway;
            }
            set
            {
                defaultGateway = value;
                NotifyPropertyChanged("DefaultGateway");
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Updates the interface.
        /// </summary>
        public virtual void UpdateInterface()
        {
            if(IpAddress == "" || Mask == "")
            {
                return;
            }

            Ipv4Address ipAddress = new Ipv4Address(IpAddress, Mask);
            if(ipAddress.NetworkAddress == ipAddress.Address)
            {
                MessageBox.Show("Kombinace adresy a masky není validní. (Je to adresa sítě).", "Chyba", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else if(ipAddress.BroadcastAddress == ipAddress.Address)
            {
                MessageBox.Show("Kombinace adresy a masky není validní. (Je to adresa broadcastu).", "Chyba", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            Interface.IpAddress = new Ipv4Address(IpAddress, Mask);
            if (DefaultGateway != "")
            {
                Interface.DefaultGateway = new Ipv4Address(DefaultGateway, Mask);
            }

        }
        #endregion

        #region INotifyPropertyChange
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
        #endregion
    }

    public class MetricValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            int metric;
            if (int.TryParse((value as string), out metric) == false)
            {
                return new ValidationResult(false, "Vstup musí být číslo.");
            }
            else if(metric <1)
            {
                return new ValidationResult(false, "Vstup musí být kladné číslo.");
            }
            return new ValidationResult(true, null);
        }
    }

    public class PortValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            int metric;
            if (int.TryParse((value as string), out metric) == false)
            {
                return new ValidationResult(false, "Vstup musí být číslo.");
            }
            else if(metric <= 0 || metric > 65535)
            {
                return new ValidationResult(false, "Vstup musí být v rozsahu 0 - 65535.");
            }
            return new ValidationResult(true, null);
        }
    }
    public class IpAddressSlashMaskBitsValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string[] parts = (value as string).Split('/');
            int maskBits;
            if (parts.Count() != 2)
            {
                return new ValidationResult(false, "Vstup musí být ve formátu adresasítě/počet bitů masky. (10.0.0.0/8)");
            }
            if (int.TryParse(parts[1], out maskBits) == false)
            {
                return new ValidationResult(false, "Vstup musí být ve formátu adresasítě/počet bitů masky. (10.0.0.0/8)");
            }
            else
            {
                if (maskBits < 0 || maskBits > 32)
                {
                    return new ValidationResult(false, "Vstup musí být ve formátu adresasítě/počet bitů masky. (10.0.0.0/8)");
                }
            }

            List<string> testStringParts = new List<string>(parts[0].Split('.'));
            Byte oktet;

            if (testStringParts.Count != 4)
            {
                return new ValidationResult(false, "Vstup musí být ve formátu adresasítě/počet bitů masky. (10.0.0.0/8)");
            }
            for (int i = 0; i < 4; i++)
            {
                if (Byte.TryParse(testStringParts[i], out oktet) == false)
                {
                    return new ValidationResult(false, "Vstup musí být ve formátu adresasítě/počet bitů masky. (10.0.0.0/8)");
                }
                if (i == 0 && oktet == 0)
                {
                    return new ValidationResult(false, "Vstup musí být ve formátu adresasítě/počet bitů masky. (10.0.0.0/8)");
                }
            }
            return new ValidationResult(true, null);
        }
    }

    public class GatewayValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            List<string> testStringParts = new List<string>((value as string).Split('.'));
            Byte oktet;

            if (testStringParts.Count != 4)
            {
                return new ValidationResult(false, "Vstup musí být Ip adresa. (10.0.0.1)");
            }
            for (int i = 0; i < 4; i++)
            {
                if (Byte.TryParse(testStringParts[i], out oktet) == false)
                {
                    return new ValidationResult(false, "Vstup musí být Ip adresa. (10.0.0.1)");
                }
                if (i == 0 && oktet == 0)
                {
                    return new ValidationResult(false, "Vstup musí být Ip adresa. (10.0.0.1)");
                }
            }
            return new ValidationResult(true, null);
        }
    }
    public class IpAddressValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            List<string> testStringParts = new List<string>((value as string).Split('.'));
            Byte oktet;

            if (testStringParts.Count != 4)
            {
                return new ValidationResult(false, "Ip adresa musí obsahovat přesně 4 oktety.");
            }
            for (int i = 0; i < 4; i++)
            {
                if (Byte.TryParse(testStringParts[i], out oktet) == false)
                {
                    return new ValidationResult(false, "Každý oktet musí být v rozsahu 0 - 255");
                }
                if (i == 0 && oktet == 0)
                {
                    return new ValidationResult(false, "První oktet nesmí být 0.");
                }
            }
            return new ValidationResult(true, null);
        }
    }

    public class MaskValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            List<string> testStringParts = new List<string>((value as string).Split('.'));
            byte[] mask = new byte[4];
            Byte oktet;

            if (testStringParts.Count != 4)
            {
                return new ValidationResult(false, "Maska adresa musí obsahovat přesně 4 oktety.");
            }
            for (int i = 0; i < 4; i++)
            {
                if (Byte.TryParse(testStringParts[i], out oktet) == false)
                {
                    return new ValidationResult(false, "Každý oktet musí být v rozsahu 0 - 255");
                }
                if (i == 0 && oktet == 0)
                {
                    return new ValidationResult(false, "První oktet nesmí být 0.");
                }
                mask[i] = oktet;
            }

            bool continuous = true;
            foreach (var octet in mask)
            {
                int shift = 7;
                for (int i = 0; i < 8; i++)
                {
                    if (((octet >> shift) & 1) == 1)
                    {
                        if (continuous == false)
                        {
                            return new ValidationResult(false, "Maska neobsahuje souvislý blok jedniček.");
                        }
                    }
                    else
                    {
                        continuous = false;
                    }
                    shift--;
                }

            }
            return new ValidationResult(true, null);
        }
    }
}
