﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core
{
    /// <summary>
    /// Base class for transport protocol
    /// </summary>
    [Serializable]
    abstract class TransportProtocol : Protocol
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="TransportProtocol"/> class.
        /// </summary>
        public TransportProtocol(List<Interface> interfaces, Dictionary<Type, Protocol> protocols) : base(protocols)
        {
            this.interfaces = interfaces;
        }
        #endregion

        #region Fields
        protected List<Interface> interfaces;
        protected List<Socket> openSockets = new List<Socket>();
        #endregion

        #region Methods
        /// <summary>
        /// Sends the data on specified socket.
        /// </summary>
        public abstract void Send(Socket socket, object data);
        /// <summary>
        /// Receives the specified packet.
        /// </summary>
        /// <param name="packet">The packet.</param>
        /// <param name="sourceIp">The source ip.</param>
        /// <param name="destinationIp">The destination ip.</param>
        public abstract void Receive(Layer3Packet packet, Ipv4Address sourceIp, Ipv4Address destinationIp);
        /// <summary>
        /// Closes the specified socket.
        /// </summary>
        /// <param name="socket">The socket.</param>
        public abstract void Close(Socket socket);
        /// <summary>
        /// Listens on the specified port.
        /// </summary>
        /// <param name="port">The port.</param>
        /// <returns></returns>
        public abstract Socket Listen(int port);
        /// <summary>
        /// Opens new socket.
        /// </summary>
        /// <param name="destinationIp">The destination ip.</param>
        /// <param name="destinationPort">The destination port.</param>
        /// <returns></returns>
        public abstract Socket Open(Ipv4Address destinationIp, int destinationPort);
        /// <summary>
        /// Determines whether protocol is listening on the specified port.
        /// </summary>
        /// <param name="port">The port.</param>
        /// <returns></returns>
        public virtual bool IsListeningOn(int port)
        {
            foreach (Socket socket in openSockets)
            {
                if (socket.IsPassive && socket.LocalPort == port)
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Determines whether the specified port is open.
        /// </summary>
        /// <param name="port">The port.</param>
        /// <returns></returns>
        protected virtual bool isOpen(int port)
        {
            foreach (Socket socket in openSockets)
            {
                if (socket.LocalPort == port)
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Gets the free port number.
        /// </summary>
        /// <returns></returns>
        protected virtual int getFreePortNumber()
        {
            Random rand = new Random();
            int port;
            while(true)
            {
                port = rand.Next(1024, 65535);
                if(isOpen(port) == false)
                {
                    return port;
                }
            }
        }
        /// <summary>
        /// Adds the socket.
        /// </summary>
        /// <param name="socket">The socket.</param>
        protected void AddSocket(Socket socket)
        {
            openSockets.Add(socket);
        }
        /// <summary>
        /// Removes the socket.
        /// </summary>
        /// <param name="socket">The socket.</param>
        protected void RemoveSocket(Socket socket)
        {
            int index;
            if (openSockets.Contains(socket))
            {
                index = openSockets.FindIndex((x) => { return x == socket; });
                openSockets.RemoveAt(index);
            }
        }
        /// <summary>
        /// Finds the socket.
        /// </summary>
        /// <param name="localIp">The local ip.</param>
        /// <param name="localPort">The local port.</param>
        /// <param name="destinationIp">The destination ip.</param>
        /// <param name="destinationPort">The destination port.</param>
        /// <returns></returns>
        protected Socket FindSocket(Ipv4Address localIp, int localPort, Ipv4Address destinationIp, int destinationPort)
        {
            foreach(Socket socket in openSockets)
            {
                if (socket.IsPassive == false && socket.LocalAddress.Address == localIp.Address && socket.LocalPort == localPort && socket.DestinationAddress.Address == destinationIp.Address && socket.DestinationPort == destinationPort)
                {
                    return socket;
                }
            }
            return null;
        }
        /// <summary>
        /// Finds the socket.
        /// </summary>
        /// <param name="localIp">The local ip.</param>
        /// <param name="localPort">The local port.</param>
        /// <returns></returns>
        protected Socket FindSocket(Ipv4Address localIp, int localPort)
        {
            foreach (Socket socket in openSockets)
            {
                if (socket.IsPassive == true && socket.LocalAddress.Address == localIp.Address && socket.LocalPort == localPort)
                {
                    return socket;
                }
            }
            return null;
        }
        #endregion

        #region ISerializable
        /// <summary>
        /// Initializes a new instance of the <see cref="Ipv4Protocol" /> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="context">The context.</param>
        protected TransportProtocol(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            interfaces = info.GetValue("interfaces", typeof(List<Interface>)) as List<Interface>;
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("interfaces", interfaces);
        }
        #endregion
    }
}
