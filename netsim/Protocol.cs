﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core
{
    /// <summary>
    /// Represents a network protocol
    /// </summary>
    [Serializable]
    public abstract class Protocol : ISerializable, IPausable
    {
        #region Types
        /// <summary>
        /// Represents protocol type
        /// </summary>
        public enum Type { SwitchProtocol, Ethernet, Arp, Ipv4, Icmp, Udp, Tcp, PingProtocol, EchoClient, EchoServer }
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Protocol"/> class.
        /// </summary>
        /// <param name="protocols">The protocols.</param>
        public Protocol(Dictionary<Type, Protocol> protocols)
        {
            Protocols = protocols;
        }
        #endregion

        #region Fields
        protected Dictionary<Type, Protocol> Protocols;
        #endregion

        #region ISerializable
        List<string> protocolsList;
        protected Protocol(SerializationInfo info, StreamingContext context)
        {
            Protocols = new Dictionary<Protocol.Type, Protocol>();
            protocolsList = info.GetValue("protocolList", typeof(List<string>)) as List<string>;
            foreach (var key in protocolsList)
            {
                Protocols.Add((Protocol.Type)Enum.Parse(typeof(Protocol.Type), key), info.GetValue(key, typeof(Protocol)) as Protocol);
            }
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            protocolsList = new List<string>();
            foreach (var pair in Protocols)
            {
                protocolsList.Add(pair.Key.ToString());
                info.AddValue(pair.Key.ToString(), pair.Value);
            }
            info.AddValue("protocolList", protocolsList);
        }
        #endregion

        #region IPausable
        /// <summary>
        /// Pauses this instance.
        /// </summary>
        public virtual void Pause()
        { 
        }
        /// <summary>
        /// Unpauses this instance.
        /// </summary>
        public virtual void Unpause()
        {
        }
        #endregion
    
    
    }
}
