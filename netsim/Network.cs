﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core
{
    /// <summary>
    /// Represents entire network
    /// </summary>
    public class Network : IPausable
    {
        #region Fields
        List<Wire> wires = new List<Wire>();
        List<NetworkElement> networkElements = new List<NetworkElement>(); 
        #endregion
        #region Methods
        /// <summary>
        /// Connects the elements.
        /// </summary>
        /// <param name="sourceElement">The source element.</param>
        /// <param name="sourcePort">The source port.</param>
        /// <param name="destinationElement">The destination element.</param>
        /// <param name="destinationPort">The destination port.</param>
        /// <returns></returns>
        public Wire connectElements(NetworkElement sourceElement, string sourceInterface, NetworkElement destinationElement, string destinationInterface)
        {
            Interface sourceInt=null;
            Interface destinationInt=null;
            if (networkElements.Contains(sourceElement)==false)
            {
                throw new ArgumentException("SourceElement doesn't exist in the Network.");
            }
            if (networkElements.Contains(destinationElement)==false)
            {
                throw new ArgumentException("DestinationElement doesn't exist in the Network.");
            }
            foreach (var interfac in sourceElement.Interfaces)
            {
                if (interfac.Name==sourceInterface)
                {
                    if (interfac.Port!=null)
                    {
                        throw new ArgumentException("Source Interface " + sourceInterface + " is already used.");
                    }
                    sourceInt = interfac;
                }
            }
            foreach (var interfac in destinationElement.Interfaces)
            {
                if (interfac.Name == destinationInterface)
                {
                    if (interfac.Port != null)
                    {
                        throw new ArgumentException("Source Interface " + destinationInterface + " is already used.");
                    }
                    destinationInt = interfac;
                }
            }
            if (sourceInt==null || destinationInt==null)
            {
                throw new ArgumentNullException();
            }
            Wire wire=new Wire();
            wire.SourceInterface = sourceInt;
            wire.DestinationInterface = destinationInt;
            wires.Add(wire);
            sourceInt.Port = wire;
            destinationInt.Port = wire;
            return wire;
        }
        /// <summary>
        /// Adds the element into the network.
        /// </summary>
        public void AddElement(NetworkElement element)
        {
            networkElements.Add(element);
        }
        /// <summary>
        /// Adds the wire into the network.
        /// </summary>
        public void AddWire(Wire wire)
        {
            wires.Add(wire);
        }
        /// <summary>
        /// Adds the pc.
        /// </summary>
        /// <returns>Returns added Pc</returns>
        public Pc AddPc()
        {
            Pc pc = new Pc();
            networkElements.Add(pc);
            return pc;
        }
        /// <summary>
        /// Adds the router.
        /// </summary>
        /// <returns>Returns added router</returns>
        public Router AddRouter()
        {
            Router router = new Router();
            networkElements.Add(router);
            return router;
        }
        /// <summary>
        /// Adds the switch.
        /// </summary>
        /// <returns>Returns added switch</returns>
        public Switch AddSwitch()
        {
            Switch switchN = new Switch();
            networkElements.Add(switchN);
            return switchN;
        }
        /// <summary>
        /// Adds the wire.
        /// </summary>
        /// <returns>Returns added wire</returns>
        public Wire AddWire()
        {
            Wire wire = new Wire();
            wires.Add(wire);
            return wire;
        }
        /// <summary>
        /// Removes the element from the network.
        /// </summary>
        /// <param name="element">The element.</param>
        public void Remove(NetworkElement element)
        {
            if (networkElements.Contains(element) == false)
            {
                throw new ArgumentException();
            }
            foreach (var interfac in element.Interfaces)
            {
                if (interfac.Port != null)
                {
                    Remove(interfac.Port);
                }
            }
            networkElements.Remove(element);
        }
        /// <summary>
        /// Removes the wire from the network.
        /// </summary>
        /// <param name="wire">The wire.</param>
        public void Remove(Wire wire)
        {
            wire.DestinationInterface.Port = null;
            wire.SourceInterface.Port = null;
            wire.DestinationInterface = null;
            wire.SourceInterface = null;
            wires.Remove(wire);
        }
        #endregion

        #region IPausable
        /// <summary>
        /// Pauses this instance.
        /// </summary>
        public void Pause()
        {
            foreach(NetworkElement element in networkElements)
            {
                element.Pause();
            }
            foreach(Wire wire in wires)
            {
                wire.Pause();
            }
        }
        /// <summary>
        /// Unpauses this instance.
        /// </summary>
        public void Unpause()
        {
            foreach (NetworkElement element in networkElements)
            {
                element.Unpause();
            }
            foreach (Wire wire in wires)
            {
                wire.Unpause();
            }
        }
        #endregion
    }
}
