﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core
{
    /// <summary>
    /// Represents Ping protocol.
    /// </summary>
    /// <remarks>
    /// Uses Icmp to send echo messages.
    /// </remarks>
    [Serializable]
    sealed class PingProtocol : Protocol, IDeserializationCallback, ISerializable
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="PingProtocol"/> class.
        /// </summary>
        /// <param name="protocols">The protocols.</param>
        public PingProtocol(Dictionary<Protocol.Type, Protocol> protocols)
            : base(protocols)
        {
            (Protocols[Protocol.Type.Icmp] as IcmpProtocol).ReceivedEchoReply += OnEchoReplyReceived;
            (Protocols[Protocol.Type.Icmp] as IcmpProtocol).ReceivedDestinationUnreachable += OnDestinationUnreachableReceived;
        }
        #endregion

        #region Fields
        int identifier;
        int currentSequenceNumber;
        Ipv4Address destinationAddress;
        #endregion

        #region Events
        /// <summary>
        /// Occurs when echo reply is received.
        /// </summary>
        public event EventHandler<IcmpReceivedEventArgs> ReplyReceived;
        /// <summary>
        /// Occurs when destination unreachable received.
        /// </summary>
        public event EventHandler<IcmpReceivedEventArgs> DestinationUnreachableReceived;
        #endregion

        #region Methods
        /// <summary>
        /// Sends the ping.
        /// </summary>
        /// <param name="destinationAddress">The destination address.</param>
        public void SendPing(Ipv4Address destinationAddress)
        {
            identifier = new Random().Next();
            currentSequenceNumber = 0;
            this.destinationAddress = destinationAddress;
            SendNext();

        }
        /// <summary>
        /// Sends the next echo request message.
        /// </summary>
        private void SendNext()
        {
            (Protocols[Protocol.Type.Icmp] as IcmpProtocol).SendEchoRequest(destinationAddress, identifier, currentSequenceNumber);
        }
        private void OnReplyReceived(IcmpReceivedEventArgs e)
        {
            if(ReplyReceived != null)
            {
                ReplyReceived(this, new IcmpReceivedEventArgs(e.SourceAddress, e.Packet));
            }
        }
        private void OnDestinationUnreachableReceived(IcmpReceivedEventArgs e)
        {
            if(DestinationUnreachableReceived != null)
            {
                DestinationUnreachableReceived(this, e);
            }
        }
        #endregion

        #region EventHandlers
        private void OnEchoReplyReceived(object sender, IcmpReceivedEventArgs e)
        {
            if (e.SourceAddress.Address == destinationAddress.Address)
            {
                if (identifier == e.Packet.Identifier)
                {
                    if (currentSequenceNumber == e.Packet.SequenceNumber)
                    {
                        OnReplyReceived(e);
                        currentSequenceNumber++;
                        if(currentSequenceNumber<4)
                        {
                            SendNext();
                        }
                    }
                }
            }
        }
        private void OnDestinationUnreachableReceived(object sender, IcmpReceivedEventArgs e)
        {
            IcmpPacket packet = e.Packet;
            if(packet.OriginalPacket.Protocol == Type.Icmp)
            {
                if((packet.OriginalPacket.Data as IcmpPacket).Identifier == identifier)
                {
                    OnDestinationUnreachableReceived(e);
                    currentSequenceNumber++;
                    if (currentSequenceNumber < 4)
                    {
                        SendNext();
                    }
                }
            }
        }
        #endregion

        #region ISerializable
        private PingProtocol(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
        #endregion

        #region IDeserializationCallback
        void IDeserializationCallback.OnDeserialization(Object sender)
        {
            (Protocols[Protocol.Type.Icmp] as IcmpProtocol).ReceivedEchoReply += OnEchoReplyReceived;
            (Protocols[Protocol.Type.Icmp] as IcmpProtocol).ReceivedDestinationUnreachable += OnDestinationUnreachableReceived;
        }
        #endregion
    }
}
