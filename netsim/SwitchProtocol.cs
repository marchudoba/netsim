﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core
{
    /// <summary>
    /// Protocol that handles frame switching.
    /// </summary>
    [Serializable]
    sealed class SwitchProtocol : Protocol, ISerializable
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="SwitchProtocol"/> class.
        /// </summary>
        public SwitchProtocol(List<Interface> interfaces, Dictionary<Type, Protocol> protocols)
            : base(protocols)
        {
            this.interfaces = interfaces;
            Table = new SwitchTable();
        }
        #endregion

        #region Fields
        List<Interface> interfaces;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the table.
        /// </summary>
        SwitchTable Table { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Receives the specified packet.
        /// </summary>
        public void Receive(EthernetPacket packet, Interface interfac)
        {
            if (packet.SourceMac.IsBroadcast == false)
            {
                Table.Add(packet.SourceMac, interfac);
            }
            Interface destInterface;

            if (Table.LookUp(packet.DestinationMac, out destInterface) == true)
            {
                Send(packet, destInterface);
            }
            else
            {
                Flood(packet, interfac);
            }
            
        }
        /// <summary>
        /// Sends the specified packet.
        /// </summary>
        public void Send(EthernetPacket packet, Interface interfac)
        {
            if (interfac.Port == null)
            {
                return;
            }
            interfac.Send(packet);
        }
        /// <summary>
        /// Floods the specified packet on all interfaces except the it was received from.
        /// </summary>
        public void Flood(EthernetPacket packet, Interface interfac)
        {
            foreach (var interfa in interfaces)
            {
                if (interfac == interfa)
                {
                    continue;
                }
                Send(packet, interfa);
            }
        }

        #endregion

        #region ISerializable
        private SwitchProtocol(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Table = new SwitchTable();
            interfaces = info.GetValue("interfaces", typeof(List<Interface>)) as List<Interface>;
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("interfaces", interfaces);
        }
        #endregion

        #region IPausable
        /// <summary>
        /// Pauses this instance.
        /// </summary>
        public override void Pause()
        {
            base.Pause();
            Table.Pause();
        }
        /// <summary>
        /// Unpauses this instance.
        /// </summary>
        public override void Unpause()
        {
            base.Unpause();
            Table.Unpause();
        }
        #endregion
    }


    class SwitchTable : IPausable
    {
        #region Types
        /// <summary>
        /// Table entry
        /// </summary>
        /// <remarks>
        /// Entries are stored in hash table for limited period of time.
        /// </remarks>
        struct Entry
        {
            public Entry(Interface interfac, int timeToLive)
            {
                Interface = interfac;
                TimeToLive = timeToLive;//seconds
            }
            public Entry(Interface interfac)
            {
                Interface = interfac;
                TimeToLive = 30;//seconds
            }
            public Interface Interface;
            public int TimeToLive;
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="SwitchTable"/> class.
        /// </summary>
        public SwitchTable()
        {
            Table = new Dictionary<MacAddress, Entry>();
            timer.Interval = 1000;
            timer.Enabled = true;
            timer.Elapsed += delegate(object sender, System.Timers.ElapsedEventArgs e)
            {
                List<KeyValuePair<MacAddress, Entry>> list = new List<KeyValuePair<MacAddress, Entry>>(Table);
                List<MacAddress> toDelete = new List<MacAddress>();

                foreach (var entry in list)
                {

                    Table[entry.Key] = new Entry(entry.Value.Interface, entry.Value.TimeToLive - 1);
                    if (Table[entry.Key].TimeToLive <= 0)
                    {
                        toDelete.Add(entry.Key);
                    }
                }

                foreach (var key in toDelete)
                {
                    Table.Remove(key);
                }
            };
        }
        #endregion

        #region Fields
        Dictionary<MacAddress, Entry> Table { get; set; }
        PausableTimer timer = new PausableTimer();
        #endregion

        #region Methods
        /// <summary>
        /// Adds the specified address into table.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <param name="interfac">The interfac.</param>
        /// <param name="TimeToLive">The time to live.</param>
        public void Add(MacAddress address, Interface interfac, int TimeToLive = 120)
        {
            if (Table.ContainsKey(address))
            {
                Table[address] = new Entry(interfac, TimeToLive);
            }
            else
            {
                Table.Add(address, new Entry(interfac, TimeToLive));
            }

        }
        /// <summary>
        /// Looks up Mac address from table.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The interface behind which lies the key.</param>
        /// <returns>true if table contains the key</returns>
        public bool LookUp(MacAddress key, out Interface value)
        {
            if (Table.ContainsKey(key))
            {
                value = Table[key].Interface;
                return true;
            }
            value = null;
            return false;
        }
        /// <summary>
        /// Deletes the specified key from table.
        /// </summary>
        /// <param name="Key">The key.</param>
        public void Delete(MacAddress Key)
        {
            if (Table.ContainsKey(Key))
            {
                Table.Remove(Key);
            }
        }
        #endregion

        #region IPausable
        /// <summary>
        /// Pauses this instance.
        /// </summary>
        public void Pause()
        {
            timer.Paused = true;
        }
        /// <summary>
        /// Unpauses this instance.
        /// </summary>
        public void Unpause()
        {
            timer.Paused = false;
        }
        #endregion
    }
}
