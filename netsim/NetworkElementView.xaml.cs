﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Core;
using System.Diagnostics;
using System.Windows.Controls.Primitives;
using System.Runtime.Serialization;

namespace Gui
{
    /// <summary>
    /// Interaction logic for NetworkElementView.xaml
    /// </summary>
    [Serializable]
    public partial class NetworkElementView : IDeserializationCallback 
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkElementView"/> class.
        /// </summary>
        /// <param name="element">The element.</param>
        public NetworkElementView(NetworkElement element) //reflect possible changes to ISerializable Constructor
        {
            InitializeComponent();
            MouseDoubleClick += OnDoubleClick;
            Element = element;
            WireViews = new List<WireView>();
            SettingsWindowData = new ObservableCollection<object>();
            SettingsWindow = new NetworkElementSettingsWindow();
            SettingsWindow.DataContext = this;



            if (element is Pc)
            {
                SettingsWindowData.Add(new EchoClientView(Element.Stack as PcProtocolStack));
                SettingsWindowData.Add(new EchoServerView(Element.Stack as PcProtocolStack));
                SettingsWindowData.Add(new PingView(Element.Stack as PcProtocolStack));
                foreach (var inter in Element.Interfaces)
                {
                    SettingsWindowData.Add(new PcInterfaceView(inter));
                }
            }
            else if (element is Router)
            {
                SettingsWindowData.Add(new RouteTableView((element.Stack.Protocols[Protocol.Type.Ipv4] as Ipv4Protocol).Table));
                foreach (var inter in Element.Interfaces)
                {
                    SettingsWindowData.Add(new RouterInterfaceView(inter));
                }
            }
            else if (element is Core.Switch)
            {

            }
        }
        #endregion

        #region Fields
        Popup interfaceMenu;
        Point? onMouseMoveOriginalPosition = null;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the settings window data.
        /// </summary>
        /// <remarks>
        /// Data for double click window.
        /// </remarks>
        public ObservableCollection<object> SettingsWindowData { get; set; }
        /// <summary>
        /// Gets or sets the wire views connected to this instance.
        /// </summary>
        public List<WireView> WireViews { get; set; }
        /// <summary>
        /// Gets the element.
        /// </summary>
        /// <value>
        /// The element.
        /// </value>
        public NetworkElement Element { get; private set; }
        /// <summary>
        /// Gets or sets the settings window.
        /// </summary>
        public NetworkElementSettingsWindow SettingsWindow { get; set; }
        #endregion
        
        #region Methods
        /// <summary>
        /// Removes this instance from workplace.
        /// </summary>
        public override void Remove()
        {
            base.Remove();
            SettingsWindow.Close();
            while (WireViews.Count > 0)
            {
                WireViews[0].Remove();
            }
            App.Network.Remove(Element);
        }
        /// <summary>
        /// Opens the interface selection.
        /// </summary>
        public void OpenInterfaceSelection()
        {
            interfaceMenu = new Popup();
            MenuItem interfaceMenuItem;
            Grid itemGrid = new Grid();
            itemGrid.ColumnDefinitions.Add(new ColumnDefinition());

            int currentRow = 0;
            for (int i = 0; i < Element.Interfaces.Count; i++)
            {
                if (Element.Interfaces[i].Port != null)
                {
                    continue;
                }
                itemGrid.RowDefinitions.Add(new RowDefinition());
                interfaceMenuItem = new MenuItem();
                interfaceMenuItem.Click += delegate(object sender, RoutedEventArgs e)
                {
                    OnInterfaceSelected(new InterfaceSelectedEventArgs(((string)((MenuItem)sender).Header)));
                    interfaceMenu.IsOpen = false;
                    e.Handled = true;
                };
                interfaceMenuItem.Header = Element.Interfaces[i].Name;
                interfaceMenuItem.Background = Brushes.White;
                Grid.SetRow(interfaceMenuItem, currentRow++);
                Grid.SetColumn(interfaceMenuItem, 0);
                itemGrid.Children.Add(interfaceMenuItem);
            }
            itemGrid.RowDefinitions.Add(new RowDefinition());
            interfaceMenuItem = new MenuItem();
            interfaceMenuItem.Click += delegate(object sender, RoutedEventArgs e) { e.Handled = true; interfaceMenu.IsOpen = false; };
            interfaceMenuItem.Header = "Close";
            interfaceMenuItem.Background = Brushes.White;
            Grid.SetRow(interfaceMenuItem, currentRow);
            Grid.SetColumn(interfaceMenuItem, 0);
            itemGrid.Children.Add(interfaceMenuItem);

            interfaceMenu.Child = itemGrid;
            interfaceMenu.AllowsTransparency = true;
            interfaceMenu.PlacementTarget = this;
            interfaceMenu.IsOpen = true;
        }
        /// <summary>
        /// Closes the interface selection.
        /// </summary>
        public void CloseInterfaceSelection()
        {
            interfaceMenu.IsOpen = false;
            interfaceMenu = null;
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when interface is selected.
        /// </summary>
        public event EventHandler<InterfaceSelectedEventArgs> InterfaceSelected;
        #endregion

        #region EventHandlers
        protected void OnInterfaceSelected(InterfaceSelectedEventArgs e)
        {
            if (InterfaceSelected != null)
            {
                InterfaceSelected(this, e);
            }
        }
        protected override void OnSelected()
        {
            base.OnSelected();
            CaptureMouse();
            MouseLeftButtonUp += OnMouseLeftButtonUp;
            MouseMove += OnMouseMove;

        }
        protected void OnMouseMove(object sender, MouseEventArgs e)
        {
            Point workplacePos = e.GetPosition((App.Current.MainWindow as MainWindow).Workplace);
            if (onMouseMoveOriginalPosition == null)
            {
                onMouseMoveOriginalPosition = e.GetPosition(this);
            }
            Point newPosition = new Point(workplacePos.X - onMouseMoveOriginalPosition.Value.X, workplacePos.Y - onMouseMoveOriginalPosition.Value.Y);
            if (newPosition.X < 0)
            {
                SetValue(Canvas.LeftProperty, 0.0);
            }
            else if (newPosition.X > ((App.Current.MainWindow as MainWindow).Workplace.ActualWidth - ActualWidth))
            {
                SetValue(Canvas.LeftProperty, (App.Current.MainWindow as MainWindow).Workplace.ActualWidth - ActualWidth);
            }
            else
            {
                SetValue(Canvas.LeftProperty, newPosition.X);
            }
            if (newPosition.Y < 0)
            {
                SetValue(Canvas.TopProperty, 0.0);
            }
            else if (newPosition.Y > ((App.Current.MainWindow as MainWindow).Workplace.ActualHeight - ActualHeight))
            {
                SetValue(Canvas.TopProperty, (App.Current.MainWindow as MainWindow).Workplace.ActualHeight - ActualHeight);
            }
            else
            {
                SetValue(Canvas.TopProperty, newPosition.Y);
            }
            e.Handled = true;
        }
        protected void OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            onMouseMoveOriginalPosition = null;
            MouseLeftButtonUp -= OnMouseLeftButtonUp;
            MouseMove -= OnMouseMove;
            this.ReleaseMouseCapture();
        }
        private void OnDoubleClick(object sender, RoutedEventArgs e)
        {
            SettingsWindow.Show();
            SettingsWindow.Activate();
        }
        #endregion


        #region ISerializable
        protected NetworkElementView(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            Element = info.GetValue("Element", typeof(NetworkElement)) as NetworkElement;
            WireViews = info.GetValue("WireViews", typeof(List<WireView>)) as List<WireView>;

            Canvas.SetLeft(this, info.GetDouble("CanvasLeft"));
            Canvas.SetTop(this, info.GetDouble("CanvasTop"));
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("Element", Element);
            info.AddValue("WireViews", WireViews);
            info.AddValue("CanvasLeft", Canvas.GetLeft(this));
            info.AddValue("CanvasTop", Canvas.GetTop(this));
        }
        #endregion

        #region IDeserializationCallback
        public virtual void OnDeserialization(Object sender)
        {
            InitializeComponent();
            MouseDoubleClick += OnDoubleClick;

            SettingsWindowData = new ObservableCollection<object>();
            SettingsWindow = new NetworkElementSettingsWindow();
            SettingsWindow.DataContext = this;
            if (Element is Pc)
            {
                SettingsWindowData.Add(new EchoClientView(Element.Stack as PcProtocolStack));
                SettingsWindowData.Add(new EchoServerView(Element.Stack as PcProtocolStack));
                SettingsWindowData.Add(new PingView(Element.Stack as PcProtocolStack));
                foreach (var inter in Element.Interfaces)
                {
                    SettingsWindowData.Add(new PcInterfaceView(inter));
                }
            }
            else if (Element is Router)
            {
                SettingsWindowData.Add(new RouteTableView((Element.Stack.Protocols[Protocol.Type.Ipv4] as Ipv4Protocol).Table));
                foreach (var inter in Element.Interfaces)
                {
                    SettingsWindowData.Add(new RouterInterfaceView(inter));
                }
            }
            else if (Element is Core.Switch)
            {

            }

            onMouseMoveOriginalPosition = null;
        }
        #endregion
    }

    /// <summary>
    /// Event args for <see cref="NetworkElementView.InterfaceSelected"/>
    /// </summary>
    public class InterfaceSelectedEventArgs : EventArgs
    {
        public InterfaceSelectedEventArgs(string interfaceName)
        {
            Interface = interfaceName;
        }
        public String Interface { get; set; }
    }
}
