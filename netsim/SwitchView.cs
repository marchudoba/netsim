﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Runtime.Serialization;

namespace Gui
{
    /// <summary>
    /// View for <see cref="Core.Switch"/>
    /// </summary>
    [Serializable]
    class SwitchView : NetworkElementView
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="SwitchView"/> class.
        /// </summary>
        public SwitchView() : base(App.Network.AddSwitch())
        {
            ((Image)FindName("Icon")).Source = new BitmapImage(new Uri("pack://application:,,,/resources/switch.png"));
            ((Label)FindName("Label")).Content = "Switch";
        }
        #endregion

        #region ISerializable
        protected SwitchView(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
        #endregion

        #region IDeserializationCallback
        public override void OnDeserialization(Object sender)
        {
            base.OnDeserialization(sender);
            ((Image)FindName("Icon")).Source = new BitmapImage(new Uri("pack://application:,,,/resources/switch.png"));
            ((Label)FindName("Label")).Content = "Switch";
        }
        #endregion
    }
}
