﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core
{
    /// <summary>
    /// Represents ethernet protocol.
    /// </summary>
    [Serializable]
    sealed class EthernetProtocol : Protocol, ISerializable, IDeserializationCallback
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="EthernetProtocol"/> class.
        /// </summary>
        /// <param name="protocols">The protocols.</param>
        public EthernetProtocol(Dictionary<Type, Protocol> protocols) : base(protocols)
        {
            (protocols[Type.Arp] as ArpProtocol).Resolved += OnResolvedIp;
            waitingToResolveQueue = new Dictionary<Ipv4Address, List<Tuple<DateTime, Layer2Packet, Interface, Ipv4Address, Type>>>();
        }
        #endregion

        #region Fields
        Dictionary<Ipv4Address, List<Tuple<DateTime, Layer2Packet, Interface, Ipv4Address, Type>>> waitingToResolveQueue;
        #endregion

        #region Methods
        /// <summary>
        /// Receives the specified packet.
        /// </summary>
        /// <param name="packet">The packet.</param>
        /// <param name="interfac">The interfac.</param>
        public void Receive(EthernetPacket packet, Interface interfac)
        {
            if (packet.DestinationMac == interfac.Mac || packet.DestinationMac.IsBroadcast == true)
            {
                switch (packet.Ethertype)
                {
                    case Type.Arp:
                        (Protocols[Type.Arp] as ArpProtocol).Receive(packet.Data as ArpPacket, interfac);
                        break;
                    case Type.Ipv4:
                        (Protocols[Type.Ipv4] as Ipv4Protocol).Receive(packet.Data as Ipv4Packet);
                        break;
                }
            }
        }
        /// <summary>
        /// Sends the specified packet.
        /// </summary>
        /// <param name="packet">The packet.</param>
        /// <param name="interfac">The interfac.</param>
        /// <param name="nextHopAddress">The next hop address.</param>
        /// <param name="etherType">Type of the ether.</param>
        public void Send(Layer2Packet packet, Interface interfac, Ipv4Address nextHopAddress, Type etherType)
        {
            MacAddress destAddress = (Protocols[Type.Arp] as ArpProtocol).Resolve(nextHopAddress, interfac);
            if (destAddress == null)
            {
                AddToQueue(packet, interfac, nextHopAddress, etherType);
                return;
            }
            EthernetPacket frame = new EthernetPacket(destAddress, interfac.Mac, etherType, packet);
            interfac.Send(frame);
        }
        /// <summary>
        /// Sends the specified packet.
        /// </summary>
        /// <param name="packet">The packet.</param>
        /// <param name="interfac">The interfac.</param>
        /// <param name="destAddress">The dest address.</param>
        /// <param name="etherType">Type of the ether.</param>
        public void Send(Layer2Packet packet, Interface interfac, MacAddress destAddress, Type etherType)
        {
            EthernetPacket frame = new EthernetPacket(destAddress, interfac.Mac, etherType, packet);
            interfac.Send(frame);
        }
        /// <summary>
        /// Sends the specified packet with destination MAC FFFFFFFFFFFF.
        /// </summary>
        /// <param name="packet">The packet.</param>
        /// <param name="interfac">The interfac.</param>
        /// <param name="etherType">Type of the ether.</param>
        public void SendBroadcast(Layer2Packet packet, Interface interfac, Type etherType)
        {
            EthernetPacket frame = new EthernetPacket(MacAddress.Broadcast, interfac.Mac, etherType, packet);
            interfac.Send(frame);
        }
        private void AddToQueue(Layer2Packet packet, Interface interfac, Ipv4Address nextHopAddress, Type etherType)
        {
            Ipv4Address address = new Ipv4Address(nextHopAddress.Address + "/0");
            if (waitingToResolveQueue.ContainsKey(address))
            {
                KeyValuePair<Ipv4Address, List<Tuple<DateTime, Layer2Packet, Interface, Ipv4Address, Type>>> entry = waitingToResolveQueue.First((x) => x.Key.Equals(address));
                entry.Value.Add(Tuple.Create(DateTime.Now, packet, interfac, address, etherType));
                while(entry.Value.Count > 5)
                {
                    entry.Value.RemoveAt(0);
                }

            }
            else
            {
                List<Tuple<DateTime, Layer2Packet, Interface, Ipv4Address, Type>> entry = new List<Tuple<DateTime, Layer2Packet, Interface, Ipv4Address, Type>>();
                entry.Add(Tuple.Create(DateTime.Now, packet, interfac, address, etherType));
                waitingToResolveQueue.Add(address, entry);

            }
        }
        private void OnResolvedIp(object sender, ResolvedIpEventArgs e)
        {
            DateTime now = DateTime.Now;
            if (waitingToResolveQueue.ContainsKey(e.ResolvedIp) == false)
            {
                return;
            }
            KeyValuePair<Ipv4Address, List<Tuple<DateTime, Layer2Packet, Interface, Ipv4Address, Type>>> entry = waitingToResolveQueue.First((x) => x.Key.Equals(e.ResolvedIp));
            foreach(var tuple in entry.Value)
            {
                if((tuple.Item1-now).Seconds < 15)
                {
                    Send(tuple.Item2, tuple.Item3, e.Mac, tuple.Item5);
                }
            }
            entry.Value.Clear();
        }
        #endregion

        #region ISerializable
        private EthernetProtocol(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            waitingToResolveQueue = new Dictionary<Ipv4Address, List<Tuple<DateTime, Layer2Packet, Interface, Ipv4Address, Type>>>();
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
        #endregion

        #region IDeserializationCallback
        void IDeserializationCallback.OnDeserialization(object sender)
        {
            (Protocols[Type.Arp] as ArpProtocol).Resolved += OnResolvedIp;
        }
        #endregion

        #region IPausable
        DateTime paused;
        /// <summary>
        /// Pauses this instance.
        /// </summary>
        public override void Pause()
        {
            base.Pause();
            paused = DateTime.Now;
        }
        /// <summary>
        /// Unpauses this instance.
        /// </summary>
        public override void Unpause()
        {
            DateTime now = DateTime.Now;
            foreach(var entry in waitingToResolveQueue) //adjust age of entries in queue
            {
                for (int i = 0; i < entry.Value.Count;i++ )
                {
                    entry.Value[i] = new Tuple<DateTime, Layer2Packet, Interface, Ipv4Address, Type>(entry.Value[i].Item1 + (now - paused), entry.Value[i].Item2, entry.Value[i].Item3, entry.Value[i].Item4, entry.Value[i].Item5);
                }
            }
        }
        #endregion
    }
}
