﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core
{
    [Serializable]
    public class RouteTable : ISerializable
    {
        #region Types
        public enum Type { DirectlyConnected, Default, Manual };
        [Serializable]
        public class Entry
        {
            #region Constructors
            public Entry(Ipv4Address destinationNetwork, Interface outgoingInterface, Ipv4Address gateway, int metric, Type type)
            {
                DestinationNetwork = destinationNetwork;
                OutgoingInterface = outgoingInterface;
                Gateway = gateway;
                Metric = metric;
                Type = type;
            }
            #endregion

            #region Properties
            public Ipv4Address DestinationNetwork { get; set; }
            public Interface OutgoingInterface { get; set; }
            public Ipv4Address Gateway { get; set; }
            public int Metric { get; set; }
            public Type Type { get; set; }
            #endregion

            #region ISerializable
            protected Entry(SerializationInfo info, StreamingContext context)
            {
                DestinationNetwork = info.GetValue("DestinationNetwork", typeof(Ipv4Address)) as Ipv4Address;
                OutgoingInterface = info.GetValue("OutgoingInterface", typeof(Interface)) as Interface;
                Gateway = info.GetValue("Gateway", typeof(Ipv4Address)) as Ipv4Address; ;
                Metric = info.GetInt32("Metric");
                Type = (Type)Enum.Parse(typeof(Type), info.GetString("Type"));
            }

            public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
            {
                info.AddValue("DestinationNetwork", DestinationNetwork);
                info.AddValue("OutgoingInterface", OutgoingInterface);
                info.AddValue("Gateway", Gateway);
                info.AddValue("Metric", Metric);
                info.AddValue("Type", this.Type.ToString());
            }
            #endregion
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="RouteTable"/> class.
        /// </summary>
        /// <param name="interfaces">The interfaces.</param>
        public RouteTable(List<Interface> interfaces)
        {
            entryComparison = (x, y) =>
                {
                    if(x == null)
                    {
                        return -1;
                    }
                    if(y==null)
                    {
                        return 1;
                    }

                    if(x.DestinationNetwork.MaskLength > y.DestinationNetwork.MaskLength)
                    {
                        return -1;
                    }
                    else if (x.DestinationNetwork.MaskLength < y.DestinationNetwork.MaskLength)
                    {
                        return 1;
                    }
                    else
                    {
                        if(x.Metric<y.Metric)
                        {
                            return -1;
                        }
                        else if(y.Metric>x.Metric)
                        {
                            return 1;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                };
            
            Interfaces = interfaces;
            table = new List<Entry>();
            foreach (Interface interfac in interfaces)
            {
                interfac.PortChanged += OnPortChanged;
                interfac.IpAddressChanged += OnIpAddressChanged;
                interfac.DefaultGatewayChanged += OnDefaultGatewayChanged;
                AddEntry(interfac);
            }
        }
        #endregion

        #region Fields
        List<Entry> table;
        Comparison<Entry> entryComparison;
        #endregion

        #region Properties
        /// <summary>
        /// Gets the entries.
        /// </summary>
        /// <value>
        /// The entries.
        /// </value>
        public List<Entry> Entries
        {
            get
            {
                return table;
            }
        }

        /// <summary>
        /// Gets or sets the interfaces.
        /// </summary>
        public List<Interface> Interfaces { get; set; }
        #endregion

        #region Events
        public event EventHandler ContentChanged;
        #endregion

        #region Methods
        /// <summary>
        /// Adds the entry.
        /// </summary>
        /// <param name="entry">The entry.</param>
        public void AddEntry(Entry entry)
        {
            table.Add(entry);
            table.Sort(entryComparison);
            OnContentChanged();
        }
        /// <summary>
        /// Adds the entry.
        /// </summary>
        /// <param name="interfac">The interfac.</param>
        public void AddEntry(Interface interfac)
        {
            if (interfac.IsUp == false)
            {
                return;
            }
            if (interfac.IpAddress != null)
            {
                AddEntry(new Entry(new Ipv4Address(interfac.IpAddress.NetworkAddress, interfac.IpAddress.Mask), interfac, null, 0, Type.DirectlyConnected));


                if (interfac.DefaultGateway != null)
                {
                    AddEntry(new Entry(new Ipv4Address("0.0.0.0/0"), interfac, new Ipv4Address(interfac.DefaultGateway.ToString()), 100, Type.Default));
                }
            }
        }
        /// <summary>
        /// Finds the route.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <returns>entry if route exists else null</returns>
        public Entry FindRoute(Ipv4Address address)
        {
            for (int i = 0; i < table.Count;i++ )
            {
                if (table[i].DestinationNetwork.NetworkAddress == address.MaskWith(table[i].DestinationNetwork.Mask).NetworkAddress)
                {
                    return table[i];
                }
            }
            return null;
        }
        /// <summary>
        /// Removes the entry.
        /// </summary>
        /// <param name="entry">The entry.</param>
        public void RemoveEntry(Entry entry)
        {
            table.Remove(entry);
        }
        /// <summary>
        /// Removes the entries containing.
        /// </summary>
        /// <param name="interfac">The interfac.</param>
        public void RemoveEntriesContaining(Interface interfac)
        {
            for (int i = 0; i < table.Count;)
            {
                if (table[i].OutgoingInterface == interfac)
                {
                    table.RemoveAt(i);
                    continue;
                }
                i++;
            }
            table.Sort(entryComparison);
            OnContentChanged();
        }
        protected void OnContentChanged()
        {
            if (ContentChanged != null)
            {
                ContentChanged(this, EventArgs.Empty);
            }
        }
        #endregion

        #region Event Handlers
        private void OnIpAddressChanged(object sender, EventArgs e)
        {
            RemoveEntriesContaining(sender as Interface);
            AddEntry(sender as Interface);
        }
        private void OnDefaultGatewayChanged(object sender, EventArgs e)
        {
            RemoveEntriesContaining(sender as Interface);
            AddEntry(sender as Interface);
        }
        private void OnPortChanged(object sender, EventArgs e)
        {
            RemoveEntriesContaining(sender as Interface);
            AddEntry(sender as Interface);
        }
        #endregion

        #region ISerializable
        protected RouteTable(SerializationInfo info, StreamingContext context)
        {
            entryComparison = (x, y) =>
            {
                if (x == null)
                {
                    return -1;
                }
                if (y == null)
                {
                    return 1;
                }

                if (x.DestinationNetwork.MaskLength > y.DestinationNetwork.MaskLength)
                {
                    return -1;
                }
                else if (x.DestinationNetwork.MaskLength < y.DestinationNetwork.MaskLength)
                {
                    return 1;
                }
                else
                {
                    if (x.Metric < y.Metric)
                    {
                        return -1;
                    }
                    else if (y.Metric > x.Metric)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
            };

            Interfaces = info.GetValue("Interfaces", typeof(List<Interface>)) as List<Interface>;
            table = info.GetValue("Table", typeof(List<Entry>)) as List<Entry>;
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Interfaces", Interfaces);
            info.AddValue("Table", table);
        }
        #endregion
    }
}
