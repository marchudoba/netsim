﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;

namespace Core
{
    [Serializable]
    public class Ipv4Address
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Ipv4Address"/> class.
        /// </summary>
        /// <param name="ipAddressSlashMaskBits">The ip address slash mask bits. 10.0.0.1/8</param>
        /// <exception cref="System.ArgumentException">
        /// </exception>
        public Ipv4Address(string ipAddressSlashMaskBits)
        {
            string ipAddressDotsBetweenOctets = ipAddressSlashMaskBits.Split('/')[0];
            int maskBits = int.Parse(ipAddressSlashMaskBits.Split('/')[1]);
            int i = 0;
            foreach (var oktet in ipAddressDotsBetweenOctets.Split(new char[] { '.' }))
            {
                address[i++] = Byte.Parse(oktet);
            }
            i = 0;
            for (int maskIndex = 0; maskIndex < 4; maskIndex++)
            {
                for (i = 0; i < 8 && maskBits > 0; i++)
                {
                    mask[maskIndex] = (byte)(((byte)(mask[maskIndex] >> (byte)1)) | (byte)128);
                    maskBits--;
                }
            }


            foreach (var oktet in address)
            {
                if (oktet > 255)
                {
                    throw new ArgumentException();
                }
            }


            bool continuous = true;
            for (int oktetIndex = 0; oktetIndex < 4; oktetIndex++)
            {
                for (int shift = 0; shift < 8; shift++)
                {
                    if (((mask[oktetIndex] << shift) & 128) == 128)
                    {
                        if (continuous == false)
                        {
                            throw new ArgumentException();
                        }
                    }
                    else
                    {
                        continuous = false;
                    }
                }
            }
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Ipv4Address"/> class.
        /// </summary>
        /// <param name="ipAddressDotsBetweenOctets">The ip address dots between octets. 10.0.0.1</param>
        /// <param name="maskDotsBetweenOctets">The mask dots between octets. 255.0.0.0</param>
        /// <exception cref="System.ArgumentException">
        /// </exception>
        public Ipv4Address(string ipAddressDotsBetweenOctets, string maskDotsBetweenOctets)
        {
            int i = 0;
            foreach (var oktet in ipAddressDotsBetweenOctets.Split(new char[] { '.' }))
            {
                address[i++] = Byte.Parse(oktet);
            }
            i = 0;
            foreach (var oktet in maskDotsBetweenOctets.Split(new char[] { '.' }))
            {
                mask[i++] = Byte.Parse(oktet);
            }


            foreach (var oktet in address)
            {
                if (oktet > 255)
                {
                    throw new ArgumentException();
                }
            }

            bool continuous = true;
            for (int oktetIndex = 0; oktetIndex < 4; oktetIndex++)
            {
                for (int shift = 0; shift < 8; shift++)
                {
                    if (((mask[oktetIndex] << shift) & 128) == 128)
                    {
                        if (continuous == false)
                        {
                            throw new ArgumentException();
                        }
                    }
                    else
                    {
                        continuous = false;
                    }
                }
            }

        }

        #endregion

        #region Fields
        /// <summary>
        /// The address, address[0] is the most significant octet
        /// </summary>
        private byte[] address = new byte[4];
        /// <summary>
        /// The mask mask[0] is the most significant octet
        /// </summary>
        private byte[] mask = new byte[4];
        #endregion

        #region Properties
        /// <summary>
        /// Gets the address.
        /// </summary>
        /// <value>
        /// The address.
        /// </value>
        public string Address 
        {
            get
            {
                return address[0].ToString() + "." + address[1].ToString() + "." + address[2].ToString() + "." + address[3].ToString();
            }
        }
        /// <summary>
        /// Gets the length of the mask (lenght is determined by counting ones in binary representation of mask).
        /// </summary>
        /// <value>
        /// The length of the mask.
        /// </value>
        public int MaskLength
        {
            get
            {
                int ones=0;
                for (int octet = 0; octet < 4;octet++ )
                {
                    for (int bit = 0; bit < 8;bit++ )
                    {
                        if (((mask[octet] << bit) & 128) != 0)
                        {
                            ones++;
                        }
                    }
                }
                return ones;
            }
        }
        /// <summary>
        /// Gets the mask.
        /// </summary>
        /// <value>
        /// The mask.
        /// </value>
        public string Mask
        {
            get
            {
                return mask[0].ToString() + "." + mask[1].ToString() + "." + mask[2].ToString() + "." + mask[3].ToString();
            }
        }
        /// <summary>
        /// Gets the network address.
        /// </summary>
        /// <value>
        /// The network address.
        /// </value>
        public string NetworkAddress
        {
            get
            {
                return this.MaskWith(mask).Address;
            }
        }
        public string BroadcastAddress
        {
            get
            {
                byte[] retAddress = (byte[])address.Clone();
                for (int oktetIndex = 0; oktetIndex < 4; oktetIndex++)
                {
                    for (int i = 0; i < 8; i++)
                    {
                        if (((mask[oktetIndex] >> i) & 1) == 1)
                        {

                        }
                        else
                        {
                            retAddress[oktetIndex] = (byte)(retAddress[oktetIndex] | (byte)1 << i);
                        }
                    }
                }


                return new Ipv4Address(retAddress[0].ToString() + "." + retAddress[1].ToString() + "." + retAddress[2].ToString() + "." + retAddress[3].ToString(), Mask).Address;
            }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Sets the address.
        /// </summary>
        /// <param name="ipAddressSlashMaskBits">The ip address slash mask bits. 10.0.0.1/8</param>
        /// <exception cref="System.ArgumentException">
        /// </exception>
        public void SetAddress(string ipAddressSlashMaskBits)
        {
            string ipAddressDotsBetweenOctets = ipAddressSlashMaskBits.Split('/')[0];
            int maskBits = int.Parse(ipAddressSlashMaskBits.Split('/')[1]);
            int i = 0;
            foreach (var oktet in ipAddressDotsBetweenOctets.Split(new char[] { '.' }))
            {
                address[i++] = Byte.Parse(oktet);
            }
            i = 0;
            for (int maskIndex = 0; maskIndex < 4; maskIndex++)
            {
                for (i = 0; i < 8 && maskBits > 0; i++)
                {
                    mask[maskIndex] = (byte)(((byte)(mask[maskIndex] >> (byte)1)) | (byte)128);
                    maskBits--;
                }
            }


            bool continuous = true;
            for (int oktetIndex = 0; oktetIndex < 4; oktetIndex++)
            {
                for (int shift = 0; shift < 8; shift++)
                {
                    if (((mask[oktetIndex] << shift) & 128) == 128)
                    {
                        if (continuous == false)
                        {
                            throw new ArgumentException();
                        }
                    }
                    else
                    {
                        continuous = false;
                    }
                }
            }
        }
        /// <summary>
        /// Sets the address.
        /// </summary>
        /// <param name="ipAddressDotsBetweenOctets">The ip address dots between octets. 10.0.0.1</param>
        /// <param name="maskDotsBetweenOctets">The mask dots between octets. 255.0.0.0</param>
        /// <exception cref="System.ArgumentException">
        /// </exception>
        public void SetAddress(string ipAddressDotsBetweenOctets, string maskDotsBetweenOctets)
        {
            int i = 0;
            foreach (var oktet in ipAddressDotsBetweenOctets.Split(new char[] { '.' }))
            {
                address[i++] = Byte.Parse(oktet);
            }
            i = 0;
            foreach (var oktet in maskDotsBetweenOctets.Split(new char[] { '.' }))
            {
                mask[i++] = Byte.Parse(oktet);
            }


            bool continuous = true;
            for (int oktetIndex = 0; oktetIndex < 4; oktetIndex++)
            {
                for (int shift = 0; shift < 8; shift++)
                {
                    if (((mask[oktetIndex] << shift) & 128) == 128)
                    {
                        if (continuous == false)
                        {
                            throw new ArgumentException();
                        }
                    }
                    else
                    {
                        continuous = false;
                    }
                }
            }

        }
        /// <summary>
        /// Determines whether two addresses are on the same network.
        /// </summary>
        /// <param name="comp">Address to compare.</param>
        /// <returns>true if two addresses are on the same network, false otherwise</returns>
        public bool IsOnSameNetwork(Ipv4Address comp)
        {
            return NetworkAddress == comp.NetworkAddress;
        }

        /// <summary>
        /// Masks this IpAddress with given mask.
        /// </summary>
        /// <param name="mask">The mask.</param>
        /// <returns>masked address 192.168.1.1/24 masked with 255.0.0.0 gives 192.0.0.0/8</returns>
        public Ipv4Address MaskWith(string mask)
        {
            List<byte> maskBytes = new List<byte>();
            foreach (var oktet in mask.Split('.'))
            {
                maskBytes.Add(byte.Parse(oktet));
            }
            return MaskWith(maskBytes.ToArray());
        }
        private Ipv4Address MaskWith(byte[] mask)
        {
            byte[] retAddress = new byte[4];
            retAddress[0] = (byte)(address[0] & mask[0]);
            retAddress[1] = (byte)(address[1] & mask[1]);
            retAddress[2] = (byte)(address[2] & mask[2]);
            retAddress[3] = (byte)(address[3] & mask[3]);

            string retAddressString = "";
            retAddressString += retAddress[0].ToString() + ".";
            retAddressString += retAddress[1].ToString() + ".";
            retAddressString += retAddress[2].ToString() + ".";
            retAddressString += retAddress[3].ToString();
            string retMaskString = "";
            retMaskString += mask[0].ToString() + ".";
            retMaskString += mask[1].ToString() + ".";
            retMaskString += mask[2].ToString() + ".";
            retMaskString += mask[3].ToString();

            return new Ipv4Address(retAddressString, retMaskString);
        }


        public override bool Equals(object obj)
        {
            Ipv4Address comp = obj as Ipv4Address;
            if (comp == null)
            {
                return false;
            }
            if (Address == comp.Address && Mask==comp.Mask)
            {
                return true;
            }
            return false;
        }
        public bool Equals(Ipv4Address comp)
        {
            if (comp == null)
            {
                return false;
            }
            if (Address == comp.Address && Mask == comp.Mask)
            {
                return true;
            }
            return false;
        }
        public override int GetHashCode()
        {
            return (Address+Mask).GetHashCode();
        }   

        public override string ToString()
        {
            int ones = 0;
            int shift;
            foreach (var oktet in mask)
            {
                shift = 0;
                while (((oktet << shift) & 128) == 128)
                {
                    shift++;
                    ones++;
                    if (shift == 8)
                    {
                        break;
                    }
                }
            }
            return address[0].ToString() + "." + address[1].ToString() + "." + address[2].ToString() + "." + address[3].ToString() + "/" + ones.ToString();
        }
        #endregion

        #region ISerializable
        protected Ipv4Address(SerializationInfo info, StreamingContext context)
        {
            address = info.GetValue("address", typeof(byte[])) as byte[];
            mask = info.GetValue("mask", typeof(byte[])) as byte[];
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("address", address);
            info.AddValue("mask", mask);

        }
        #endregion
    }

}
