﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Media.Effects;
using System.Windows.Media;
using System.ComponentModel;

namespace Gui
{
    /// <summary>
    /// Represents work area of application
    /// </summary>
    public class NetworkViewModel : INotifyPropertyChanged
    {
        #region Types
        /// <summary>
        /// Represents which tool is currently being used.
        /// </summary>
        public enum State { Select, AddingPc, AddingRouter, AddingWire, AddingSwitch }
        #endregion
        #region Constructors
        public NetworkViewModel()
        {
            (App.Current.MainWindow as MainWindow).Loaded += delegate(object sender, RoutedEventArgs e) { (App.Current.MainWindow as MainWindow).Workplace.KeyUp += workplaceOnKeyUp; };
        }
        #endregion

        #region Fields
        State currentState;
        NetworkElementView addingWireSource;
        string addingWireSourceInterface;
        NetworkElementView addingWireDestination;
        string addingWireDestinationInterface;




        WorkplaceItem selection;
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the state of the current.
        /// </summary>
        public State CurrentState
        {
            get
            {
                return currentState;
            }
            set
            {
                MainWindow window = App.Current.MainWindow as MainWindow;
                switch (CurrentState)
                {
                    case State.AddingPc:
                    case State.AddingRouter:
                    case State.AddingSwitch:
                         window.Workplace.MouseLeftButtonUp -= AddingNetworkElementOnMouseLeftButtonUp;
                        break;
                    case State.AddingWire:
                        window.Workplace.MouseLeftButtonUp -= AddingWireOnMouseLeftButtonUp;
                        break;
                    case State.Select:
                        window.Workplace.MouseLeftButtonDown -= SelectOnMouseLeftButtonDown;
                        window.Workplace.ReleaseMouseCapture();
                        break;
                }
                switch (value)
                {
                    case State.AddingPc:
                    case State.AddingRouter:
                    case State.AddingSwitch:
                        window.Workplace.MouseLeftButtonUp += AddingNetworkElementOnMouseLeftButtonUp;
                        break;
                    case State.AddingWire:
                        addingWireSource = null;
                        addingWireSourceInterface = null;
                        addingWireDestination = null;
                        addingWireDestinationInterface = null;
                        window.Workplace.MouseLeftButtonUp += AddingWireOnMouseLeftButtonUp;
                        break;
                    case State.Select:
                        Keyboard.Focus(window.Workplace);
                        window.Workplace.MouseLeftButtonDown += SelectOnMouseLeftButtonDown;

                        break;
                }
                currentState = value;
                NotifyPropertyChanged("CurrentState");
            }
        }
        #endregion

        #region Methods
        #endregion

        #region Event handlers
        /// <summary>
        /// Handles key events for client area.
        /// </summary>
        private void workplaceOnKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                CurrentState = NetworkViewModel.State.Select;
                e.Handled = true;
            }
            else if (e.Key == Key.Delete)
            {
                if (selection != null)
                {
                    selection.Remove();
                    selection = null;
                }
                e.Handled = true;
            }
        }

        /// <summary>
        /// Handles mouse clicks to client area of main window when <see cref="State.AddingPc"/>, <see cref="State.AddingRouter"/> or <see cref="State.AddingSwitch"/> is <see cref="CurrentState"/>.
        /// </summary>
        private void AddingNetworkElementOnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            WorkplaceItem element = null;
            switch(CurrentState)
            {
                case State.AddingPc:
                    element = new PcViewModel();
                    break;
                case State.AddingRouter:
                    element = new RouterViewModel();
                    break;
                case State.AddingSwitch:
                    element=new SwitchViewModel();
                    break;
            }
            Canvas.SetTop(element, e.GetPosition((App.Current.MainWindow as MainWindow).Workplace).Y);
            Canvas.SetLeft(element, e.GetPosition((App.Current.MainWindow as MainWindow).Workplace).X);
            (App.Current.MainWindow as MainWindow).Workplace.Children.Add(element);
            e.Handled = true;
        }


        private void SetSourceInterface(object sender, InterfaceSelectedEventArgs e)
        {
            addingWireSourceInterface = e.Interface; ((NetworkElementView)sender).InterfaceSelected -= SetSourceInterface;
            Keyboard.Focus((App.Current.MainWindow as MainWindow).Workplace);
        }
        private void SetDestinationInterface(object sender, InterfaceSelectedEventArgs e)
        {
            ((NetworkElementView)sender).InterfaceSelected -= SetDestinationInterface;
            WireView wireView;
            addingWireDestinationInterface = e.Interface;
            wireView = new WireView(App.Network.connectElements(addingWireSource.Element, addingWireSourceInterface, addingWireDestination.Element, addingWireDestinationInterface));
            wireView.SourceElementView = addingWireSource;
            wireView.DestinationElementView = addingWireDestination;
            addingWireSource.WireViews.Add(wireView);
            addingWireDestination.WireViews.Add(wireView);
            (App.Current.MainWindow as MainWindow).Workplace.Children.Add(wireView);
            CurrentState = State.Select;
        }
        private void AddingWireOnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

            if (e.Source is NetworkElementView)
            {
                if (addingWireSource == null)
                {
                    addingWireSource = (NetworkElementView)e.Source;
                    addingWireSource.OpenInterfaceSelection();
                    addingWireSource.InterfaceSelected += SetSourceInterface;
                }
                else if (addingWireSource != null && addingWireSourceInterface == null)
                {
                    addingWireSource.CloseInterfaceSelection();
                    addingWireSource = (NetworkElementView)e.Source;
                    addingWireSource.OpenInterfaceSelection();
                    addingWireSource.InterfaceSelected += SetSourceInterface;
                }
                else if (addingWireSource == (NetworkElementView)e.Source)
                {

                }
                else if (addingWireDestination == null)
                {
                    addingWireDestination = (NetworkElementView)e.Source;
                    addingWireDestination.OpenInterfaceSelection();
                    addingWireDestination.InterfaceSelected += SetDestinationInterface;
                }
                else if (addingWireDestination != null && addingWireDestinationInterface == null)
                {
                    addingWireDestination.CloseInterfaceSelection();
                    addingWireDestination = (NetworkElementView)e.Source;
                    addingWireDestination.OpenInterfaceSelection();
                    addingWireDestination.InterfaceSelected += SetDestinationInterface;
                }
            }
            e.Handled = true;
        }

        /// <summary>
        /// Handler for MouseLeftButtonDown event.
        /// Selects <see cref="WorkplaceItem"/> under click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs"/> instance containing the event data.</param>
        private void SelectOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //deselect currently selected item
            if (selection != null)
            {
                selection.IsSelected = false;
            }


            if (e.Source is WorkplaceItem)
            {
                selection = (WorkplaceItem)e.Source;
                selection.IsSelected = true;
            }
            else
            {
                selection = null;
            }
            e.Handled = true;
        }

        #endregion

        #region INotifyPropertyChange
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
        #endregion
    }
}
