﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core
{
    /// <summary>
    /// Base class for all layer 2 protocol packet (<see cref="Ipv4packet"/>)
    /// </summary>
    public abstract class Layer2Packet : Packet
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Layer2Packet"/> class.
        /// </summary>
        /// <param name="data">The data.</param>
        public Layer2Packet(Layer3Packet data)
        {
            Data = data;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        public Layer3Packet Data { get; set; }
        #endregion
    }
}
