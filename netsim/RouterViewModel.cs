﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace Gui
{
    /// <summary>
    /// View for <see cref="Core.Router"/>
    /// </summary>
    class RouterViewModel : NetworkElementView
    {
        #region Constructors
        public RouterViewModel() : base(App.Network.AddRouter())
        {
            ((Image)FindName("Icon")).Source = new BitmapImage(new Uri("pack://application:,,,/resources/router.png"));
            ((Label)FindName("Label")).Content = "Router";
        }
        #endregion
    }
}
