﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Runtime.Serialization;

namespace Core
{
    /// <summary>
    /// Represents wire.
    /// </summary>
    [Serializable]
    public class Wire : ISerializable, IPausable
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Wire"/> class.
        /// </summary>
        public Wire()
        {
            toSourceTimer = new PausableTimer();
            toDestTimer = new PausableTimer();
            toDestElapsed = 0;
            toSourceElapsed = 0;


            toSourceTimer.AutoReset = true;
            toDestTimer.AutoReset = true;
            toSourceTimer.Interval = 10;
            toDestTimer.Interval = 10;
            toDestTimer.Elapsed += OntoDestTimerElapsed;
            toSourceTimer.Elapsed += OntoSourceTimerElapsed;

            toDestTimer.Enabled = true;
            toSourceTimer.Enabled = true;
            timerElapsedLock = new object();
        }
        #endregion


        #region Fields
        private object timerElapsedLock;
        const int transferTime = 3000;
        Queue<Layer1Packet> toSource = new Queue<Layer1Packet>();
        Queue<Layer1Packet> toDest = new Queue<Layer1Packet>();
        PausableTimer toSourceTimer;
        PausableTimer toDestTimer;
        int toSourceElapsed;
        int toDestElapsed;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the source interface.
        /// </summary>
        public Interface SourceInterface { get; set; }
        /// <summary>
        /// Gets or sets the destination interface.
        /// </summary>
        public Interface DestinationInterface { get; set; }
        /// <summary>
        /// Occurs periodically when packet is being sent.
        /// </summary>
        public event EventHandler<SendingPacketEventArgs> SendingPacket;
        /// <summary>
        /// Occurs when packet is sent.
        /// </summary>
        public event EventHandler<SendingPacketEventArgs> SentPacket;
        #endregion

        #region EventHandlers
        private void OntoDestTimerElapsed(object sender, ElapsedEventArgs e)
            {
                lock (timerElapsedLock)
                {
                    if (toDest.Count == 0 || DestinationInterface == null)
                    {
                        return;
                    }
                    if (toDestElapsed > transferTime)
                    {
                        OnSentPacket(new SendingPacketEventArgs(toDest.Peek(), true, false, transferTime, toDestElapsed));
                        DestinationInterface.Receive(toDest.Dequeue());
                        toDestElapsed = 0;
                    }
                    else
                    {
                        toDestElapsed += 10;
                        OnSendingPacket(new SendingPacketEventArgs(toDest.Peek(), true, false, transferTime, toDestElapsed));
                    }
                }
            }
            private void OntoSourceTimerElapsed(object sender, ElapsedEventArgs e)
            {
                lock (timerElapsedLock)
                {
                    if (toSource.Count == 0 || SourceInterface==null)
                    {
                        return;
                    }
                    if (toSourceElapsed > transferTime)
                    {
                        OnSentPacket(new SendingPacketEventArgs(toSource.Peek(), false, true, transferTime, toSourceElapsed));
                        SourceInterface.Receive(toSource.Dequeue());
                        toSourceElapsed = 0;
                    }
                    else
                    {
                        toSourceElapsed += 10;
                        OnSendingPacket(new SendingPacketEventArgs(toSource.Peek(), false, true, transferTime, toSourceElapsed));
                    }
                }
            }
        #endregion

        #region Methods
        /// <summary>
        /// Sends the specified packet.
        /// </summary>
        /// <param name="packet">The packet.</param>
        /// <param name="source">The source interface.</param>
        /// <exception cref="System.ArgumentException">Wrong interface used.</exception>
        public void Send(Layer1Packet packet, Interface source)
        {
            if (SourceInterface == source)
            {
                toDest.Enqueue(packet);
            }
            else if (DestinationInterface == source)
            {
                toSource.Enqueue(packet);
            }
            else
            {
                throw new ArgumentException("Wrong interface used.");
            }
        }
        private void OnSendingPacket(SendingPacketEventArgs e)
        {
            if (SendingPacket != null)
            {
                SendingPacket(this, e);
            }
        }
        private void OnSentPacket(SendingPacketEventArgs e)
        {
            if(SentPacket != null)
            {
                SentPacket(this, e);
            }
        }
        #endregion

        #region ISerializable
        protected Wire(SerializationInfo info, StreamingContext context)
        {
            SourceInterface = info.GetValue("SourceInterface", typeof(Interface)) as Interface;
            DestinationInterface = info.GetValue("DestinationInterface", typeof(Interface)) as Interface;

            toSourceTimer = new PausableTimer();
            toDestTimer = new PausableTimer();


            toDestElapsed = 0;
            toSourceElapsed = 0;


            toSourceTimer.AutoReset = true;
            toDestTimer.AutoReset = true;
            toSourceTimer.Interval = 10;
            toDestTimer.Interval = 10;
            toDestTimer.Elapsed += OntoDestTimerElapsed;
            toSourceTimer.Elapsed += OntoSourceTimerElapsed;
            toDestTimer.Enabled = true;
            toSourceTimer.Enabled = true;
            timerElapsedLock = new object();
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("SourceInterface", SourceInterface);
            info.AddValue("DestinationInterface", DestinationInterface);
        }
        #endregion

        #region IPausable
        public void Pause()
        {
            toDestTimer.Paused = true;
            toSourceTimer.Paused = true;
        }
        public void Unpause()
        {
            toDestTimer.Paused = false;
            toSourceTimer.Paused = false;
        }
        #endregion
    }
    /// <summary>
    /// Arguments for <see cref="Wire.SendingPacket"/> and <see cref="SentPacket"/> events.
    /// </summary>
    public class SendingPacketEventArgs : EventArgs
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="SendingPacketEventArgs"/> class.
        /// </summary>
        /// <param name="packet">The packet.</param>
        /// <param name="sourceToDest">if set to <c>true</c> packet sent to destination.</param>
        /// <param name="destToSource">if set to <c>true</c> packet sent to source.</param>
        /// <param name="transferTime">The transfer time.</param>
        public SendingPacketEventArgs(Packet packet, bool sourceToDest, bool destToSource, int transferTime, int progress)
        {
            Packet = packet;
            SourceToDest = sourceToDest;
            DestToSource = destToSource;
            TransferTime = transferTime;
            SignalTime = DateTime.Now;
            Progress = progress;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the packet.
        /// </summary>
        public Packet Packet { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether packet is sent to destination.
        /// </summary>
        public bool SourceToDest { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether packet is sent to source.
        /// </summary>
        public bool DestToSource { get; set; }
        /// <summary>
        /// Gets or sets the transfer time.
        /// </summary>
        public int TransferTime { get; set; }
        /// <summary>
        /// Gets or sets the time event occured.
        /// </summary>
        public DateTime SignalTime { get; set; }
        /// <summary>
        /// Gets or sets the current progress.
        /// </summary>
        public int Progress { get; set; }
        #endregion
    }
}
