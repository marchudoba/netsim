﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core
{
    /// <summary>
    /// Represents protocol stack for switch.
    /// </summary>
    [Serializable]
    class SwitchProtocolStack : ProtocolStack
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="SwitchProtocolStack"/> class.
        /// </summary>
        public SwitchProtocolStack(List<Interface> interfaces) : base(interfaces)
        {
            Protocols.Add(Protocol.Type.SwitchProtocol, new SwitchProtocol(interfaces, Protocols));
        }
        #endregion

        #region Methods
        /// <summary>
        /// Receives the specified packet.
        /// </summary>
        protected override void Receive(Packet packet, Interface interfac)
        {
            (Protocols[Protocol.Type.SwitchProtocol] as SwitchProtocol).Receive(packet as EthernetPacket, interfac);
        }
        #endregion

        #region ISerializable
        protected SwitchProtocolStack(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
        #endregion
    }
}
