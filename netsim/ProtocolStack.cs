﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core
{
    /// <summary>
    /// Represents protocol stack
    /// </summary>
    [Serializable]
    public abstract class ProtocolStack : ISerializable, IDeserializationCallback, IPausable
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ProtocolStack"/> class.
        /// </summary>
        public ProtocolStack(List<Interface> interfaces)
        {
            Interfaces = interfaces;
            Protocols = new Dictionary<Protocol.Type, Protocol>();

            foreach (var interfac in Interfaces)
            {
                interfac.ReceivedPacket += OnReceive;
            }
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the protocols.
        /// </summary>
        public Dictionary<Protocol.Type, Protocol> Protocols { get; set; }
        /// <summary>
        /// Gets or sets the interfaces.
        /// </summary>
        protected List<Interface> Interfaces { get; set; }
        #endregion

        #region Methods
        protected void OnReceive(object sender, PacketReceivedEventArgs e)
        {
            Receive(e.Packet, sender as Interface);
        }
        protected abstract void Receive(Packet packet, Interface interfac);
        #endregion
    
        #region ISerializable
        List<string> protocolsList;
        protected ProtocolStack(SerializationInfo info, StreamingContext context)
        {
            Interfaces = info.GetValue("Interfaces", typeof(List<Interface>)) as List<Interface>;

            Protocols = new Dictionary<Protocol.Type, Protocol>();

            protocolsList = info.GetValue("protocolList", typeof(List<string>)) as List<string>;
            foreach (var key in protocolsList)
            {
                Protocols.Add((Protocol.Type)Enum.Parse(typeof(Protocol.Type), key), info.GetValue(key, typeof(Protocol)) as Protocol);
            }
        
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Interfaces", Interfaces);
            protocolsList = new List<string>();
            foreach (var pair in Protocols)
            {
                protocolsList.Add(pair.Key.ToString());
                info.AddValue(pair.Key.ToString(), pair.Value);
            }
            info.AddValue("protocolList", protocolsList);
        }
        #endregion

        #region IDeserializationCallback
        void IDeserializationCallback.OnDeserialization(object sender)
        {
            foreach (var interfac in Interfaces)
            {
                interfac.ReceivedPacket += OnReceive;
            }
        }
        #endregion

        #region IPausable
        /// <summary>
        /// Pauses this instance.
        /// </summary>
        public void Pause()
        {
            foreach(var protocol in Protocols)
            {
                protocol.Value.Pause();
            }
        }
        /// <summary>
        /// Unpauses this instance.
        /// </summary>
        public void Unpause()
        {
            foreach (var protocol in Protocols)
            {
                protocol.Value.Unpause();
            }
        }
        #endregion

    }
}
