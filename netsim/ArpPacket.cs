﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core
{
    /// <summary>
    /// Represents arp packet
    /// </summary>
    class ArpPacket : Layer2Packet
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ArpPacket"/> class.
        /// </summary>
        /// <param name="opcode">The opcode.</param>
        /// <param name="senderMac">The sender mac.</param>
        /// <param name="senderIp">The sender ip.</param>
        /// <param name="targetMac">The target mac.</param>
        /// <param name="targetIp">The target ip.</param>
        public ArpPacket(ArpProtocol.Opcode opcode, MacAddress senderMac, Ipv4Address senderIp, MacAddress targetMac, Ipv4Address targetIp) : base(null)
        {
            Opcode = opcode;
            SenderMac = senderMac;
            SenderIp = senderIp;
            TargetMac = targetMac;
            TargetIp = targetIp;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the opcode.
        /// </summary>
        public ArpProtocol.Opcode Opcode { get; set; }
        /// <summary>
        /// Gets or sets the sender mac.
        /// </summary>
        public MacAddress SenderMac { get; set; }
        /// <summary>
        /// Gets or sets the sender ip.
        /// </summary>
        public Ipv4Address SenderIp { get; set; }
        /// <summary>
        /// Gets or sets the target mac.
        /// </summary>
        public MacAddress TargetMac { get; set; }
        /// <summary>
        /// Gets or sets the target ip.
        /// </summary>
        public Ipv4Address TargetIp { get; set; }
        #endregion
    }
}
