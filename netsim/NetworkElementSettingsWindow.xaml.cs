﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;

namespace Gui
{
    /// <summary>
    /// Interaction logic for NetworkElementSettingsWindow.xaml
    /// </summary>
    public partial class NetworkElementSettingsWindow : Window
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkElementSettingsWindow"/> class.
        /// </summary>
        public NetworkElementSettingsWindow()
        {
            InitializeComponent();
            Closing += OnClosing;
        }
        #endregion
       
        #region Event Handlers
        /// <summary>
        /// Called when the window is closing.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="CancelEventArgs"/> instance containing the event data.</param>
        private void OnClosing(object sender, CancelEventArgs e)
        {
            Hide();
            e.Cancel = true;
        }
        /// <summary>
        /// Called when listen button is clicked in <see cref="EchoServerView"/>
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        protected virtual void ListenOnClick(object sender, RoutedEventArgs e)
        {
            if (this.tabPanel.SelectedContent is EchoServerView)
            {
                (this.tabPanel.SelectedContent as EchoServerView).Listen();
            }
        }
        /// <summary>
        /// Called when send is clicked in  <see cref="EchoClientView"/>
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        protected virtual void SendOnClick(object sender, RoutedEventArgs e)
        {
            if (this.tabPanel.SelectedContent is EchoClientView)
            {
                (this.tabPanel.SelectedContent as EchoClientView).Send();
            }
        }
        /// <summary>
        /// Called when remove is clicked in <see cref="RouteTableView"/>
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        protected virtual void RemoveOnClick(object sender, RoutedEventArgs e)
        {
            if (this.tabPanel.SelectedContent is RouteTableView)
            {
                (this.tabPanel.SelectedContent as RouteTableView).RemoveEntry((sender as Button).Tag as Core.RouteTable.Entry);
            }
        }
        /// <summary>
        /// Called when add is clicked in <see cref="RouteTableView"/>
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        protected virtual void AddOnClick(object sender, RoutedEventArgs e)
        {
            if (this.tabPanel.SelectedContent is RouteTableView)
            {
                (this.tabPanel.SelectedContent as RouteTableView).AddEntry();
            }
        }
        /// <summary>
        /// Called when apply is clicked in interface view.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        protected virtual void ApplyOnClick(object sender, RoutedEventArgs e)
        {
            if (this.tabPanel.SelectedContent is InterfaceView)
            {
                (this.tabPanel.SelectedContent as InterfaceView).UpdateInterface();
            }
        }

        /// <summary>
        /// Called when send is clicked in ping view.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        protected virtual void OnPingSendClicked(object sender, RoutedEventArgs e)
        {
            if (this.tabPanel.SelectedContent is PingView)
            {
                (this.tabPanel.SelectedContent as PingView).Send();
            }
        }
        #endregion
    }

    /// <summary>
    /// Data template selector for <see cref="NetowrkElementSettingsWindow"/>
    /// </summary>
    public class ContentSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(Object item, DependencyObject container)
        {
            if (item is PcInterfaceView)
            {
                return (container as FrameworkElement).FindResource("pcInterfaceContentTemplate") as DataTemplate;
            }
            else if (item is RouterInterfaceView)
            {
                return (container as FrameworkElement).FindResource("routerInterfaceContentTemplate") as DataTemplate;
            }
            else if (item is RouteTableView)
            {
                return (container as FrameworkElement).FindResource("routeTableContentTemplate") as DataTemplate;
            }
            else if (item is PingView)
            {
                return (container as FrameworkElement).FindResource("pingContentTemplate") as DataTemplate;
            }
            else if (item is EchoClientView)
            {
                return (container as FrameworkElement).FindResource("echoClientViewContentTemplate") as DataTemplate;
            }
            else if (item is EchoServerView)
            {
                return (container as FrameworkElement).FindResource("echoServerViewContentTemplate") as DataTemplate;
            }
            else
            {
                throw new ArgumentException("No data template for " + item.ToString());
            }
        }
    }
    /// <summary>
    /// Data template selector for <see cref="NetworkElementSettingsWindow"/>
    /// </summary>
    public class ItemSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(Object item, DependencyObject container)
        {
            if (item is InterfaceView)
            {
                return (container as FrameworkElement).FindResource("interfaceItemTemplate") as DataTemplate;
            }
            else if (item is RouteTableView)
            {
                return (container as FrameworkElement).FindResource("routeTableItemTemplate") as DataTemplate;
            }
            else if (item is PingView)
            {
                return (container as FrameworkElement).FindResource("pingItemTemplate") as DataTemplate;
            }
            else if (item is EchoClientView)
            {
                return (container as FrameworkElement).FindResource("echoClientViewItemTemplate") as DataTemplate;

            }
            else if (item is EchoServerView)
            {
                return (container as FrameworkElement).FindResource("echoServerViewItemTemplate") as DataTemplate;

            }
            else
            {
                throw new ArgumentException("No data template for " + item.ToString());
            }
        }
    }
}
