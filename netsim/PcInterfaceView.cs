﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core;

namespace Gui
{
    /// <summary>
    /// View for pc interface.
    /// </summary>
    class PcInterfaceView : InterfaceView
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="PcInterfaceView"/> class.
        /// </summary>
        /// <param name="interfac">The interfac.</param>
        public PcInterfaceView(Interface interfac)
            : base(interfac)
        {

        }
        #endregion
    }
}
