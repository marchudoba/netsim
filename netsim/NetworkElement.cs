﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core
{
    /// <summary>
    /// Base class for network elements
    /// </summary>
    [Serializable]
    public class NetworkElement : ISerializable, IPausable
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkElement"/> class.
        /// </summary>
        /// <param name="interfaceCount">The interface count.</param>
        public NetworkElement(int interfaceCount)
        {
            Interfaces = new List<Interface>();
            for (int i = 0; i < interfaceCount; i++)
            {
                Interfaces.Add(new Interface("fa0/"+i.ToString()));
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the interfaces.
        /// </summary>
        public List<Interface> Interfaces { get; private set; }
        /// <summary>
        /// Gets the protocol stack.
        /// </summary>
        public ProtocolStack Stack { get; protected set; }
        #endregion

        #region ISerializable
        protected NetworkElement(SerializationInfo info, StreamingContext context)
        {
            Interfaces = info.GetValue("Interfaces", typeof(List<Interface>)) as List<Interface>;
            Stack = info.GetValue("Stack", typeof(ProtocolStack)) as ProtocolStack;
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Interfaces", Interfaces);
            info.AddValue("Stack", Stack);
        }
        #endregion

        #region IPausable
        /// <summary>
        /// Pauses this instance.
        /// </summary>
        public void Pause()
        {
            Stack.Pause();
        }
        /// <summary>
        /// Unpauses this instance.
        /// </summary>
        public void Unpause()
        {
            Stack.Unpause();
        }
        #endregion
    }
}
