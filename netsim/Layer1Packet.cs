﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core
{
    /// <summary>
    /// Base class for all layer 1 protocol packets ( <see cref="EthernetPacket"/>)
    /// </summary>
    public abstract class Layer1Packet : Packet
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Layer1Packet"/> class.
        /// </summary>
        /// <param name="data">The data.</param>
        public Layer1Packet(Layer2Packet data)
        {
            Data = data;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        public Layer2Packet Data { get; set; }
        #endregion
    }
}
