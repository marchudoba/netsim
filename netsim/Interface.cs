﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core
{
    /// <summary>
    /// Represents interface
    /// </summary>
    [Serializable]
    public class Interface : ISerializable
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Interface"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public Interface(string name)
        {
            Name = name;
            IpAddress = null;
            Mac = new MacAddress();
            DefaultGateway = null;
        }
        #endregion


        #region Fields
        Ipv4Address ipAddress;
        Ipv4Address defaultGateway;
        Wire port;
        #endregion

        #region Properties
        /// <summary>
        /// The name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets the ip address.
        /// </summary>
        public Ipv4Address IpAddress
        {
            get
            {
                return ipAddress;
            }
            set
            {
                ipAddress = value;
                OnIpAddressChanged();
            }
        }
        /// <summary>
        /// Gets the default gateway.
        /// </summary>
        public Ipv4Address DefaultGateway
        {
            get
            {
                return defaultGateway;
            }
            set
            {
                defaultGateway = value;
                OnDefaultGatewayChanged();
            }
        }
        /// <summary>
        /// Gets the mac.
        /// </summary>
        public MacAddress Mac { get; private set; }
        /// <summary>
        /// Gets or sets the port.
        /// </summary>
        public Wire Port
        {
            get
            {
                return port;
            }
            set
            {
                port = value;
                OnPortChanged();
            }
        }
        /// <summary>
        /// Gets a value indicating whether the unterface is up.
        /// </summary>
        /// <remarks>
        /// This indicates whether the interface has a wire connected to its port and have an ip address set.
        /// </remarks>
        /// <value>
        ///   <c>true</c> if interface is up; otherwise, <c>false</c>.
        /// </value>
        public bool IsUp
        {
            get
            {
                if(Port!=null && IpAddress != null)
                {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Occurs when packet is received.
        /// </summary>
        public event EventHandler<PacketReceivedEventArgs> ReceivedPacket;
        /// <summary>
        /// Occurs when ip address changed.
        /// </summary>
        public event EventHandler IpAddressChanged;
        /// <summary>
        /// Occurs when default gateway changed.
        /// </summary>
        public event EventHandler DefaultGatewayChanged;
        /// <summary>
        /// Occurs when port changed.
        /// </summary>
        public event EventHandler PortChanged;
        #endregion

        #region Methods
        protected void OnIpAddressChanged()
        {
            if (IpAddressChanged != null)
            {
                IpAddressChanged(this, EventArgs.Empty);
            }
        }
        protected void OnDefaultGatewayChanged()
        {
            if (DefaultGatewayChanged != null)
            {
                DefaultGatewayChanged(this, EventArgs.Empty);
            }
        }
        protected void OnPortChanged()
        {
            if (PortChanged != null)
            {
                PortChanged(this, EventArgs.Empty);
            }
        }
        protected void OnReceivedPacket(PacketReceivedEventArgs e)
        {
            if (ReceivedPacket != null)
            {
                ReceivedPacket(this, e);
            }
        }
        public void Receive(Layer1Packet packet)
        {
            if(IsUp)
            {
                OnReceivedPacket(new PacketReceivedEventArgs(packet));
            }
        }
        /// <summary>
        /// Sends the specified packet.
        /// </summary>
        /// <param name="packet">The packet.</param>
        public void Send(Layer1Packet packet)
        {
            Port.Send(packet, this);
        }
        #endregion

        #region ISerializable
        protected Interface(SerializationInfo info, StreamingContext context)
        {
            Name = info.GetString("Name");
            IpAddress = info.GetValue("IpAddress", typeof(Ipv4Address)) as Ipv4Address;
            DefaultGateway = info.GetValue("DefaultGateway", typeof(Ipv4Address)) as Ipv4Address;
            Mac = info.GetValue("Mac", typeof(MacAddress)) as MacAddress;
            Port = info.GetValue("Port", typeof(Wire)) as Wire;
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", Name);
            info.AddValue("IpAddress", IpAddress);
            info.AddValue("DefaultGateway", DefaultGateway);
            info.AddValue("Mac", Mac);
            info.AddValue("Port", Port);
        }
        #endregion
    }

    /// <summary>
    /// Event args for <see cref="Interface.ReceivedPacket"/>
    /// </summary>
    public class PacketReceivedEventArgs : EventArgs
    {
        #region Constructors
        public PacketReceivedEventArgs(Layer1Packet packet)
        {
            Packet = packet;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the packet.
        /// </summary>
        /// <value>
        /// The packet.
        /// </value>
        public Layer1Packet Packet { get; set; }
        #endregion
    }
}
