﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using Core;

namespace Gui
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region Contructors
        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            Network = new Network();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the network.
        /// </summary>
        /// <value>
        /// The network.
        /// </value>
        static public Network Network { get; set; }
        /// <summary>
        /// Gets or sets the status bar.
        /// </summary>
        /// <value>
        /// The status bar.
        /// </value>
        static public StatusBar StatusBar { get; set; }
        /// <summary>
        /// Gets or sets the debug.
        /// </summary>
        /// <value>
        /// The debug.
        /// </value>
        static public Label Debug { get; set; }
        #endregion

        
    }
}
