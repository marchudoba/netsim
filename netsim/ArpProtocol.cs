﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Timers;

namespace Core
{
    /// <summary>
    /// Represents Arp protocol 
    /// </summary>
    [Serializable]
    sealed class ArpProtocol : Protocol, ISerializable
    {
        #region Types
        /// <summary>
        /// Represents Opcode field
        /// </summary>
        public enum Opcode { Request, Reply }
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ArpProtocol"/> class.
        /// </summary>
        /// <param name="protocols">The protocols.</param>
        public ArpProtocol(Dictionary<Type, Protocol> protocols)
            : base(protocols)
        {
            Table = new ArpTable();
            requestsSent = new Dictionary<string, Timer>();
        }
        #endregion

        #region Fields
        Dictionary<string, Timer> requestsSent;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the arp table.
        /// </summary>
        /// <value>
        /// The table.
        /// </value>
        ArpTable Table { get; set; }
        #endregion

        #region Events
        public event EventHandler<ResolvedIpEventArgs> Resolved;
        #endregion

        #region Methods
        /// <summary>
        /// Resolves the specified address.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <param name="interfac">The interfac.</param>
        /// <returns></returns>
        public MacAddress Resolve(Ipv4Address address, Interface interfac)
        {
            MacAddress resolved = null;

            if (Table.LookUp(address, out resolved) == false)
            {
                Send(new ArpPacket(Opcode.Request, interfac.Mac, interfac.IpAddress, MacAddress.Broadcast, address), interfac, MacAddress.Broadcast);
            }


            return resolved;
        }
        /// <summary>
        /// Receives the specified packet.
        /// </summary>
        /// <param name="packet">The packet.</param>
        /// <param name="receivingInterface">The receiving interface.</param>
        public void Receive(ArpPacket packet, Interface receivingInterface)
        {
            Table.Add(packet.SenderIp, packet.SenderMac);
            OnResolvedIp(packet.SenderIp, packet.SenderMac);
            if (packet.Opcode == Opcode.Request && packet.TargetIp.Address ==  receivingInterface.IpAddress.Address)
            {
                Send(new ArpPacket(Opcode.Reply, receivingInterface.Mac, new Ipv4Address(receivingInterface.IpAddress.Address+"/0"), packet.SenderMac, packet.SenderIp), receivingInterface, packet.SenderMac);
            }
        }
        /// <summary>
        /// Sends the specified packet.
        /// </summary>
        /// <param name="packet">The packet.</param>
        /// <param name="interfac">The interfac.</param>
        /// <param name="destAddress">The dest address.</param>
        private void Send(ArpPacket packet, Interface interfac, MacAddress destAddress)
        {
            if(packet.Opcode==Opcode.Request)
            {
                if(requestsSent.ContainsKey(packet.TargetIp.Address))
                {
                    if(requestsSent[packet.TargetIp.Address].Enabled)
                    {
                        return;
                    }
                    else
                    {
                        requestsSent[packet.TargetIp.Address].Enabled = true;
                    }
                    
                }
                else
                {
                    Timer timer = new Timer();
                    timer.Interval = 7000;
                    timer.AutoReset = false;
                    timer.Enabled = true;
                    requestsSent.Add(packet.TargetIp.Address, timer); 
                }
                   
            }
            
            (Protocols[Type.Ethernet] as EthernetProtocol).Send(packet, interfac, destAddress, Protocol.Type.Arp);
        }

        private void OnResolvedIp(Ipv4Address resolvedIp, MacAddress Mac)
        {
            if(Resolved != null)
            {
                Resolved(this, new ResolvedIpEventArgs(new Ipv4Address(resolvedIp.Address+"/0"), Mac));
            }
        }
        #endregion

        #region ISerializable
        private ArpProtocol(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            Table = new ArpTable();
            requestsSent = new Dictionary<string, Timer>();
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
        #endregion

        #region IPausable
        /// <summary>
        /// Pauses this instance.
        /// </summary>
        public override void Pause()
        {
            base.Pause();
            Table.Pause();
        }
        /// <summary>
        /// Unpauses this instance.
        /// </summary>
        public override void Unpause()
        {
            base.Unpause();
            Table.Unpause();
        }
        #endregion
    }

    /// <summary>
    /// Represents arp table
    /// </summary>
    class ArpTable : IPausable
    {
        #region Types
        /// <summary>
        /// Represents entry in arp table
        /// </summary>
        struct Entry
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Entry"/> struct.
            /// </summary>
            /// <param name="address">The address.</param>
            /// <param name="timeToLive">The time to live.</param>
            public Entry(MacAddress address, int timeToLive)
            {
                Address = address;
                TimeToLive = timeToLive;//seconds
            }
            /// <summary>
            /// Initializes a new instance of the <see cref="Entry"/> struct.
            /// </summary>
            /// <param name="address">The address.</param>
            public Entry(MacAddress address)
            {
                Address = address;
                TimeToLive = 120;//seconds
            }
            /// <summary>
            /// The address
            /// </summary>
            public MacAddress Address;
            /// <summary>
            /// The time to live
            /// </summary>
            public int TimeToLive;
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ArpTable"/> class.
        /// </summary>
        public ArpTable()
        {
            Table = new Dictionary<Ipv4Address, Entry>();
            timer.Interval = 1000;
            timer.Enabled = false;
            timer.Elapsed += delegate(object sender, System.Timers.ElapsedEventArgs e)
            {
                lock (tableLock)
                {
                    List<KeyValuePair<Ipv4Address, Entry>> list = new List<KeyValuePair<Ipv4Address, Entry>>(Table);
                    List<Ipv4Address> toDelete = new List<Ipv4Address>();


                    foreach (var entry in list)
                    {

                        Table[entry.Key] = new Entry(entry.Value.Address, entry.Value.TimeToLive - 1);
                        
                        if (Table[entry.Key].TimeToLive <= 0)
                        {
                            toDelete.Add(entry.Key);
                        }
                    }

                    foreach (var key in toDelete)
                    {
                        Table.Remove(key);
                    }
                }
            };
        }
        #endregion

        #region Fields
        private object tableLock = new Object();
        Dictionary<Ipv4Address, Entry> Table { get; set; }
        PausableTimer timer = new PausableTimer();
        #endregion

        #region Methods
        /// <summary>
        /// Adds the specified ip address.
        /// </summary>
        /// <param name="IpAddress">The ip address.</param>
        /// <param name="MacAddress">The mac address.</param>
        /// <param name="TimeToLive">The time to live.</param>
        public void Add(Ipv4Address IpAddress, MacAddress MacAddress, int TimeToLive = 30)
        {
            Ipv4Address key = new Ipv4Address(IpAddress.Address + "/0");
            lock (tableLock)
            {
                if (Table.ContainsKey(key))
                {
                    Table[key] = new Entry(MacAddress, TimeToLive);
                }
                else
                {
                    Table.Add(key, new Entry(MacAddress, TimeToLive));
                }
            }

        }
        /// <summary>
        /// Looks up.
        /// </summary>
        /// <param name="IpAddress">The ip address.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public bool LookUp(Ipv4Address IpAddress, out MacAddress value)
        {
            Ipv4Address key = new Ipv4Address(IpAddress.Address + "/0");
            lock (tableLock)
            {
                if (Table.ContainsKey(key))
                {
                    value = Table[key].Address;
                    return true;
                }
            }
            value = null;
            return false;
        }
        /// <summary>
        /// Deletes the specified ip address.
        /// </summary>
        /// <param name="IpAddress">The ip address.</param>
        public void Delete(Ipv4Address IpAddress)
        {
            Ipv4Address key = new Ipv4Address(IpAddress.Address + "/0");
            lock (tableLock)
            {
                if (Table.ContainsKey(key))
                {
                    Table.Remove(key);
                }
            }
        }
        #endregion

        #region IPausable
        /// <summary>
        /// Pauses this instance.
        /// </summary>
        public void Pause()
        {
            timer.Paused = true;
        }
        /// <summary>
        /// Unpauses this instance.
        /// </summary>
        public void Unpause()
        {
            timer.Paused = false;
        }
        #endregion
    }

    class ResolvedIpEventArgs : EventArgs
    {
        #region Constructors
        public ResolvedIpEventArgs(Ipv4Address resolvedIp, MacAddress mac)
        {
            ResolvedIp = resolvedIp;
            Mac = mac;
        }
        #endregion

        #region Properties
        public Ipv4Address ResolvedIp { get; private set; }
        public MacAddress Mac { get; private set; }
        #endregion
    }
}
