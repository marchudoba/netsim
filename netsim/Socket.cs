﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core
{
    class Socket
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Socket"/> class.
        /// </summary>
        /// <param name="localIp">The local ip.</param>
        /// <param name="localPort">The local port.</param>
        /// <param name="destinationIp">The destination ip.</param>
        /// <param name="destinationPort">The destination port.</param>
        public Socket(Ipv4Address localIp, int localPort, Ipv4Address destinationIp, int destinationPort)
        {
            LocalAddress = localIp;
            LocalPort = localPort;
            DestinationAddress = destinationIp;
            DestinationPort = destinationPort;
            sendBuffer = "";
            receiveBuffer = "";
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Socket"/> class.
        /// </summary>
        /// <param name="localIp">The local ip.</param>
        /// <param name="localPort">The local port.</param>
        public Socket(Ipv4Address localIp, int localPort)
        {
            LocalAddress = localIp;
            LocalPort = localPort;
            DestinationAddress = null;
            DestinationPort = 0;
            sendBuffer = "";
            receiveBuffer = "";
        }
        #endregion

        #region Fields
        object bufferLock = new object();
        String receiveBuffer;
        String sendBuffer;
        Queue<Socket> pendingConnections = new Queue<Socket>();
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the local address.
        /// </summary>
        /// <value>
        /// The local address.
        /// </value>
        public Ipv4Address LocalAddress { get; set; }
        /// <summary>
        /// Gets or sets the destination address.
        /// </summary>
        /// <value>
        /// The destination address.
        /// </value>
        public Ipv4Address DestinationAddress { get; set; }
        /// <summary>
        /// Gets or sets the local port.
        /// </summary>
        /// <value>
        /// The local port.
        /// </value>
        public int LocalPort { get; set; }
        /// <summary>
        /// Gets or sets the destination port.
        /// </summary>
        /// <value>
        /// The destination port.
        /// </value>
        public int DestinationPort { get; set; }
        /// <summary>
        /// Gets a value indicating whether socket is passive.
        /// </summary>
        /// <value>
        ///   <c>true</c> if socket is passive; otherwise, <c>false</c>.
        /// </value>
        public bool IsPassive
        { 
            get
            {
                if(DestinationAddress == null && DestinationPort == 0)
                {
                    return true;
                }
                return false;
            }
        }
        /// <summary>
        /// Gets the source socket address.
        /// </summary>
        /// <value>
        /// The source socket.
        /// </value>
        public string SourceSocket
        {
            get
            {
                if(LocalAddress == null)
                {
                    return "";
                }
                string ret;
                ret = LocalAddress.Address;
                ret += ":";
                ret += LocalPort.ToString();
                return ret;
            }
        }
        /// <summary>
        /// Gets the destination socket addresss.
        /// </summary>
        /// <value>
        /// The destination socket.
        /// </value>
        public string DestinationSocket
        {
            get
            {
                if (DestinationAddress == null)
                {
                    return "";
                }
                string ret;
                ret = DestinationAddress.Address;
                ret += ":";
                ret += DestinationPort.ToString();
                return ret;
            }
        }
        /// <summary>
        /// Gets the pending connection.
        /// </summary>
        /// <value>
        /// The pending connection or null.
        /// </value>
        public Socket PendingConnection 
        { 
            get
            {
                if(pendingConnections.Count==0)
                {
                    return null;
                }
                return pendingConnections.Dequeue();
            }
        }
        /// <summary>
        /// Gets the send buffer.
        /// </summary>
        public string SendBuffer
        {
            get
            {
                lock(bufferLock)
                {
                    string tmp = sendBuffer;
                    sendBuffer = "";
                    return tmp;
                }
                
            }
        }
        /// <summary>
        /// Gets the received buffer.
        /// </summary>
        public string ReceivedBuffer
        {
            get
            {
                lock (bufferLock)
                {
                    string tmp = receiveBuffer;
                    receiveBuffer = "";
                    return tmp;
                }
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Raised when new data are received.
        /// </summary>
        public EventHandler NewReceivedData;
        /// <summary>
        /// Raised when new data are to send
        /// </summary>
        public EventHandler NewSendData;
        /// <summary>
        /// Raised when theres a new pending connection
        /// </summary>
        public EventHandler NewConnection;
        #endregion

        #region Methods
        /// <summary>
        /// Adds the new connection.
        /// </summary>
        /// <param name="newConnection">The new connection.</param>
        public void AddNewConnection(Socket newConnection)
        {
            pendingConnections.Enqueue(newConnection);
            OnNewConnection();
        }
        /// <summary>
        /// Adds the send data.
        /// </summary>
        /// <param name="data">The data.</param>
        public void AddSendData(string data)
        {
            lock(bufferLock)
            {
                sendBuffer += data;
            }
            OnSendData();
        }
        /// <summary>
        /// Adds the received data.
        /// </summary>
        /// <param name="data">The data.</param>
        public void AddReceivedData(string data)
        {
            lock(bufferLock)
            {
                receiveBuffer += data;
            }
            OnReceviedData();
        }
        private void OnReceviedData()
        {
            if(NewReceivedData != null)
            {
                NewReceivedData(this, EventArgs.Empty);
            }
        }
        private void OnSendData()
        {
            if(NewSendData != null)
            {
                NewSendData(this, EventArgs.Empty);
            }
        }
        private void OnNewConnection()
        {
            if(NewConnection != null)
            {
                NewConnection(this, EventArgs.Empty);
            }
        }
        #endregion
    }
}
