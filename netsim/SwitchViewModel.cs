﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace Gui
{
    /// <summary>
    /// View for <see cref="Core.Switch"/>
    /// </summary>
    class SwitchViewModel : NetworkElementView
    {
        #region Constructors
        public SwitchViewModel() : base(App.Network.AddSwitch())
        {
            ((Image)FindName("Icon")).Source = new BitmapImage(new Uri("pack://application:,,,/resources/switch.png"));
            ((Label)FindName("Label")).Content = "Switch";
        }
        #endregion
    }
}
