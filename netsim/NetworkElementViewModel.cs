﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core;

namespace Gui
{
    class NetworkElementViewModel
    {
        #region Constructors
        public NetworkElementViewModel(NetworkElement element)
        {
            Element = element;
        }
        #endregion

        #region Properties
        public NetworkElement Element { get; private set; }
        #endregion
        
    }
}
