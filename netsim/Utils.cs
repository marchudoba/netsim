﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;


namespace Core
{
    /// <summary>
    /// Contains utility functions.
    /// </summary>
    class Utils
    {
        /// <summary>
        /// Linear interpolation between x and y.
        /// </summary>
        public static Vector Lerp(Vector x, Vector y, double amount)
        {
            return (1 - amount) * x + amount * y;
        }
    }
}
