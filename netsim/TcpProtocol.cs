﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core
{
    [Serializable]
    sealed class TcpProtocol : TransportProtocol
    {
        #region Types
        /// <summary>
        /// Represents TCP connection
        /// </summary>
        class Connection : IPausable
        {
            #region Types
            /// <summary>
            /// Represents connection state
            /// </summary>
            internal enum State { Closed, Listen, SynSent, SynReceived, Established, FinWait, LastAck }
            internal class SendEventArgs : EventArgs
            {
                #region Constructors
                public SendEventArgs(TcpPacket segment)
                {
                    Segment = segment;
                }
                #endregion
                #region Properties
                public TcpPacket Segment { get; set; }
                #endregion
            }
            class DataBuffer
            {
                #region Constructors
                public DataBuffer()
                {
                    queue = new List<Tuple<int, string>>();
                }
                #endregion

                #region Fields
                List<Tuple<int, string>> queue;
                #endregion

                #region Properties
                /// <summary>
                /// Gets the lowest sequence in queue.
                /// </summary>
                public int? LowestSequence
                {
                    get
                    {
                        if(queue.Count == 0)
                        {
                            return null;
                        }
                        return queue[0].Item1;
                    }
                }
                #endregion

                #region Methods
                /// <summary>
                /// Adds the specified data in a queue.
                /// </summary>
                /// <param name="sequenceNumber">The sequence number.</param>
                /// <param name="data">The data.</param>
                public void Add(int sequenceNumber, string data)
                {
                    if(data=="")
                    {
                        return;
                    }
                    if (queue.Count == 0)
                    {
                        queue.Add(Tuple.Create<int, string>(sequenceNumber, data));
                        return;
                    }

                    List<Tuple<int, string>> merge = new List<Tuple<int, string>>();
                    int itemFirstSequence;
                    int itemLastSequence;
                    int dataFirstSequence = sequenceNumber;
                    int dataLastSequence = sequenceNumber + data.Length - 1;
                    for (int i = 0; i < queue.Count; i++) //add items in queue which seq# intersect with added to merge list
                    {
                        itemFirstSequence = queue[i].Item1;
                        itemLastSequence = itemFirstSequence + queue[i].Item2.Length - 1;
                        if (itemLastSequence == dataFirstSequence - 1)
                        {
                            merge.Add(queue[i]);
                            queue.RemoveAt(i--);
                        }
                        else if (dataFirstSequence <= itemLastSequence && itemFirstSequence <= dataLastSequence)
                        {
                            merge.Add(queue[i]);
                            queue.RemoveAt(i--);
                        }
                        else if (dataLastSequence + 1 == itemFirstSequence)
                        {
                            merge.Add(queue[i]);
                            queue.RemoveAt(i--);
                        }
                        else
                        {
                            break;
                        }
                    }
                    bool addedAddedToMerge = false; //add added item to merge list
                    for (int i = 0; i < merge.Count; i++)
                    {
                        if (merge[i].Item1 >= sequenceNumber)
                        {
                            merge.Insert(i, new Tuple<int, string>(sequenceNumber, data));
                            addedAddedToMerge = true;
                            break;
                        }
                    }
                    if (addedAddedToMerge == false)
                    {
                        merge.Add(new Tuple<int, string>(sequenceNumber, data));
                    }


                    Tuple<int, string> insert = this.merge(merge);
                    bool mergedAdded = false;
                    dataFirstSequence = insert.Item1;
                    dataLastSequence = dataFirstSequence + insert.Item2.Length - 1;
                    for (int i = 0; i < queue.Count; i++)
                    {
                        itemFirstSequence = queue[i].Item1;
                        itemLastSequence = itemFirstSequence + queue[i].Item2.Length - 1;
                        if (dataLastSequence < itemFirstSequence)
                        {
                            queue.Insert(i, insert);
                            mergedAdded = true;
                            break;
                        }
                    }
                    if (mergedAdded == false)
                    {
                        queue.Add(insert);
                    }
                }
                private Tuple<int, string> merge(List<Tuple<int, string>> toMerge)
                {
                    int sequenceNumber = toMerge[0].Item1;
                    string data = toMerge[0].Item2;
                    int currentLastSequence = sequenceNumber + data.Length - 1;
                    for (int i = 1; i < toMerge.Count; i++)
                    {
                        if ((toMerge[i].Item1 + toMerge[i].Item2.Length - 1 < currentLastSequence))
                        {
                            continue;
                        }
                        data += toMerge[i].Item2.Substring(currentLastSequence - toMerge[i].Item1 + 1);
                        currentLastSequence = sequenceNumber + data.Length - 1;
                    }
                    return new Tuple<int, string>(sequenceNumber, data);
                }
                /// <summary>
                /// Tries to retrieve data starting from sequenceNumber and of maximum length specified by maxLength from queue.
                /// </summary>
                /// <param name="sequenceNumber">The sequence number.</param>
                /// <param name="maxLength">Maximum allowed length.</param>
                /// <param name="data">If function returns true, this contains returned string.</param>
                /// <returns>
                /// true if at least one character is in queue with sequence number equals to sequenceNumber parameter.
                /// </returns>
                public bool TryGetData(int sequenceNumber, int maxLength, out string data)
                {
                    string itemSubstring;
                    foreach (var tuple in queue)
                    {
                        if (tuple.Item1 <= sequenceNumber)
                        {
                            itemSubstring = tuple.Item2.Substring(sequenceNumber - tuple.Item1);
                            if (itemSubstring.Length > maxLength)
                            {
                                data = itemSubstring.Substring(0, maxLength);
                            }
                            else
                            {
                                data = itemSubstring;
                            }
                            return true;
                        }
                    }
                    data = "";
                    return false;
                }
                /// <summary>
                /// Tries to retrieve continuous block of data from buffer.
                /// </summary>
                /// <param name="sequenceNumber">The sequence number.</param>
                /// <param name="data">The data.</param>
                /// <returns></returns>
                public bool TryGetData(int sequenceNumber, out string data)
                {
                    string itemSubstring;
                    foreach (var tuple in queue)
                    {
                        if (tuple.Item1 <= sequenceNumber)
                        {
                            itemSubstring = tuple.Item2.Substring(sequenceNumber - tuple.Item1);
                            data = itemSubstring;
                            return true;
                        }
                    }
                    data = "";
                    return false;
                }
                public void Remove(int sequenceNumber, int length)
                {
                    int dataFirstSequence = sequenceNumber;
                    int dataLastSequence = sequenceNumber + length - 1;
                    int itemFirstSequence;
                    int itemLastSequence;
                    Tuple<int, string> tmp;
                    for (int i = 0; i < queue.Count; i++)
                    {
                        itemFirstSequence = queue[i].Item1;
                        itemLastSequence = itemFirstSequence + queue[i].Item2.Length - 1;
                        if (dataFirstSequence <= itemLastSequence && itemFirstSequence <= dataLastSequence)
                        {
                            if (dataFirstSequence < itemFirstSequence)
                            {
                                Remove(dataFirstSequence, itemFirstSequence - dataFirstSequence);
                                dataFirstSequence = itemFirstSequence;
                            }
                            if (itemLastSequence < dataLastSequence)
                            {
                                Remove(itemLastSequence + 1, dataLastSequence - itemLastSequence);
                                dataLastSequence = itemLastSequence;
                            }
                            if (itemFirstSequence == dataFirstSequence && itemLastSequence > dataLastSequence)
                            {
                                queue[i] = new Tuple<int, string>(dataLastSequence + 1, queue[i].Item2.Substring(dataLastSequence - dataFirstSequence + 1));
                            }
                            else if (itemLastSequence == dataLastSequence && itemFirstSequence < dataFirstSequence)
                            {
                                queue[i] = new Tuple<int, string>(itemFirstSequence, queue[i].Item2.Substring(0, dataFirstSequence - itemFirstSequence));
                            }
                            else if (itemFirstSequence == dataFirstSequence && itemLastSequence == dataLastSequence)
                            {
                                queue.RemoveAt(i--);
                            }
                            else
                            {
                                tmp = queue[i];
                                queue[i] = new Tuple<int, string>(dataLastSequence + 1, tmp.Item2.Substring(dataLastSequence - itemFirstSequence + 1));
                                queue.Insert(i, new Tuple<int, string>(itemFirstSequence, tmp.Item2.Substring(0, dataLastSequence - itemFirstSequence)));
                            }
                        }
                    }
                }
                #endregion
            }
            #endregion

            #region Constructors
            /// <summary>
            /// Initializes a new instance of the <see cref="Connection"/> class.
            /// Connections starts in Listen state, to send open connection call <see cref="Open"/>
            /// </summary>
            /// <param name="socket">The socket associated with this connection.</param>
            public Connection(Socket socket)
            {
                Socket = socket;
                CurrentState = TcpProtocol.Connection.State.Listen;
                SendWindow = 30;
                SendNext = generateSequenceNumber();
                sendUnacknowledged = SendNext;
                retransmitTriggered = 0;
                retransmitTimer = new PausableTimer();
                retransmitTimer.AutoReset = true;
                retransmitTimer.Interval = 20000;
                Socket.NewSendData += OnNewSendData;

                receiveLock = new object();
                receiveData = new DataBuffer();
                retransmitData = new DataBuffer();
                closing =false;
            }
            #endregion

            #region Fields
            bool closing;
            object receiveLock;
            DataBuffer retransmitData;
            DataBuffer receiveData;
            int retransmitTriggered;
            PausableTimer retransmitTimer;
            int sendUnacknowledged;
            #endregion

            #region Properties
            /// <summary>
            /// Gets or sets the socket.
            /// </summary>
            /// <value>
            /// The socket.
            /// </value>
            public Socket Socket { get; set; }
            /// <summary>
            /// Gets or sets current state of the connection.
            /// </summary>
            public State CurrentState { get; set; }
            /// <summary>
            /// Gets or sets the send window size.
            /// </summary>
            /// <value>
            /// The send window.
            /// </value>
            public int SendWindow { get; set; }
            /// <summary>
            /// Gets or sets the receive window size.
            /// </summary>
            /// <value>
            /// The receive window.
            /// </value>
            public int ReceiveWindow { get; set; }
            /// <summary>
            /// Gets or sets the next expected sequence number.
            /// </summary>
            /// <value>
            /// The receive next.
            /// </value>
            public int ReceiveNext { get; set; }
            /// <summary>
            /// Gets or sets the next sequence number to send.
            /// </summary>
            /// <value>
            /// The send next.
            /// </value>
            public int SendNext { get; set; }
            #endregion

            #region Events
            /// <summary>
            /// Occurs when connection is closed.
            /// </summary>
            public event EventHandler Closed;
            /// <summary>
            /// Occurs when new segment is sent.
            /// </summary>
            public event EventHandler<SendEventArgs> Send;
            #endregion

            #region Event Handlers
            private void OnNewSendData(object sender, EventArgs e)
            {
                if (CurrentState != State.Established)
                {
                    return;
                }
                TrySendWindow();
            }
            #endregion

            #region Methods
            /// <summary>
            /// Receives the specified data.
            /// </summary>
            /// <param name="sequenceNumber">The sequence number.</param>
            /// <param name="acknowledgmentNumber">The acknowledgment number.</param>
            /// <param name="length">The length.</param>
            /// <param name="window">The window.</param>
            /// <param name="flags">The flags.</param>
            /// <param name="data">The data.</param>
            public void Receive(int sequenceNumber, int acknowledgmentNumber, int length, int window, TcpPacket.Flags flags, object data)
            {
                lock (receiveLock)
                {
                    if (flags.HasFlag(TcpPacket.Flags.RST)) //RST processing
                    {
                        if (Socket.IsPassive)
                        {
                            CurrentState = State.Listen;
                        }
                        else
                        {
                            CurrentState = State.Closed;
                            OnClosed();
                        }
                    }

                    if (CurrentState == State.Listen)
                    {
                        if (flags.HasFlag(TcpPacket.Flags.SYN))
                        {
                            ReceiveNext = sequenceNumber + length;
                            ReceiveWindow = window;
                            CurrentState = State.SynReceived;
                            send(SendNext, ReceiveNext, TcpPacket.Flags.SYN | TcpPacket.Flags.ACK, "");
                            sendUnacknowledged += 1;
                        }
                        else
                        {
                            send(SendNext, sequenceNumber, TcpPacket.Flags.RST, "");
                        }
                        return;
                    }

                    if (CurrentState == State.SynSent)
                    {
                        if (flags.HasFlag(TcpPacket.Flags.SYN) && flags.HasFlag(TcpPacket.Flags.ACK))
                        {
                            if (acknowledgmentNumber == sendUnacknowledged)
                            {
                                SendNext = acknowledgmentNumber;
                                ReceiveNext = sequenceNumber + length;
                                ReceiveWindow = window;
                                CurrentState = State.Established;
                                retransmitTimer.Enabled = true;
                                retransmitTimer.Elapsed += OnRetransmitTimerElapsed;
                                send(SendNext, ReceiveNext, TcpPacket.Flags.ACK, "");
                                TrySendWindow();
                            }
                        }
                        return;
                    }

                    if (CurrentState == State.SynReceived)
                    {
                        if (flags.HasFlag(TcpPacket.Flags.ACK))
                        {
                            if (acknowledgmentNumber == sendUnacknowledged)
                            {
                                SendNext = acknowledgmentNumber;
                                ReceiveWindow = window;
                                ReceiveNext += length;
                                CurrentState = State.Established;
                                retransmitTimer.Enabled = true;
                                retransmitTimer.Elapsed += OnRetransmitTimerElapsed;
                                TrySendWindow();
                            }
                        }
                        return;
                    }

                    if (CurrentState == State.Established || CurrentState == State.FinWait)
                    {

                        if (flags.HasFlag(TcpPacket.Flags.FIN))
                        {
                            if(ReceiveNext == sequenceNumber)
                            {
                                if(CurrentState== State.FinWait)
                                {
                                    CurrentState = State.LastAck;
                                    OnClosed();
                                }
                                send(SendNext, ReceiveNext, TcpPacket.Flags.FIN, "");
                                CurrentState = State.LastAck;
                                OnClosed();
                            }
                            else
                            {
                                closing = true;
                            }
                        }
                        else if (flags.HasFlag(TcpPacket.Flags.ACK))
                        {
                            if(closing && ReceiveNext == sequenceNumber)
                            {
                                send(SendNext, ReceiveNext, TcpPacket.Flags.FIN, "");
                                CurrentState = State.LastAck;
                                OnClosed();
                            }
                            if (ReceiveNext <= sequenceNumber && sequenceNumber <= ReceiveNext + ReceiveWindow)
                            {
                                SendNext = acknowledgmentNumber;

                                receiveData.Add(sequenceNumber, data as string);
                                if (receiveData.LowestSequence == ReceiveNext)
                                {
                                    string receiveDataPart;
                                    if (receiveData.TryGetData(ReceiveNext, out receiveDataPart))
                                    {
                                        receiveData.Remove(ReceiveNext, receiveDataPart.Length);
                                        ReceiveNext += receiveDataPart.Length;
                                        Socket.AddReceivedData(receiveDataPart);
                                        send(SendNext, ReceiveNext, TcpPacket.Flags.ACK, "");
                                    }
                                }
                                TrySendWindow();
                            }

                        }
                        return;
                    }

                }
            }
            /// <summary>
            /// Opens this connection by sending SYN.
            /// </summary>
            public void Open()
            {
                send(SendNext, 0, TcpPacket.Flags.SYN, "");
                CurrentState = State.SynSent;
                sendUnacknowledged += 1;
            }
            /// <summary>
            /// Closes this connection.
            /// </summary>
            public void Close()
            {
                if(CurrentState != State.Established)
                {
                    OnClosed();
                    return;
                }
                CurrentState = State.FinWait;
                send(sendUnacknowledged, ReceiveNext, TcpPacket.Flags.FIN, "");
                sendUnacknowledged += 1;
            }
            /// <summary>
            /// Tries to the send window.
            /// </summary>
            private void TrySendWindow()
            {
                string sendData = String.Empty;
                sendData = Socket.SendBuffer;
                string sendDataWindow;
                if (sendData.Length > SendWindow - (sendUnacknowledged - SendNext))
                {
                    sendDataWindow = sendData.Substring(0, SendWindow - (sendUnacknowledged - SendNext));
                    Socket.NewSendData -= OnNewSendData;
                    Socket.AddSendData(sendData.Substring(SendWindow - (sendUnacknowledged - SendNext)));
                    Socket.NewSendData += OnNewSendData;
                }
                else
                {
                    sendDataWindow = sendData;
                }

                List<string> splitData = new List<string>();
                while (sendDataWindow.Length != 0)
                {
                    if (sendDataWindow.Length < 10)
                    {
                        splitData.Add(sendDataWindow.Substring(0, sendDataWindow.Length));
                        break;
                    }
                    splitData.Add(sendDataWindow.Substring(0, 10));
                    sendDataWindow = sendDataWindow.Substring(10);
                }
                foreach (string dataPart in splitData)
                {
                    send(sendUnacknowledged, ReceiveNext, TcpPacket.Flags.ACK, dataPart);
                    sendUnacknowledged += dataPart.Length;
                }
            }
            private void OnClosed()
            {
                if (Closed != null)
                {
                    Closed(this, EventArgs.Empty);
                }
            }
            private void OnSend(TcpPacket segment)
            {
                if (Send != null)
                {
                    Send(this, new SendEventArgs(segment));
                }
            }
            private void addToRetransmitQueue(TcpPacket segment)
            {
                retransmitData.Add(segment.SequenceNumber, segment.Data as string);

            }
            private int generateSequenceNumber()
            {
                Random rnd = new Random();
                return Math.Abs(rnd.Next());
            }
            private void OnRetransmitTimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
            {
                if(retransmitData.LowestSequence < SendNext &&retransmitData.LowestSequence != null)
                {
                    retransmitData.Remove(retransmitData.LowestSequence.Value, SendNext - retransmitData.LowestSequence.Value);
                    retransmitTriggered = 0;
                }

                
                if (retransmitTriggered >= 3)
                {
                    retransmitData.Remove(SendNext, 10);
                    OnClosed();
                }
                string data;
                if(retransmitData.TryGetData(SendNext, 10, out data))
                {
                    retransmitTriggered++;
                    OnSend(new TcpPacket(Socket.LocalPort, Socket.DestinationPort, SendNext, ReceiveNext, SendWindow, TcpPacket.Flags.ACK, data));
                }
            }
            private void send(int sequenceNumber, int acknowledgmentNumber, TcpPacket.Flags flags, string data)
            {
                TcpPacket segment = new TcpPacket(Socket.LocalPort, Socket.DestinationPort, sequenceNumber, acknowledgmentNumber, SendWindow, flags, data);
                if(data!="")
                {
                    addToRetransmitQueue(segment);
                }
                OnSend(segment);
            }
            #endregion

            #region IPausable
            /// <summary>
            /// Pauses this instance.
            /// </summary>
            public virtual void Pause()
            {
                retransmitTimer.Paused = true;
                if(retransmitTimer.Enabled == true)
                {
                    retransmitTimer.Paused= true;
                }
            }
            /// <summary>
            /// Unpauses this instance.
            /// </summary>
            public virtual void Unpause()
            {
                retransmitTimer.Paused = false;
                if (retransmitTimer.Paused == true)
                {
                    retransmitTimer.Paused = false;
                }
            }
            #endregion
        }
        #endregion

        #region Constructors
        public TcpProtocol(List<Interface> interfaces, Dictionary<Type, Protocol> protocols)
            : base(interfaces, protocols)
        {
            connections = new List<Connection>();
        }
        #endregion

        #region Fields
        List<Connection> connections;
        #endregion

        #region Event Handlers
        private void OnClosed(object sender, EventArgs e)
        {
            RemoveConnection((sender as Connection).Socket);
        }
        private void OnSend(object sender, Connection.SendEventArgs e)
        {
            Send((sender as Connection).Socket.DestinationAddress, e.Segment);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Sends the data on specified socket.
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        public override void Send(Socket socket, object data)
        {
            socket.AddSendData(data as string);
        }
        /// <summary>
        /// Receives the specified packet.
        /// </summary>
        /// <param name="packet">The packet.</param>
        /// <param name="sourceIp">The source ip.</param>
        /// <param name="destinationIp">The destination ip.</param>
        public override void Receive(Layer3Packet packet, Ipv4Address sourceIp, Ipv4Address destinationIp)
        {
            TcpPacket segment = packet as TcpPacket;
            Connection connection = FindConnection(FindSocket(destinationIp, segment.DestinationPort, sourceIp, segment.SourcePort));
            if (connection == null && segment.Flag.HasFlag(TcpPacket.Flags.RST) == false && IsListeningOn(segment.DestinationPort) == false) // in case connection doesnt exist rst is sent to sender
            {
                Send(sourceIp, new TcpPacket(segment.DestinationPort, segment.SourcePort, segment.AcknowledgmentNumber, segment.SequenceNumber+segment.Length, 0, TcpPacket.Flags.RST | TcpPacket.Flags.ACK, ""));
                return;
            }
            else if (connection != null)
            {
                connection.Receive(segment.SequenceNumber, segment.AcknowledgmentNumber, segment.Length, segment.Window, segment.Flag, segment.Data);
            }
            else if (IsListeningOn(segment.DestinationPort))
            {
                Socket socket = new Socket(destinationIp, segment.DestinationPort, sourceIp, segment.SourcePort);
                Socket serverSocket = FindSocket(destinationIp, segment.DestinationPort);
                serverSocket.AddNewConnection(socket);
                CreateConnection(socket);
                connection = FindConnection(socket);
                connection.Receive(segment.SequenceNumber, segment.AcknowledgmentNumber, segment.Length, segment.Window, segment.Flag, segment.Data);
            }
        }
        /// <summary>
        /// Closes the specified socket.
        /// </summary>
        /// <param name="socket">The socket.</param>
        public override void Close(Socket socket)
        {
            Connection connection = FindConnection(socket);
            if(connection != null)
            {
                connection.Close();
            }

        }
        /// <summary>
        /// Listens on the specified port.
        /// </summary>
        /// <param name="port">The port.</param>
        /// <returns></returns>
        public override Socket Listen(int port)
        {
            Socket socket = new Socket(interfaces.First().IpAddress, port);
            if (isOpen(port))
            {
                return null;
            }
            CreateConnection(socket);
            return socket;
        }
        /// <summary>
        /// Opens new socket.
        /// </summary>
        /// <param name="destinationIp">The destination ip.</param>
        /// <param name="destinationPort">The destination port.</param>
        /// <returns></returns>
        public override Socket Open(Ipv4Address destinationIp, int destinationPort)
        {
            Socket socket = new Socket(interfaces.First().IpAddress, getFreePortNumber(), destinationIp, destinationPort);
            CreateConnection(socket);
            Connection connection = FindConnection(socket);
            connection.Open();
            return socket;
        }
        private void CreateConnection(Socket socket)
        {
            Connection connection = new Connection(socket);

            connections.Add(connection);
            AddSocket(socket);
            connection.Closed += OnClosed;
            connection.Send += OnSend;
        }
        private void RemoveConnection(Socket socket)
        {
            Connection connection = FindConnection(socket);
            if(connection == null)
            {
                return;
            }
            connections.Remove(connection);
            RemoveSocket(connection.Socket);
        }
        /// <summary>
        /// Finds the connection.
        /// </summary>
        /// <param name="socket">The socket.</param>
        /// <returns></returns>
        private Connection FindConnection(Socket socket)
        {
            foreach (var connection in connections)
            {
                if (connection.Socket == socket)
                {
                    return connection;
                }
            }
            return null;
        }
        private void Send(Ipv4Address destinationIp, TcpPacket segment)
        {
            (Protocols[Type.Ipv4] as Ipv4Protocol).Send(destinationIp, segment, Type.Tcp);
        }
        #endregion

        #region ISerializable
        /// <summary>
        /// Initializes a new instance of the <see cref="Ipv4Protocol" /> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="context">The context.</param>
        private TcpProtocol(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            connections = new List<Connection>();
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
        #endregion

        #region IPausable
        /// <summary>
        /// Pauses this instance.
        /// </summary>
        public override void Pause()
        {
            foreach (Connection connection in connections)
            {
                connection.Pause();
            }
        }
        /// <summary>
        /// Unpauses this instance.
        /// </summary>
        public override void Unpause()
        {
            foreach (Connection connection in connections)
            {
                connection.Unpause();
            }
        }
        #endregion
    }
}
