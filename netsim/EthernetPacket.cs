﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core
{
    /// <summary>
    /// Represents class for ethernet packet.
    /// </summary>
    class EthernetPacket : Layer1Packet
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="EthernetPacket"/> class.
        /// </summary>
        /// <param name="destinationMac">The destination mac.</param>
        /// <param name="sourceMac">The source mac.</param>
        /// <param name="etherType">EtherType.</param>
        /// <param name="data">The data.</param>
        public EthernetPacket(MacAddress destinationMac, MacAddress sourceMac, Protocol.Type etherType, Layer2Packet data)
            : base(data)
        {
            DestinationMac = destinationMac;
            SourceMac = sourceMac;
            Ethertype = etherType;
            FrameCheckSequence = calculateFrameCheckSequence();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the destination mac.
        /// </summary>
        public MacAddress DestinationMac { get; set; }
        /// <summary>
        /// Gets or sets the source mac.
        /// </summary>
        public MacAddress SourceMac { get; set; }
        /// <summary>
        /// Gets or sets the EtherType.
        /// </summary>
        public Protocol.Type Ethertype { get; set; }
        /// <summary>
        /// Gets or sets the frame check sequence.
        /// </summary>
        public int FrameCheckSequence { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Calculates the frame check sequence.
        /// </summary>
        /// <returns></returns>
        private int calculateFrameCheckSequence()
        {
            return Math.Abs((DestinationMac.ToString() + SourceMac.ToString()).GetHashCode());

        }
        #endregion
    }
}
