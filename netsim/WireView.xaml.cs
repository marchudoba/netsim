﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core;
using System.ComponentModel;
using System.Windows.Media.Animation;
using System.Runtime.Serialization;

namespace Gui
{
    /// <summary>
    /// Interaction logic for WireView.xaml
    /// </summary>
    [Serializable]
    public partial class WireView : WorkplaceItem, INotifyPropertyChanged, IDeserializationCallback
    {
        #region Contructors
        /// <summary>
        /// Initializes a new instance of the <see cref="WireView"/> class.
        /// </summary>
        /// <param name="wire">The wire.</param>
        public WireView(Wire wire) //reflect changes to ISerializable constructor
        {
            InitializeComponent();
            Wire = wire;
            Wire.SendingPacket += OnSendingPacket;
            Wire.SentPacket += OnSentPacket;
            packetViewLock = new object();
        }
        #endregion

        #region Fields
        private object packetViewLock;
        private NetworkElementView sourceElementView;
        private NetworkElementView destinationElementView;
        private PacketView toSourcePacket = null;
        private PacketView toDestPacket = null;
        #endregion

        #region Properties
        /// <summary>
        /// Gets <see cref="PacketView"/> thats currently sent to source.
        /// </summary>
        public PacketView ToSourcePacket
        {
            get
            {
                return toSourcePacket;
            }
            private set
            {
                toSourcePacket = value;
                NotifyPropertyChanged("ToSourcePacket");
            }
        }
        /// <summary>
        /// Gets <see cref="PacketView"/> thats currently sent to destination.
        /// </summary>
        public PacketView ToDestPacket
        {
            get
            {
                return toDestPacket;
            }
            private set
            {
                toDestPacket = value;
                NotifyPropertyChanged("ToDestPacket");
            }
        }
        /// <summary>
        /// Gets or sets the <see cref="Wire"/>.
        /// </summary>
        public Wire Wire { get; set; }
        /// <summary>
        /// Gets or sets the source <see cref="NetworkElementView"/>.
        /// </summary>
        public NetworkElementView SourceElementView
        {
            get
            {
                return sourceElementView;
            }
            set
            {
                sourceElementView = value;
                NotifyPropertyChanged("SourceElementView");
            }
        }
        /// <summary>
        /// Gets or sets the destination <see cref="NetworkElementView"/>.
        /// </summary>
        public NetworkElementView DestinationElementView
        {
            get
            {
                return destinationElementView;
            }
            set
            {
                destinationElementView = value;
                NotifyPropertyChanged("DestinationElementView");
            }
        }
        #endregion

        #region Methods
        public override void Remove()
        {
            if (toSourcePacket != null)
            {
                toSourcePacket.Remove();
            }
            if (toDestPacket != null)
            {
                toDestPacket.Remove();
            }
            SourceElementView.WireViews.Remove(this);
            DestinationElementView.WireViews.Remove(this);
            App.Network.Remove(this.Wire);
            base.Remove();
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Called when <see cref="Wire.SendingPacket"/> is raised.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="SendingPacketEventArgs"/> instance containing the event data.</param>
        protected void OnSendingPacket(object sender, SendingPacketEventArgs e)
        {
            if (e.SourceToDest)
            {
                Dispatcher.BeginInvoke(new Action(
                        delegate()
                        {
                            lock (packetViewLock)
                            {
                                if(toDestPacket == null)
                                {
                                    toDestPacket = new PacketView(e.Packet);
                                    Canvas.Children.Add(toDestPacket);
                                }

                                Point pos = (Point)Utils.Lerp(new Vector(Line.X1, Line.Y1), new Vector(Line.X2, Line.Y2), (double)e.Progress / (double)e.TransferTime);

                                Canvas.SetLeft(ToDestPacket, pos.X - ToDestPacket.ActualWidth / 2);
                                Canvas.SetTop(ToDestPacket, pos.Y - ToDestPacket.ActualHeight / 2);
                            }
                        }));
                
            }
            else if (e.DestToSource)
            {
                Dispatcher.BeginInvoke(new Action(
                        delegate()
                        {
                            lock (packetViewLock)
                            {
                                if(toSourcePacket == null)
                                {

                                    toSourcePacket = new PacketView(e.Packet);
                                    Canvas.Children.Add(toSourcePacket);
                                }

                                Point pos = (Point)Utils.Lerp(new Vector(Line.X2, Line.Y2), new Vector(Line.X1, Line.Y1), (double)e.Progress / (double)e.TransferTime);

                                Canvas.SetLeft(toSourcePacket, pos.X - toSourcePacket.ActualWidth / 2);
                                Canvas.SetTop(toSourcePacket, pos.Y - toSourcePacket.ActualHeight / 2);
                            }
                        }));
            }
        }
        /// <summary>
        /// Called when <see cref="Wire.SentPacket"/> is raised.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="SendingPacketEventArgs"/> instance containing the event data.</param>
        protected void OnSentPacket(object sender, SendingPacketEventArgs e)
        {
            if (e.SourceToDest)
            {
                Dispatcher.BeginInvoke(new Action(
                    delegate()
                    {
                        lock (packetViewLock)
                        {
                            if (toDestPacket != null)
                            {
                                Canvas.Children.Remove(ToDestPacket);
                                ToDestPacket = null;
                            }
                        }
                    }));
            }
            else if (e.DestToSource)
            {
                Dispatcher.BeginInvoke(new Action(
                    delegate()
                    {
                        lock (packetViewLock)
                        {
                            if (toSourcePacket != null)
                            {
                                Canvas.Children.Remove(toSourcePacket);
                                toSourcePacket = null;
                            }
                        }
                    }));
            }
        }
        protected override void OnSelected()
        {
            SourceLabel.Background = SystemColors.HighlightBrush;
            DestinationLabel.Background = SystemColors.HighlightBrush;
            Line.Stroke = SystemColors.HighlightBrush;
        }
        protected override void OnDeselected()
        {
            Line.Stroke = Brushes.Black;
            SourceLabel.Background = Brushes.White;
            DestinationLabel.Background = Brushes.White;
        }
        #endregion

        #region ISerializable
        protected WireView(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Wire = info.GetValue("Wire", typeof(Wire)) as Wire;


            SourceElementView = info.GetValue("SourceElementView", typeof(NetworkElementView)) as NetworkElementView;
            DestinationElementView = info.GetValue("DestinationElementView", typeof(NetworkElementView)) as NetworkElementView;
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("Wire", Wire);
            info.AddValue("SourceElementView", sourceElementView);
            info.AddValue("DestinationElementView", destinationElementView);
        }
        #endregion

        #region IDeserializationCallback
        public virtual void OnDeserialization(Object sender)
        {
            InitializeComponent();
            Wire.SendingPacket += OnSendingPacket;
            Wire.SentPacket += OnSentPacket;
            packetViewLock = new object();
        }
        #endregion

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;


        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }

    /// <summary>
    /// Converter from <see cref="NetworkElement"/> position and dimensions to <see cref="WireView"/> x,y coordinates.
    /// </summary>
    public class OffsetPositonByDims : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (targetType != typeof(Double))
            {
                throw new ArgumentException("Target type must be a Double");
            }

            if ((string)parameter == "X")
            {
                return (value[0] as NetworkElementView).ActualWidth / 2 + (double)value[1];
            }
            else if ((string)parameter == "Y")
            {
                return (value[0] as NetworkElementView).ActualHeight / 2 + (double)value[1];
            }
            throw new ArgumentException("Parameter must be X or Y");

        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }

    /// <summary>
    /// Converter from <see cref="WireView"/> position to interface label position.
    /// </summary>
    public class LineToLabelPosition : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (targetType != typeof(Double))
            {
                throw new ArgumentException("Target type must be a Double");
            }

            if ((string)parameter == "sourceLeft")
            {
                return Utils.Lerp(new Vector((double)value[0], (double)value[1]), new Vector((double)value[2], (double)value[3]), 0.3).X - (double)value[4] / 2;
            }
            else if ((string)parameter == "sourceTop")
            {
                return Utils.Lerp(new Vector((double)value[0], (double)value[1]), new Vector((double)value[2], (double)value[3]), 0.3).Y - (double)value[5] / 2;
            }
            else if ((string)parameter == "destinationLeft")
            {
                return Utils.Lerp(new Vector((double)value[2], (double)value[3]), new Vector((double)value[0], (double)value[1]), 0.3).X - (double)value[4] / 2;
            }
            else if ((string)parameter == "destinationTop")
            {
                return Utils.Lerp(new Vector((double)value[2], (double)value[3]), new Vector((double)value[0], (double)value[1]), 0.3).Y - (double)value[5] / 2;
            }

            throw new ArgumentException("Argument must be sourceTop, sourceLeft, destinationTop or destinationLeft");
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
