﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;

namespace Gui
{
    /// <summary>
    /// Canvas class changed to have Size, needed for Scrollbasrs to work.
    /// </summary>
    public class AutosizableCanvas : Canvas
    {
        protected override System.Windows.Size MeasureOverride(System.Windows.Size constraint)
        {
            // base.MeasureOverride(constraint);

            double width = 0;
            double height = 0;

            foreach (var child in InternalChildren)
            {
                if (child is UIElement)
                {
                    (child as UIElement).Measure(constraint);

                    if ((double)(child as UIElement).GetValue(Canvas.LeftProperty) != double.NaN)
                    {
                        if (width < (double)(child as UIElement).GetValue(Canvas.LeftProperty) + (double)(child as UIElement).DesiredSize.Width)
                        {
                            width = (double)(child as UIElement).GetValue(Canvas.LeftProperty) + (double)(child as UIElement).DesiredSize.Width;
                        }
                    }
                    if ((double)(child as UIElement).GetValue(Canvas.TopProperty) != double.NaN)
                    {
                        if (height < (double)(child as UIElement).GetValue(Canvas.TopProperty) + (double)(child as UIElement).DesiredSize.Height)
                        {
                            height = (double)(child as UIElement).GetValue(Canvas.TopProperty) + (double)(child as UIElement).DesiredSize.Height;
                        }
                    }
                }
            }

            return new Size(width+50, height+50);
        }
    }
}
