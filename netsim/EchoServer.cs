﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core
{
    /// <summary>
    /// Represents echo server
    /// </summary>
    [Serializable]
    sealed class EchoServer : Protocol
    {
        #region Types
        /// <summary>
        /// Class represents connection to echo server.
        /// </summary>
        public class Connection
        {
            /// <summary>
            /// Gets or sets the socket.
            /// </summary>
            /// <value>
            /// The socket.
            /// </value>
            public Socket socket { get; set; }
            /// <summary>
            /// Gets or sets the received.
            /// </summary>
            /// <value>
            /// The received.
            /// </value>
            public string received { get; set; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="EchoServer"/> class.
        /// </summary>
        /// <param name="protocols">The protocols.</param>
        public EchoServer(Dictionary<Type, Protocol> protocols) : base(protocols)
        {
            Connections = new List<Connection>();
        }
        #endregion


        #region Properties
        /// <summary>
        /// Gets or sets the server socket.
        /// </summary>
        /// <value>
        /// The server socket.
        /// </value>
        public Socket ServerSocket { get; set; }
        /// <summary>
        /// Gets or sets the connections.
        /// </summary>
        /// <value>
        /// The connections.
        /// </value>
        public List<Connection> Connections { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Listens on the specified port.
        /// </summary>
        /// <param name="port">The port.</param>
        /// <param name="protocol">The protocol.</param>
        public void Listen(int port, Type protocol)
        {
            if(ServerSocket != null)
            {
                (Protocols[protocol] as TransportProtocol).Close(ServerSocket);
                ServerSocket.NewConnection -= OnNewConnection;
                Connections.Clear();
                OnConnectionChanged();
            }
            ServerSocket = (Protocols[protocol] as TransportProtocol).Listen(port);
            ServerSocket.NewConnection += OnNewConnection;
        }
        private void OnConnectionChanged()
        {
            if(ConnectionsChanged != null)
            {
                ConnectionsChanged(this, EventArgs.Empty);
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when <see cref="Connections"/> change.
        /// </summary>
        public event EventHandler ConnectionsChanged;
        #endregion

        #region Event Handlers
        private void OnNewConnection(object sender, EventArgs e)
        {
            Connection connection = new Connection();
            connection.received = "";
            connection.socket = ServerSocket.PendingConnection;
            connection.socket.NewReceivedData += OnNewData;
            Connections.Add(connection);
            OnConnectionChanged();
        }
        private void OnNewData(object sender, EventArgs e)
        {
            Connection connection = Connections.First((x) =>  { if(x.socket==sender){return true;} return false; });
            connection.received += connection.socket.ReceivedBuffer;
            OnConnectionChanged();
        }
        #endregion

        #region ISerializable
        /// <summary>
        /// Initializes a new instance of the <see cref="Ipv4Protocol" /> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="context">The context.</param>
        private EchoServer(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Connections = new List<Connection>();
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
        #endregion
    }
}
