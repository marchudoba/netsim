﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Gui
{
    /// <summary>
    /// View for ping protocol
    /// </summary>
    class PingView : INotifyPropertyChanged
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="PingView"/> class.
        /// </summary>
        /// <param name="stack">The stack.</param>
        public PingView(Core.PcProtocolStack stack)
        {
            pingProtocol = (stack.Protocols[Core.Protocol.Type.PingProtocol] as Core.PingProtocol);
            pingProtocol.ReplyReceived += OnReplyReceived;
            pingProtocol.DestinationUnreachableReceived += OnDestinationUnreachableReceived;
        }

        #endregion

        #region Fields
        Core.PingProtocol pingProtocol;
        string log;
        string destinationIp;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the log.
        /// </summary>
        /// <value>
        /// The log.
        /// </value>
        public string Log
        {
            get
            {
                return log;
            }
            set
            {
                log = value;
                NotifyPropertyChanged("Log");
            }
        }
        /// <summary>
        /// Gets or sets the destination ip.
        /// </summary>
        /// <value>
        /// The destination ip.
        /// </value>
        public string DestinationIp
        {
            get
            {
                return destinationIp;
            }
            set
            {
                destinationIp = value;
                NotifyPropertyChanged("DestinationIp");
            }
        }
        #endregion

        #region EventHandlers
        private void OnReplyReceived(object sender, Core.IcmpReceivedEventArgs e)
        {
            Log+=("Received reply from " + e.SourceAddress.Address+"\n");
        }
        private void OnDestinationUnreachableReceived(object sender, Core.IcmpReceivedEventArgs e)
        {
            Log += "Received destination unreachable from " + e.SourceAddress.Address + "\n";
        }
        #endregion

        #region Methods
        /// <summary>
        /// Sends this instance.
        /// </summary>
        public void Send()
        {

            if (string.IsNullOrEmpty(DestinationIp))
            {
                return;
            }
            Log = "";
            pingProtocol.SendPing(new Core.Ipv4Address(DestinationIp+"/0"));
                
        }
        #endregion

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
        #endregion
    }
}
