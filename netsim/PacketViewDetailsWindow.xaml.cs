﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using Core;

namespace Gui
{
    /// <summary>
    /// Interaction logic for PacketViewDetailsWindow.xaml
    /// </summary>
    public partial class PacketViewDetailsWindow : Window
    {
        public PacketViewDetailsWindow()
        {
            InitializeComponent();

            Closing += OnClosing;
        }

        #region Event Handlers
        private void OnClosing(object sender, CancelEventArgs e)
        {
            Hide();
            e.Cancel = true;
        }
        #endregion
    }

    /// <summary>
    /// Data template selector for different kind of packets.
    /// </summary>
    public class PacketViewItemSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(Object item, DependencyObject container)
        {
            if (item is EthernetPacket)
            {
                return (container as FrameworkElement).FindResource("ethernetTemplate") as DataTemplate;
            }
            else if (item is ArpPacket)
            {
                return (container as FrameworkElement).FindResource("arpTemplate") as DataTemplate;
            }
            else if (item is Ipv4Packet)
            {
                return (container as FrameworkElement).FindResource("ipTemplate") as DataTemplate;
            }
            else if(item is IcmpPacket)
            {
                return (container as FrameworkElement).FindResource("icmpTemplate") as DataTemplate;
            }
            else if(item is UdpPacket)
            {
                return (container as FrameworkElement).FindResource("udpTemplate") as DataTemplate;
            }
            else if(item is TcpPacket)
            {
                return (container as FrameworkElement).FindResource("tcpTemplate") as DataTemplate;
            }
            else
            {
                throw new ArgumentException("No data template for " + item.ToString());
            }
        }
    }
}
