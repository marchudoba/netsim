﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Gui
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            App.StatusBar = (StatusBar)FindName("StatusBar");
            App.Debug = (Label)FindName("Debug");
            App.Current.Resources.Add("StatusBar", App.StatusBar);
            Title = "Untitled";
        }
        #endregion

        #region Fields
        string file = null;  
        #endregion

        #region Methods
        /// <summary>
        /// Removes all items from the workplace.
        /// </summary>
        private void clearWorkplace()
        {
            App.Network.Pause();
            Canvas workplace = (App.Current.MainWindow as MainWindow).Workplace;
            
            while (workplace.Children.Count > 0)
            {
                if (workplace.Children[0] is WorkplaceItem)
                {
                    (workplace.Children[0] as WorkplaceItem).Remove();
                    continue;
                }
                else
                {
                    throw new ArgumentException("Workplace contains something else that Workplace item: " + workplace.Children[0].ToString());
                }
            }
            App.Network.Unpause();
        }

        /// <summary>
        /// Saves items from workplace to file.
        /// </summary>
        private void saveWorkplace()
        {
            FileStream fileStream = File.Open(file, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            Canvas workplace = (App.Current.MainWindow as MainWindow).Workplace;
            BinaryFormatter formatter = new BinaryFormatter();

            List<WorkplaceItem> items = new List<WorkplaceItem>();
            foreach (var item in workplace.Children)
            {
                items.Add(item as WorkplaceItem);
            }

            formatter.Serialize(fileStream, items);
            fileStream.Close();
        }
        /// <summary>
        /// Loads items from file to the workplace.
        /// </summary>
        private void loadWorkplace()
        {
            FileStream fileStream = File.Open(file, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            Canvas workplace = (App.Current.MainWindow as MainWindow).Workplace;
            BinaryFormatter formatter = new BinaryFormatter();

            List<WorkplaceItem> items = formatter.Deserialize(fileStream) as List<WorkplaceItem>;
            fileStream.Close();

            foreach(var item in items)
            {
                if (item is WireView)
                {
                    App.Network.AddWire((item as WireView).Wire);
                }
                else if (item is NetworkElementView)
                {
                    App.Network.AddElement((item as NetworkElementView).Element);
                }
                
                workplace.Children.Add(item);
            }

         
            (Workplace.DataContext as Workplace).CurrentState = Gui.Workplace.State.Select;
        }
        /// <summary>
        /// Opens the file to save.
        /// </summary>
        /// <returns>true if file is successfully opened</returns>
        private bool openFileToSave()
        {
            if (file == null)
            {
                Microsoft.Win32.SaveFileDialog saveDialog = new Microsoft.Win32.SaveFileDialog();
                saveDialog.DefaultExt = ".network";
                saveDialog.FileName = "Untitled";
                saveDialog.Filter = "Network save file (.network)|*.network";
                if (saveDialog.ShowDialog() == true)
                {
                    file = saveDialog.FileName;
                    Title = saveDialog.FileName;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
        #endregion

        #region Event handlers
        /// <summary>
        /// Handles click event for tool buttons.
        /// </summary>
        private void onToolButtonOnClick(object sender, RoutedEventArgs e)
        {
            System.Windows.Resources.StreamResourceInfo resourceInfo=null;
            
            switch (((ToggleButton)sender).Name)
            {
                case "SelectButton":
                    ((Workplace)DataContext).CurrentState = Gui.Workplace.State.Select;
                    break;
                case "RouterButton":
                    resourceInfo = Application.GetResourceStream(new Uri("pack://application:,,,/resources/routerCursor.cur"));
                    ((Workplace)DataContext).CurrentState = Gui.Workplace.State.AddingRouter;
                    break;
                case "SwitchButton":
                    resourceInfo = Application.GetResourceStream(new Uri("pack://application:,,,/resources/switchCursor.cur"));
                    ((Workplace)DataContext).CurrentState = Gui.Workplace.State.AddingSwitch;
                    break;
                case "PcButton":
                    resourceInfo = Application.GetResourceStream(new Uri("pack://application:,,,/resources/pcCursor.cur"));
                    ((Workplace)DataContext).CurrentState = Gui.Workplace.State.AddingPc;
                    break;
                case "WireButton":
                    resourceInfo = Application.GetResourceStream(new Uri("pack://application:,,,/resources/wireCursor.cur"));
                    ((Workplace)DataContext).CurrentState = Gui.Workplace.State.AddingWire;
                    break;
            }
        }
        /// <summary>
        /// On the new button click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void onNewButtonClick(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Přejete si uložit aktuální projekt?", "", MessageBoxButton.YesNoCancel);

            if (result == MessageBoxResult.Yes)
            {
                if (openFileToSave()==false)
                {
                    return;
                }
                saveWorkplace();
            }
            else if (result == MessageBoxResult.Cancel)
            {
                return;
            } //else

            if (file != null)
            {
                file = null;
            }
            Title = "Untitled";
            clearWorkplace();
        }
        /// <summary>
        /// Ons the open button click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void onOpenButtonClick(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Přejete si uložit aktuální projekt?", "", MessageBoxButton.YesNoCancel);

            if (result == MessageBoxResult.Yes)
            {
                if (openFileToSave() == false)
                {
                    return;
                }
                saveWorkplace();
            }
            else if (result == MessageBoxResult.Cancel)
            {
                return;
            }

            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            dialog.DefaultExt = ".network";
            dialog.FileName = "Untitled.network";
            dialog.Filter = "Network save file (.network)|*.network";
            if (dialog.ShowDialog() == true)
            {
                file = dialog.FileName;
                Title = dialog.FileName;
            }
            else
            {
                return;
            }
            clearWorkplace();
            loadWorkplace();
        }
        /// <summary>
        /// Ons the save button click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void onSaveButtonClick(object sender, RoutedEventArgs e)
        {
            if (openFileToSave() == false)
            {
                return;
            }
            saveWorkplace();
        }
        /// <summary>
        /// Ons the save as button click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void onSaveAsButtonClick(object sender, RoutedEventArgs e)
        {
            if (file !=null)
            {
                file = null;
                Title = "Untitled";
            }
            if (openFileToSave() == false)
            {
                return;
            }
            saveWorkplace();
        }
        private void onHelpButtonClicked(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Nápověda momentálně dostupná pouze v uživatelské příručce.", "Nápověda", MessageBoxButton.OK);
        }
        /// <summary>
        /// Ons the pause button checked.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void onPauseButtonChecked(object sender, RoutedEventArgs e)
        {
            Canvas workplace = (App.Current.MainWindow as MainWindow).Workplace;
            List<Core.IPausable> pausableItems = new List<Core.IPausable>();

            foreach (var item in workplace.Children)
            {
                if (item is Core.IPausable)
                {
                    pausableItems.Add(item as Core.IPausable);
                }
            }

            foreach(var item in pausableItems)
            {
                item.Pause();
            }
            App.Network.Pause();
        }
        /// <summary>
        /// Ons the pause button unchecked.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void onPauseButtonUnchecked(object sender, RoutedEventArgs e)
        {
            Canvas workplace = (App.Current.MainWindow as MainWindow).Workplace;
            List<Core.IPausable> pausableItems = new List<Core.IPausable>();

            foreach (var item in workplace.Children)
            {
                if (item is Core.IPausable)
                {
                    pausableItems.Add(item as Core.IPausable);
                }
            }

            foreach (var item in pausableItems)
            {
                item.Unpause();
            }
            App.Network.Unpause();
        }
        #endregion
    }
}
