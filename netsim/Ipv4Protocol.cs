﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Core;

namespace Core
{
    /// <summary>
    /// Ipv4 protocol
    /// </summary>
    [Serializable]
    sealed class Ipv4Protocol : Protocol, ISerializable
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Ipv4Protocol"/> class.
        /// </summary>
        /// <param name="interfaces">The interfaces.</param>
        /// <param name="protocols">The protocols.</param>
        public Ipv4Protocol(List<Interface> interfaces, Dictionary<Type, Protocol> protocols)
            : base(protocols)
        {
            this.interfaces = interfaces;
            Table = new RouteTable(interfaces);
        }
        #endregion

        #region Fields
        List<Interface> interfaces;
        #endregion

        #region Properties
        /// <summary>
        /// Gets the route table.
        /// </summary>
        public RouteTable Table { get; private set; }
        #endregion

        #region Methods
        /// <summary>
        /// Creates ip packet and sends it to destination ip.
        /// </summary>
        /// <param name="destinationIp">The destination ip.</param>
        /// <param name="data">The data.</param>
        /// <param name="protocol">The protocol.</param>
        /// <param name="ttl">The TTL.</param>
        public void Send(Ipv4Address destinationIp, Layer3Packet data, Protocol.Type protocol, int ttl = 10)
        {
            Ipv4Packet packet;
            if (destinationIp.IsOnSameNetwork(new Ipv4Address("127.0.0.1/8")))
            {
                packet = new Ipv4Packet(interfaces[0].IpAddress, destinationIp, protocol, ttl, data);
                Receive(packet);
                return;
            }
            foreach (Interface interfac in interfaces)
            {
                if (interfac.IsUp && interfac.IpAddress.Address == destinationIp.Address)
                {
                    packet = new Ipv4Packet(interfac.IpAddress, destinationIp, protocol, ttl, data);
                    Receive(packet);
                    return;
                }
            }



            RouteTable.Entry entry = Table.FindRoute(destinationIp);
            if (entry == null)
            {
                return;
            }
            packet = new Ipv4Packet(entry.OutgoingInterface.IpAddress, destinationIp, protocol, ttl, data);
            if (entry.Gateway == null)
            {
                (Protocols[Protocol.Type.Ethernet] as EthernetProtocol).Send(packet, entry.OutgoingInterface, destinationIp, Protocol.Type.Ipv4);
            }
            else
            {
                (Protocols[Protocol.Type.Ethernet] as EthernetProtocol).Send(packet, entry.OutgoingInterface, entry.Gateway, Protocol.Type.Ipv4);
            }

        }
        /// <summary>
        /// Sends the specified packet.
        /// </summary>
        /// <param name="packet">The packet.</param>
        public void Send(Ipv4Packet packet)
        {
            RouteTable.Entry entry = Table.FindRoute(packet.DestinationAddress);
            if (entry == null)
            {
                if (packet.Protocol != Type.Icmp || (packet.Protocol == Type.Icmp && (((packet.Data as IcmpPacket).Type == IcmpProtocol.Type.EchoRequest) || ((packet.Data as IcmpPacket).Type == IcmpProtocol.Type.EchoReply))))
                {
                    (Protocols[Type.Icmp] as IcmpProtocol).SendDestinationUnreacheble(packet.SourceAddress, IcmpProtocol.Code.NetUnreachable, packet);
                }
                return;
            }
            if (entry.Gateway == null)
            {
                (Protocols[Protocol.Type.Ethernet] as EthernetProtocol).Send(packet, entry.OutgoingInterface, packet.DestinationAddress, Protocol.Type.Ipv4);
            }
            else
            {
                (Protocols[Protocol.Type.Ethernet] as EthernetProtocol).Send(packet, entry.OutgoingInterface, entry.Gateway, Protocol.Type.Ipv4);
            }

        }
        /// <summary>
        /// Receives the specified packet.
        /// </summary>
        /// <param name="packet">The packet.</param>
        public void Receive(Ipv4Packet packet)
        {
            foreach (Interface interfac in interfaces)
            {
                if (interfac.IsUp && interfac.IpAddress.Address == packet.DestinationAddress.Address)
                {
                    if(packet.Protocol == Type.Icmp)
                    {
                        (Protocols[Type.Icmp] as IcmpProtocol).Receive(packet.SourceAddress, packet.Data as IcmpPacket);
                        return;
                    }
                    else if(packet.Protocol == Type.Udp)
                    {
                        (Protocols[Type.Udp] as UdpProtocol).Receive(packet.Data, packet.SourceAddress, packet.DestinationAddress);
                        return;
                    }
                    else if(packet.Protocol == Type.Tcp)
                    {
                        (Protocols[Type.Tcp] as TcpProtocol).Receive(packet.Data, packet.SourceAddress, packet.DestinationAddress);
                        return;
                    }
                }
            }


            packet.TimeToLive--;
            if (packet.TimeToLive == 0)
            {
                if (packet.Protocol != Type.Icmp)
                {
                    (Protocols[Type.Icmp] as IcmpProtocol).SendTimeExceeded(packet.SourceAddress, IcmpProtocol.Code.TtlExceeded);
                }
            }
            Send(packet);
        }
        #endregion

        #region ISerializable
        /// <summary>
        /// Initializes a new instance of the <see cref="Ipv4Protocol" /> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="context">The context.</param>
        private Ipv4Protocol(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            interfaces = info.GetValue("interfaces", typeof(List<Interface>)) as List<Interface>;
            Table = info.GetValue("Table", typeof(RouteTable)) as RouteTable;
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("interfaces", interfaces);
            info.AddValue("Table", Table);
        }
        #endregion
    }
}
