﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Core;

namespace Gui
{
    /// <summary>
    /// View class for echo server
    /// </summary>
    class EchoServerView : INotifyPropertyChanged
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="EchoServerView"/> class.
        /// </summary>
        /// <param name="stack">The stack.</param>
        public EchoServerView(PcProtocolStack stack)
        {
            App.Current.Dispatcher.BeginInvoke(new Action(
                    delegate()
                    {
                        Connections = new ObservableCollection<EchoServer.Connection>();
                    }));

            server = stack.Protocols[Core.Protocol.Type.EchoServer] as EchoServer;
            server.ConnectionsChanged += OnConnectionsChanged;
            Protocols = new List<Core.Protocol.Type>();
            Protocols.Add(Core.Protocol.Type.Udp);
            Protocols.Add(Core.Protocol.Type.Tcp);
        }
        #endregion

        #region Fields
        EchoServer server;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the port.
        /// </summary>
        /// <value>
        /// The port.
        /// </value>
        public int Port { get; set; }
        /// <summary>
        /// Gets or sets the protocol.
        /// </summary>
        /// <value>
        /// The protocol.
        /// </value>
        public Core.Protocol.Type Protocol { get; set; }
        /// <summary>
        /// Gets or sets the protocols.
        /// </summary>
        /// <value>
        /// The protocols.
        /// </value>
        public List<Core.Protocol.Type> Protocols { get; set; }
        /// <summary>
        /// Gets the connections.
        /// </summary>
        /// <value>
        /// The connections.
        /// </value>
        public ObservableCollection<EchoServer.Connection> Connections { get; private set; }
        #endregion

        #region Methods
        /// <summary>
        /// Listens on specified port.
        /// </summary>
        public void Listen()
        {
            if (Port == 0 || (Protocol != Core.Protocol.Type.Tcp && Protocol != Core.Protocol.Type.Udp))
            {
                return;
            }
            server.Listen(Port, Protocol);
        }
        #endregion

        #region Event Handlers
        private void OnConnectionsChanged(object sender, EventArgs e)
        {
            
            App.Current.Dispatcher.BeginInvoke(new Action(
                    delegate()
                    {
                        Connections.Clear();
                        foreach (var connection in server.Connections)
                        {
                            Connections.Add(connection);
                        }
                    }));
        }
        #endregion

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;


        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
