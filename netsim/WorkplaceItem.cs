﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows;
using System.Runtime.Serialization;

namespace Gui
{
    /// <summary>
    /// Represents item that can be placed inside the Workplace.
    /// </summary>
    [Serializable]
    public class WorkplaceItem : UserControl, ISerializable
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkplaceItem"/> class.
        /// </summary>
        public WorkplaceItem()
        {
        }
        #endregion

        #region Fields
        private bool isSelected = false;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the is selected.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return isSelected;
            }
            set
            {
                isSelected = value;
                if (isSelected == true)
                {
                    onSelected();
                }
                else
                {
                    onDeselected();
                }

            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Removes this instance from workplace.
        /// </summary>
        public virtual void Remove()
        {
            (App.Current.MainWindow as MainWindow).Workplace.Children.Remove(this);
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when IsSelected is set to true.
        /// </summary>
        public event EventHandler Selected;
        /// <summary>
        /// Occurs when IsSelected is set to false.
        /// </summary>
        public event EventHandler Deselected;
        #endregion

        #region EventHandler
        /// <summary>
        /// Called when IsSelected is set to true.
        /// </summary>
        protected virtual void OnSelected()
        {
            BorderBrush = SystemColors.HighlightBrush;
            BorderThickness = new Thickness(2);

        }
        private void onSelected()
        {
            OnSelected();
            if (Selected != null)
            {
                Selected(this, EventArgs.Empty);
            }
        }
        /// <summary>
        /// Called when IsSelected is set to false.
        /// </summary>
        protected virtual void OnDeselected()
        {
            BorderThickness = new Thickness(0);
        }
        private void onDeselected()
        {
            OnDeselected();
            if (Deselected != null)
            {
                Deselected(this, EventArgs.Empty);
            }
        }
        #endregion

        #region ISerializable
        protected WorkplaceItem(SerializationInfo info, StreamingContext context)
        {
            isSelected = false;
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
        }
        #endregion
    }
}
