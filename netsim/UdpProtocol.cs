﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core
{
    /// <summary>
    /// Represents Udp protocol.
    /// </summary>
    [Serializable]
    sealed class UdpProtocol : TransportProtocol
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="UdpProtocol"/> class.
        /// </summary>
        public UdpProtocol(List<Interface> interfaces, Dictionary<Type, Protocol> protocols)
            : base(interfaces, protocols)
        { 

        }
        #endregion
        
        #region Methods
        /// <summary>
        /// Sends the data on specified socket.
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        public override void Send(Socket socket, object data)
        {
            List<string> splitData = new List<string>();
            while((data as string).Length!=0)
            {
                if((data as string).Length<10)
                {
                    splitData.Add((data as string).Substring(0, (data as string).Length));
                    break;
                }
                splitData.Add((data as string).Substring(0, 10));
                data  = (data as string).Substring(10);
            }
            foreach(string dataPart in splitData)
            {
                (Protocols[Type.Ipv4] as Ipv4Protocol).Send(socket.DestinationAddress, new UdpPacket(socket.LocalPort, socket.DestinationPort, dataPart), Type.Udp);
            }
        }
        /// <summary>
        /// Receives the specified UDP packet.
        /// </summary>
        /// <param name="udpPacket">The UDP packet.</param>
        /// <param name="sourceIp">The source ip.</param>
        /// <param name="destinationIp">The destination ip.</param>
        public override void Receive(Layer3Packet udpPacket, Ipv4Address sourceIp, Ipv4Address destinationIp)
        {
            UdpPacket packet = udpPacket as UdpPacket;

            Socket socket = FindSocket(destinationIp, packet.DestinationPort, sourceIp, packet.SourcePort);

            if(socket != null)
            {
                socket.AddReceivedData(packet.Data.ToString());
            }
            else if(IsListeningOn(packet.DestinationPort))
            {
                socket = FindSocket(destinationIp, packet.DestinationPort);
                Socket newConnection = new Socket(destinationIp, packet.DestinationPort, sourceIp, packet.SourcePort);
                socket.AddNewConnection(newConnection);
                newConnection.AddReceivedData(packet.Data.ToString());
                openSockets.Add(newConnection);
            }
            else
            {
                (Protocols[Type.Icmp] as IcmpProtocol).SendDestinationUnreacheble(sourceIp, IcmpProtocol.Code.PortUnreachable, new Ipv4Packet(sourceIp, destinationIp, Type.Udp, 10, udpPacket));
            }
        }
        /// <summary>
        /// Listens on the specified port.
        /// </summary>
        /// <param name="port">The port.</param>
        /// <returns></returns>
        public override Socket Listen(int port)
        {
            Socket socket = new Socket(interfaces.First().IpAddress, port); 
            if (isOpen(port))
            {
                return null;
            }
            AddSocket(socket);
            return socket;
        }
        /// <summary>
        /// Opens new socket.
        /// </summary>
        /// <param name="destinationIp">The destination ip.</param>
        /// <param name="destinationPort">The destination port.</param>
        /// <returns></returns>
        public override Socket Open(Ipv4Address destinationIp, int destinationPort)
        {
            Socket socket = new Socket(interfaces.First().IpAddress, getFreePortNumber(), destinationIp, destinationPort);
            AddSocket(socket);
            return socket;
        }
        /// <summary>
        /// Closes the specified socket.
        /// </summary>
        /// <param name="socket">The socket.</param>
        public override void Close(Socket socket)
        {
            RemoveSocket(socket);
        }
        #endregion
        
        #region ISerializable
        /// <summary>
        /// Initializes a new instance of the <see cref="Ipv4Protocol" /> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="context">The context.</param>
        private UdpProtocol(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
        #endregion
    }
}
