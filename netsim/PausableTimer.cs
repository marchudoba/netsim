﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace Core
{
    /// <summary>
    /// Represents pausable timer.
    /// </summary>
    /// <remarks>
    /// There can be several milliseconds difference when timer is paused.
    /// </remarks>
    public class PausableTimer : Timer
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="PausableTimer"/> class.
        /// </summary>
        public PausableTimer()
        {
            Elapsed += onElapsed;
            paused = false;
            timerLock = new object();
        }
        #endregion

        #region Fields
        private object timerLock;
        DateTime start;
        double remaining;
        bool paused;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="T:System.Timers.Timer" /> should raise the <see cref="E:System.Timers.Timer.Elapsed" /> event.
        /// </summary>
        /// <returns>true if the <see cref="T:System.Timers.Timer" /> should raise the <see cref="E:System.Timers.Timer.Elapsed" /> event; otherwise, false. The default is false.</returns>
        public new bool Enabled
        {
            get
            {
                return base.Enabled;
            }
            set
            {
                lock (timerLock)
                {
                    base.Enabled = value;
                    if (value == true)
                    {
                        start = DateTime.Now;
                        paused = false;
                    }
                }
            }
        }
        /// <summary>
        /// Gets or sets a value indicating whether the timer is paused.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [paused]; otherwise, <c>false</c>.
        /// </value>
        public bool Paused
        {
            get
            {
                return paused;
            }
            set
            {
                lock (timerLock)
                {
                    if (paused == value) { return; }
                    if (value == true)
                    {
                        if (Enabled == false) { return; }
                        remaining = Interval - (DateTime.Now - start).TotalMilliseconds;
                        if (remaining <= 0) { remaining = 1; }
                        
                        Enabled = false;
                        paused = true;
                    }
                    else
                    {
                        Interval = remaining;
                        Enabled = true;
                        start = DateTime.Now;
                        paused = false;
                    }
                }
            }
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Needed in case autoreset is on.
        /// </summary>
        private void onElapsed(object sender, ElapsedEventArgs e)
        {
            lock (timerLock)
            {
                if (paused == true) { return; }
                start = e.SignalTime;
            }
        }
        #endregion
    }
}
