﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core
{
    /// <summary>
    /// Represents Icmp packet.
    /// </summary>
    public class IcmpPacket : Layer3Packet
    {
        

        #region Contructors
        /// <summary>
        /// Initializes a new instance of the <see cref="IcmpPacket"/> class.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="code">The code.</param>
        public IcmpPacket(IcmpProtocol.Type type, IcmpProtocol.Code code)
        {
            this.Type = type;
            this.Code = code;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="IcmpPacket"/> class.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="code">The code.</param>
        /// <param name="identifier">The identifier.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <param name="originalPacket">The original packet.</param>
        public IcmpPacket(IcmpProtocol.Type type, IcmpProtocol.Code code, int identifier, int sequenceNumber, Ipv4Packet originalPacket)
        {
            this.Type = type;
            this.Code = code;
            Identifier = identifier;
            SequenceNumber = sequenceNumber;
            OriginalPacket = originalPacket;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public IcmpProtocol.Type Type { get; set; }
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        public IcmpProtocol.Code Code { get; set; }
        /// <summary>
        /// Gets or sets the checksum.
        /// </summary>
        /// <value>
        /// The checksum.
        /// </value>
        public int Checksum { get; set; }
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Identifier { get; set; }
        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>
        /// The sequence number.
        /// </value>
        public int SequenceNumber { get; set; }
        /// <summary>
        /// Gets or sets the original packet.
        /// </summary>
        /// <value>
        /// The original packet.
        /// </value>
        public Ipv4Packet OriginalPacket { get; set; }
        #endregion
    }
}
