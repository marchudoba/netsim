﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Runtime.Serialization;

namespace Core
{ 
    /// <summary>
    /// Represents a MAC address
    /// </summary>
    [Serializable]
    public class MacAddress : ISerializable
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="MacAddress"/> class.
        /// The address is generated randomly.
        /// </summary>
        public MacAddress()
        {
            address = randomMac();

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="MacAddress"/> class.
        /// </summary>
        /// <param name="address">The address in hexadecimal without dashes.</param>
        public MacAddress(string address)
        {
            if (isMac(address) == false)
            {
                throw new ArgumentException();
            }
            this.address = address;
        }
        #endregion

        #region Fields
        private static Random rndMac = new Random();
        private string address;
        private static MacAddress broadcast = new MacAddress("FFFFFFFFFFFF");
        #endregion

        #region Properties
        /// <summary>
        /// Gets the broadcast mac address.
        /// </summary>
        public static MacAddress Broadcast
        {
            get
            {
                return broadcast;
            }
        }
        /// <summary>
        /// Gets a value indicating whether mac address is broadcast.
        /// </summary>
        public bool IsBroadcast
        {
            get
            {
                if (address == "FFFFFFFFFFFF")
                {
                    return true;
                }
                return false;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Generates random mac address
        /// </summary>
        /// <returns></returns>
        private string randomMac()
        {
            string ret = "";
            byte[] randN = new byte[6];
            rndMac.NextBytes(randN);

            foreach (var part in randN)
            {
                ret += part.ToString("X");
            }
            return ret;
        }
        /// <summary>
        /// Determines whether the specified address is mac.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <returns></returns>
        private bool isMac(string address)
        {
            if (address.Length>12)
            {
                return false;
            }
            ulong res;
            if(UInt64.TryParse(address, System.Globalization.NumberStyles.HexNumber, null, out res)==false)
            {
                return false;
            }
            return true;
        }
        public override string ToString()
        {
            return address;
        }
        #endregion

        #region ISerializable
        protected MacAddress(SerializationInfo info, StreamingContext context)
        {
            address = info.GetString("address");
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("address", address);
        }
        #endregion
    }
}
