﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core
{
    [Serializable]
    public class Router : NetworkElement
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Router"/> class.
        /// </summary>
        public Router() :base(4)
        {
            Stack = new RouterProtocolStack(Interfaces);
        }
        #endregion

        #region ISerializable
        protected Router(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
        #endregion
    }
}
