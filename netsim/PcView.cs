﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows;
using System.Runtime.Serialization;

namespace Gui
{
    /// <summary>
    /// View for <see cref="Core.Pc"/>
    /// </summary>
    [Serializable]
    class PcView : NetworkElementView
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="PcView"/> class.
        /// </summary>
        public PcView()
            : base(App.Network.AddPc())
        {
            ((Image)FindName("Icon")).Source = new BitmapImage(new Uri("pack://application:,,,/resources/pc.png"));
            ((Label)FindName("Label")).Content = "Pc";
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Called when apply button in settings menu is clicked.
        /// </summary>
        protected void OnApplyClicked(object sender, RoutedEventArgs e)
        {
            string ipAddress = ((TextBox)SettingsWindow.FindName("ipAddressTextBox")).Text;
            string mask = ((TextBox)SettingsWindow.FindName("maskTextBox")).Text;
            string defaultGateway = ((TextBox)SettingsWindow.FindName("defaultGatewayTextBox")).Text;

            Element.Interfaces[0].IpAddress.SetAddress(ipAddress, mask);
            Element.Interfaces[0].DefaultGateway.SetAddress(defaultGateway, mask);
        }
        /// <summary>
        /// Called when ok button in settings menu is clicked.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        protected void OnOkClicked(object sender, RoutedEventArgs e)
        {
            string ipAddress = ((TextBox)SettingsWindow.FindName("ipAddressTextBox")).Text;
            string mask = ((TextBox)SettingsWindow.FindName("maskTextBox")).Text;
            string defaultGateway = ((TextBox)SettingsWindow.FindName("defaultGatewayTextBox")).Text;

            Element.Interfaces[0].IpAddress.SetAddress(ipAddress, mask);
            Element.Interfaces[0].DefaultGateway.SetAddress(defaultGateway, mask);
            SettingsWindow.Hide();
        }
        /// <summary>
        /// Called when cancel button in settings menu is clicked.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        protected void OnCancelClicked(object sender, RoutedEventArgs e)
        {
            SettingsWindow.Hide();
        }
        #endregion

        #region ISerializable
        protected PcView(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
        #endregion

        #region IDeserializationCallback
        public override void OnDeserialization(Object sender)
        {
            base.OnDeserialization(sender);
            ((Image)FindName("Icon")).Source = new BitmapImage(new Uri("pack://application:,,,/resources/pc.png"));
            ((Label)FindName("Label")).Content = "Pc";
        }
        #endregion
    }
}
