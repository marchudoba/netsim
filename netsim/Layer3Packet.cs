﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core
{
    /// <summary>
    /// Base class for all layer3 protocol packet (<see cref="IcmpPacket"/>, <see cref="UdpPacket"/>, <see cref="TcpPacket"/>)
    /// </summary>
    public abstract class Layer3Packet : Packet
    {
    }
}
