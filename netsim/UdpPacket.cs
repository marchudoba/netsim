﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core
{
    /// <summary>
    /// Represents Udp packet.
    /// </summary>
    class UdpPacket : Layer3Packet
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="UdpPacket"/> class.
        /// </summary>
        public UdpPacket(int sourcePort, int destinationPort, object data) 
        {
            SourcePort = sourcePort;
            DestinationPort = destinationPort;
            Data = data;
            Length = calculateLength();
            Checksum = calculateChecksum();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the source port.
        /// </summary>
        public int SourcePort {get; set;}
        /// <summary>
        /// Gets or sets the destination port.
        /// </summary>
        public int DestinationPort {get; set;}
        /// <summary>
        /// Gets or sets the length.
        /// </summary>
        public int Length { get; set; }
        /// <summary>
        /// Gets or sets the checksum.
        /// </summary>
        public int Checksum { get; set; }
        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        public object Data { get; set; }
        #endregion

        #region Methods
        private int calculateLength()
        {
            return Data.ToString().Length;
        }
        private int calculateChecksum()
        {
            return Math.Abs(Data.GetHashCode());
        }
        #endregion
    }
}
