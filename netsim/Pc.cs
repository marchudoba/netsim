﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core
{
    /// <summary>
    /// Represents pc
    /// </summary>
    [Serializable]
    public class Pc : NetworkElement
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Pc"/> class.
        /// </summary>
        public Pc() :base(1)
        {
            Stack = new PcProtocolStack(Interfaces);
        }
        #endregion

        #region ISerializable
        protected Pc(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
        #endregion
    }
}
