﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Core;
using System.Windows.Controls;
using System.Globalization;

namespace Gui
{
    /// <summary>
    /// View class for RouteTable.
    /// </summary>
    class RouteTableView
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="RouteTableView"/> class.
        /// </summary>
        /// <param name="routeTable">The route table.</param>
        public RouteTableView(Core.RouteTable routeTable)
        {
            this.routeTable = routeTable;
            List<Core.RouteTable.Entry> list = routeTable.Entries;
            //list.Reverse();
            Table = new ObservableCollection<Core.RouteTable.Entry>(list);
            routeTable.ContentChanged += OnContentChanged;

            Interfaces = new List<string>();
            foreach (Core.Interface interfac in routeTable.Interfaces)
            {
                Interfaces.Add(interfac.Name);
            }

        }
        #endregion

        #region Fields
        Core.RouteTable routeTable;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the list of interface names.
        /// </summary>
        public List<string> Interfaces { get; set; }
        /// <summary>
        /// Gets or sets the route table.
        /// </summary>
        public ObservableCollection<Core.RouteTable.Entry> Table { get; set; }
        /// <summary>
        /// Gets or sets the destination network.
        /// </summary>
        public string DestinationNetwork { get; set; }
        /// <summary>
        /// Gets or sets the interface.
        /// </summary>
        public string Interface { get; set; }
        /// <summary>
        /// Gets or sets the gateway.
        /// </summary>
        public string Gateway { get; set; }
        /// <summary>
        /// Gets or sets the metric.
        /// </summary>
        public int Metric { get; set; }
        #endregion

        #region EventHandlers
        private void OnContentChanged(object sender, EventArgs e)
        {
            List<Core.RouteTable.Entry> list = routeTable.Entries;
            Table.Clear();
            
            foreach(var entry in list)
            {
                Table.Add(entry);
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds the entry.
        /// </summary>
        public void AddEntry()
        {
            if(Interface == null || DestinationNetwork == null || Gateway == null)
            {
                return;
            }

            Ipv4Address destNetwork = new Ipv4Address(DestinationNetwork);
            Core.Interface interfac = routeTable.Interfaces.Find((x) => x.Name == Interface);
            Ipv4Address gateway = new Ipv4Address(Gateway + "/0");
            int metric = Metric;


            RouteTable.Entry entry = new Core.RouteTable.Entry(destNetwork, interfac, gateway, metric, RouteTable.Type.Manual);
            routeTable.AddEntry(entry);
        }
        /// <summary>
        /// Removes the entry.
        /// </summary>
        /// <param name="entry">The entry.</param>
        public void RemoveEntry(RouteTable.Entry entry)
        {
            Table.Remove(entry);
        }
        #endregion

    }
}
