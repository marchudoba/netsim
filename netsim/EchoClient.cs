﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core
{
    /// <summary>
    /// Represents echo client.
    /// </summary>
    [Serializable]
    class EchoClient : Protocol
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="EchoClient"/> class.
        /// </summary>
        /// <param name="protocols">The protocols.</param>
        public EchoClient(Dictionary<Type, Protocol> protocols) : base(protocols)
        {

        }
        #endregion

        #region Fields
        Socket socket;
        Type protocol;
        #endregion

        #region Methods
        /// <summary>
        /// Sends the specified destination address.
        /// </summary>
        /// <param name="destinationAddress">The destination address.</param>
        /// <param name="destinationPort">The destination port.</param>
        /// <param name="data">The data.</param>
        /// <param name="protocol">The protocol.</param>
        public void Send(Ipv4Address destinationAddress, int destinationPort, object data, Type protocol)
        {
            if(socket != null && socket.DestinationAddress.Address == destinationAddress.Address && socket.DestinationPort == destinationPort && this.protocol==protocol)
            {    
            }
            else
            {
                if(socket != null)
                {
                    (Protocols[protocol] as TransportProtocol).Close(socket);
                }
                socket = (Protocols[protocol] as TransportProtocol).Open(destinationAddress, destinationPort);
                this.protocol = protocol;
            }
            (Protocols[protocol] as TransportProtocol).Send(socket, data);
            
        }
        #endregion

        #region ISerializable
        /// <summary>
        /// Initializes a new instance of the <see cref="Ipv4Protocol" /> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="context">The context.</param>
        protected EchoClient(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
        #endregion
    }
}
