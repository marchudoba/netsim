﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core;

namespace Gui
{
    /// <summary>
    /// View for router interface.
    /// </summary>
    class RouterInterfaceView : InterfaceView
    {
        #region Constructors
        public RouterInterfaceView(Interface interfac)
            : base(interfac)
        {

        }
        #endregion
    }
}
