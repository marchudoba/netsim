﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core
{
    class TcpPacket : Layer3Packet
    {
        #region Types
        [Flags]
        public enum Flags { None=0, SYN=1, ACK=2, RST=4 , FIN=8}
        #endregion

        #region Constructors
        public TcpPacket(int sourcePort, int destinationPort, int sequenceNumber, int acknowledgmentNumber, int window, Flags flag, object data)
        {
            SourcePort = sourcePort;
            DestinationPort = destinationPort;
            SequenceNumber = sequenceNumber;
            AcknowledgmentNumber = acknowledgmentNumber;
            Window = window;
            Flag = flag;
            Data = data;
            Checksum = calculateChecksum();
        }
        #endregion

        #region Properties
        public int SourcePort {get;set;}
        public int DestinationPort {get;set;}
        public int SequenceNumber {get;set;}
        public int AcknowledgmentNumber  {get;set;}
        public int Window {get;set;}
        public int Checksum { get; set; }
        public int Length
        {
            get
            {
                int len = (Data as string).Length;
                if(Flag.HasFlag(Flags.SYN))
                {
                    len += 1;
                }
                return len;
            }
        }
        public Flags Flag { get; set; }
        public object Data { get; set; }
        #endregion

        #region Methods
        private int calculateChecksum()
        {
            return Math.Abs(Data.GetHashCode());
        }
        #endregion
    }
}
