﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows;

namespace Gui
{
    /// <summary>
    /// View for <see cref="Core.Pc"/>
    /// </summary>
    class PcViewModel : NetworkElementView
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="PcViewModel"/> class.
        /// </summary>
        public PcViewModel() : base(App.Network.AddPc())
        {
            ((Image)FindName("Icon")).Source = new BitmapImage(new Uri("pack://application:,,,/resources/pc.png"));
            ((Label)FindName("Label")).Content = "Pc";
            SettingsWindow.ApplyButton.Click += OnApplyClicked;
            SettingsWindow.OkButton.Click += OnOkClicked;
            SettingsWindow.CancelButton.Click += OnCancelClicked;
        }
        #endregion


        

        #region Event Handlers
        /// <summary>
        /// Called when apply button in settings menu is clicked.
        /// </summary>
        protected void OnApplyClicked(object sender, RoutedEventArgs e)
        {
            string ipAddress = ((TextBox)SettingsWindow.FindName("ipAddressTextBox")).Text;
            string mask = ((TextBox)SettingsWindow.FindName("maskTextBox")).Text;
            string defaultGateway = ((TextBox)SettingsWindow.FindName("defaultGatewayTextBox")).Text;

            Element.Interfaces[0].IpAddress.SetAddress(ipAddress, mask);
            Element.Interfaces[0].DefaultGateway.SetAddress(defaultGateway, mask);
        }
        protected void OnOkClicked(object sender, RoutedEventArgs e)
        {
            string ipAddress = ((TextBox)SettingsWindow.FindName("ipAddressTextBox")).Text;
            string mask = ((TextBox)SettingsWindow.FindName("maskTextBox")).Text;
            string defaultGateway = ((TextBox)SettingsWindow.FindName("defaultGatewayTextBox")).Text;

            Element.Interfaces[0].IpAddress.SetAddress(ipAddress, mask);
            Element.Interfaces[0].DefaultGateway.SetAddress(defaultGateway, mask);
        }
        protected void OnCancelClicked(object sender, RoutedEventArgs e)
        {
            
        }
        #endregion
    }
}
