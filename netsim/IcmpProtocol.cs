﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core
{
    /// <summary>
    /// Represents protocol Icmp
    /// </summary>
    [Serializable]
    sealed public class IcmpProtocol : Protocol, ISerializable
    {
        #region Types
        /// <summary>
        /// Represents Type field in Icmp packet
        /// </summary>
        public new enum Type { DestinationUnreachable, TimeExceeded, EchoReply, EchoRequest }
        /// <summary>
        /// Represents Code field in Icmp packet
        /// </summary>
        public enum Code { NetUnreachable, PortUnreachable, TtlExceeded, Empty }
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="IcmpProtocol"/> class.
        /// </summary>
        /// <param name="protocols">The protocols.</param>
        public IcmpProtocol(Dictionary<Protocol.Type, Protocol> protocols) : base(protocols)
        {

        }
        #endregion

        #region Events
        /// <summary>
        /// Raised when echo request is received.
        /// </summary>
        public EventHandler<IcmpReceivedEventArgs> ReceivedEchoRequest;
        /// <summary>
        /// Raised when echo reply is received.
        /// </summary>
        public EventHandler<IcmpReceivedEventArgs> ReceivedEchoReply;
        /// <summary>
        /// Raised when destination unreachable is received.
        /// </summary>
        public EventHandler<IcmpReceivedEventArgs> ReceivedDestinationUnreachable;
        /// <summary>
        /// Raised when time exceeded is received.
        /// </summary>
        public EventHandler<IcmpReceivedEventArgs> ReceivedTimeExceeded;
        #endregion

        #region Methods
        /// <summary>
        /// Sends the echo request.
        /// </summary>
        /// <param name="destinationAddress">The destination address.</param>
        /// <param name="identifier">The identifier.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        public void SendEchoRequest(Ipv4Address destinationAddress, int identifier, int sequenceNumber)
        {
            Send(destinationAddress, Type.EchoRequest, Code.Empty ,identifier, sequenceNumber, null);
        }

        /// <summary>
        /// Sends the echo reply.
        /// </summary>
        /// <param name="destinationAddress">The destination address.</param>
        /// <param name="identifier">The identifier.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        public void SendEchoReply(Ipv4Address destinationAddress, int identifier, int sequenceNumber)
        {
            Send(destinationAddress, Type.EchoReply, Code.Empty, identifier, sequenceNumber, null);
        }

        /// <summary>
        /// Sends the destination unreacheble.
        /// </summary>
        /// <param name="destinationAddress">The destination address.</param>
        /// <param name="code">The code.</param>
        /// <param name="originalPacket">The original packet.</param>
        public void SendDestinationUnreacheble(Ipv4Address destinationAddress, Code code, Ipv4Packet originalPacket)
        {
            Send(destinationAddress, Type.DestinationUnreachable, code, 0, 0, originalPacket);
        }

        /// <summary>
        /// Sends the time exceeded.
        /// </summary>
        /// <param name="destinationAddress">The destination address.</param>
        /// <param name="code">The code.</param>
        public void SendTimeExceeded(Ipv4Address destinationAddress, Code code)
        {
            Send(destinationAddress, Type.TimeExceeded, code, 0, 0, null);
        }

        private void Send(Ipv4Address destinationAddress, Type type, Code code, int identifier, int sequenceNumber, Ipv4Packet originalPacket)
        {
            IcmpPacket packet = new IcmpPacket(type, code, identifier, sequenceNumber, originalPacket);
            (Protocols[Protocol.Type.Ipv4] as Ipv4Protocol).Send(destinationAddress, packet, Protocol.Type.Icmp);
        }

        /// <summary>
        /// Receives the packet from source ip.
        /// </summary>
        /// <param name="sourceIp">The source ip.</param>
        /// <param name="packet">The packet.</param>
        public void Receive(Ipv4Address sourceIp, IcmpPacket packet)
        {
            switch(packet.Type)
            {
                case Type.EchoRequest:
                    SendEchoReply(sourceIp, packet.Identifier, packet.SequenceNumber);
                    OnReceivedEchoRequest(new IcmpReceivedEventArgs(sourceIp, packet));
                    break;
                case Type.EchoReply:
                    OnReceivedEchoReply(new IcmpReceivedEventArgs(sourceIp, packet));
                    break;
                case Type.DestinationUnreachable:
                    OnReceivedDestinationUnreachable(new IcmpReceivedEventArgs(sourceIp, packet));
                    break;
                case Type.TimeExceeded:
                    OnReceivedTimeExceeded(new IcmpReceivedEventArgs(sourceIp, packet));
                    break;
            }

        }

        private void OnReceivedEchoRequest(IcmpReceivedEventArgs e)
        {
            if(ReceivedEchoRequest!=null)
            {
                ReceivedEchoRequest(this, e);
            }
        }
        private void OnReceivedEchoReply(IcmpReceivedEventArgs e)
        {
            if (ReceivedEchoReply != null)
            {
                ReceivedEchoReply(this, e);
            }
        }
        private void OnReceivedTimeExceeded(IcmpReceivedEventArgs e)
        {
            if (ReceivedTimeExceeded != null)
            {
                ReceivedTimeExceeded(this, e);
            }
        }
        private void OnReceivedDestinationUnreachable(IcmpReceivedEventArgs e)
        {
            if (ReceivedDestinationUnreachable != null)
            {
                ReceivedDestinationUnreachable(this, e);
            }
        }
        #endregion

        #region ISerializable
        private IcmpProtocol(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
        #endregion
    }
    
    /// <summary>
    /// Event args for <see cref="ReceivedEchoRequest"/>, <see cref="ReceivedEchoReply"/>,
    /// <see cref="ReceivedDestinationUnreachable"/> and <see cref="ReceivedTimeExceeded"/>.
    /// </summary>
    public class IcmpReceivedEventArgs : EventArgs
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="IcmpReceivedEventArgs"/> class.
        /// </summary>
        /// <param name="sourceAddress">The source address.</param>
        /// <param name="packet">The packet.</param>
        public IcmpReceivedEventArgs(Ipv4Address sourceAddress, IcmpPacket packet)
        {
            Packet = packet;
            SourceAddress = sourceAddress;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the packet.
        /// </summary>
        /// <value>
        /// The packet.
        /// </value>
        public IcmpPacket Packet { get; set; }
        /// <summary>
        /// Gets or sets the source address.
        /// </summary>
        /// <value>
        /// The source address.
        /// </value>
        public Ipv4Address SourceAddress { get; set; }
        #endregion

    }
}
