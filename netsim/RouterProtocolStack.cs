﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core
{
    /// <summary>
    /// Represents protocol stack for router.
    /// </summary>
    [Serializable]
    class RouterProtocolStack : ProtocolStack
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="RouterProtocolStack"/> class.
        /// </summary>
        /// <param name="interfaces"></param>
        public RouterProtocolStack(List<Interface> interfaces)
            : base(interfaces)
        {
            Protocols.Add(Protocol.Type.Arp, new ArpProtocol(Protocols));
            Protocols.Add(Protocol.Type.Ethernet, new EthernetProtocol(Protocols));
            Protocols.Add(Protocol.Type.Ipv4, new Ipv4Protocol(interfaces, Protocols));
            Protocols.Add(Protocol.Type.Icmp, new IcmpProtocol(Protocols));
        }
        #endregion

        #region Methods
        /// <summary>
        /// Receives the specified packet.
        /// </summary>
        /// <param name="packet">The packet.</param>
        /// <param name="interfac">The interfac.</param>
        protected override void Receive(Packet packet, Interface interfac)
        {
            (Protocols[Protocol.Type.Ethernet] as EthernetProtocol).Receive(packet as EthernetPacket, interfac);
        }
        #endregion

        #region ISerializable
        protected RouterProtocolStack(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
        #endregion
    }
}
